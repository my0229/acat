#pragma once

#include "../ChunkData.h"
#include "../Defines.h"

class cWorld;



class cByteBuffer;





/** Serializes one chunk's data to (possibly multiple) protocol versions.
Caches the serialized data for as long as this object lives, so that the same data can be sent to
other clients using the same protocol. */
/**将一个区块的数据序列化为（可能是多个）协议版本。

只要此对象存在，就会缓存序列化数据，以便将相同的数据发送到

使用相同协议的其他客户端*/
//块数据序列化程序
class cChunkDataSerializer 
{
public:

	cChunkDataSerializer(
		int                                         a_ChunkX,
		int                                         a_ChunkZ,
		const cChunkData &                          a_Data,
		const unsigned char *                       a_BiomeData,
		const eDimension                            a_Dimension
	);

	/** For each client, serializes the chunk into their protocol version and sends it. */
	/**对于每个客户机，将区块序列化为其协议版本并发送*/
	void SendToClients(const std::unordered_set<cClientHandle *> & a_SendTo);

	void Serialize110(const std::vector<cClientHandle *> * a_SendTo);  // Release 1.9.4 //被大改了

protected:
	

	/** Writes all blocks in a chunk section into a series of Int64.
	Writes start from the bit directly subsequent to the previous write's end, possibly crossing over to the next Int64. */
	/**将块段中的所有块写入一系列Int64。

	写入从上一次写入结束后的位开始，可能会过渡到下一个Int64*/
	inline void WriteSectionDataSeamless(cByteBuffer & a_Packet, const cChunkData::sChunkSection & a_Section, const UInt8 a_BitsPerEntry);

	/** Finalises the data, compresses it if required, and delivers it to all clients. */
	/**最终确定数据，根据需要压缩数据，并将其交付给所有客户*/
	void CompressAndSend(cByteBuffer & a_Packet, const std::vector<cClientHandle *> & a_SendTo);

	/** The coordinates of the chunk to serialise. */
	/**要序列化的块的坐标*/
	int m_ChunkX, m_ChunkZ;

	/** The data read from the chunk, to be serialized. */
	/**从区块读取的数据，将被序列化*/
	const cChunkData & m_Data;

	/** The biomes in the chunk, to be serialized. */
	/**区块中的生物群落，待序列化*/
	const unsigned char * m_BiomeData;

	/** The dimension where the chunk resides. */
	/**块所在的维度*/
	const eDimension m_Dimension;
} ;




