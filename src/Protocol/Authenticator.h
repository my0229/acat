
// cAuthenticator.h

// Interfaces to the cAuthenticator class representing the thread that authenticates users against the official Mojang servers
// Authentication prevents "hackers" from joining with an arbitrary username (possibly impersonating the server admins)
// For more info, see http://wiki.vg/Session
// In Cuberite, authentication is implemented as a single thread that receives queued auth requests and dispatches them one by one.

//与代表线程的cAuthenticator类的接口，该线程根据官方Mojang服务器对用户进行身份验证

//身份验证可防止“黑客”使用任意用户名加入（可能模拟服务器管理员）

//有关详细信息，请参阅http://wiki.vg/Session

//在Cuberite中，身份验证被实现为单个线程，该线程接收排队的身份验证请求并逐个分派它们。





#pragma once

#include "../OSSupport/IsThread.h"

// fwd:
class cUUID;
class cSettingsRepositoryInterface;

namespace Json
{
	class Value;
}





class cAuthenticator:
	public cIsThread
{
	using Super = cIsThread;

public:

	cAuthenticator();
	virtual ~cAuthenticator() override;

		/** (Re-)read server and address from INI: */
	/**（从INI和服务器地址读取）：*/
	void ReadSettings(cSettingsRepositoryInterface & a_Settings);

	/** Queues a request for authenticating a user. If the auth fails, the user will be kicked */
	/**对验证用户的请求进行排队。如果身份验证失败，用户将被踢出*/
	void Authenticate(int a_ClientID, const AString & a_UserName, const AString & a_ServerHash);

	/** Starts the authenticator thread. The thread may be started and stopped repeatedly */
	/**启动验证器线程。线程可以反复启动和停止*/
	void Start(cSettingsRepositoryInterface & a_Settings);

	/** Stops the authenticator thread. The thread may be started and stopped repeatedly */
	/**停止验证器线程。线程可以反复启动和停止*/
	void Stop(void);

private:

	class cUser
	{
	public:
		int     m_ClientID;
		AString m_Name;
		AString m_ServerID;

		cUser(int a_ClientID, const AString & a_Name, const AString & a_ServerID) :
			m_ClientID(a_ClientID),
			m_Name(a_Name),
			m_ServerID(a_ServerID)
		{
		}
	};

	typedef std::deque<cUser> cUserList;

	cCriticalSection m_CS;
	cUserList        m_Queue;
	cEvent           m_QueueNonempty;

	/** The server that is to be contacted for auth / UUID conversions */
	/**要联系进行身份验证/UUID转换的服务器*/
	AString m_Server;

	/** The URL to use for auth, without server part.
	%USERNAME% will be replaced with actual user name.
	%SERVERID% will be replaced with server's ID.
	For example "/session/minecraft/hasJoined?username=%USERNAME%&serverId=%SERVERID%". */

	/**用于身份验证的URL，不包含服务器部件。

	%用户名%将替换为实际用户名。

	%SERVERID%将替换为服务器的ID。

	例如“/session/minecraft/hasJoin？username=%username%&serverId=%serverId%”*/
	AString m_Address;

	AString m_PropertiesAddress;
	bool    m_ShouldAuthenticate;

	/** cIsThread override: */
	/**线程重写：*/
	virtual void Execute(void) override;

	/** Returns true if the user authenticated okay, false on error
	Returns the case-corrected username, UUID, and properties (eg. skin). */
	/**如果用户通过身份验证，则返回true，如果出现错误，则返回false

	返回经过大小写更正的用户名、UUID和属性（如皮肤）*/
	bool AuthWithYggdrasil(AString & a_UserName, const AString & a_ServerId, cUUID & a_UUID, Json::Value & a_Properties);
};




