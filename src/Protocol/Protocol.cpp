#include "Protocol.h"
#include <zlib.h>
#include "json/json.h"
#include "../StringCompression.h"
#include "../Server.h"
#include "../Root.h"
#include "ProtocolRecognizer.h"
#include "Packetizer.h"
#include "../JsonUtils.h"
#include "../ClientHandle.h"
//#include "../Entities/Player.h"
#include "ChunkDataSerializer.h"


const int MAX_ENC_LEN = 512;  // Maximum size of the encrypted message; should be 128, but who knows...
const uLongf MAX_COMPRESSED_PACKET_LEN = 200 KiB;  // Maximum size of compressed packets.
static const UInt32 CompressionThreshold = 256;  // After how large a packet should we compress it.



	void cProtocol::SendLoginSuccess(void)
{
	ASSERT(m_State == 2);  // State: login?

	// Enable compression:
	//启用压缩：
	{
		cPacketizer Pkt(*this, pktStartCompression);
		Pkt.WriteVarInt32(CompressionThreshold);
	}

	m_State = 3;  // State = Game

	{
		cPacketizer Pkt(*this, pktLoginSuccess);
		Pkt.WriteString(m_Client->GetUUID().ToLongString());//UUID
		Pkt.WriteString("lengjing");//用户名
	}
}
void cProtocol::DataReceived(cByteBuffer & a_Buffer, const char * a_Data, size_t a_Size)
{
	AddReceivedData(a_Buffer, a_Data, a_Size);
}

// fwd: main.cpp:
extern bool g_ShouldLogCommIn, g_ShouldLogCommOut;


void cProtocol::SendPluginMessage(const AString & a_Channel, const AString & a_Message)
{
	ASSERT(m_State == 3);  // In game mode?

	cPacketizer Pkt(*this, pktPluginMessage);
	Pkt.WriteString(a_Channel);
	Pkt.WriteBuf(a_Message.data(), a_Message.size());
}

void cProtocol::SendLogin(const cPlayer & a_Player, const cWorld & a_World)
{
	// Send the Join Game packet:
	{
		cServer * Server = cRoot::Get()->GetServer();
		cPacketizer Pkt(*this, pktJoinGame);
		Pkt.WriteBEUInt32(31);//玩家实体
		Pkt.WriteBEUInt8(1); //游戏模式
		Pkt.WriteBEInt32(0); 
		Pkt.WriteBEUInt8(2); 
		Pkt.WriteBEUInt8(10);
		Pkt.WriteString("default");  // Level type - wtf?
		Pkt.WriteBool(false);  // Reduced Debug Info - wtf?
	}

	// Send the spawn position:
	//发送玩家位置
	{
		cPacketizer Pkt(*this, pktSpawnPosition);
		Pkt.WriteXYZPosition64(24,62,24);
	}

	// Send the server difficulty:
	{
		cPacketizer Pkt(*this, pktDifficulty);
		Pkt.WriteBEInt8(1);
	}

	// Send player abilities:
	SendPlayerAbilities();
}

#define HANDLE_READ(ByteBuf, Proc, Type, Var) \
	Type Var; \
	do { \
		if (!ByteBuf.Proc(Var))\
		{\
			return;\
		} \
	} while (false)
void cProtocol::HandlePacketPluginMessage(cByteBuffer & a_ByteBuffer)
{
	HANDLE_READ(a_ByteBuffer, ReadVarUTF8String, AString, Channel);

	//// If the plugin channel is recognized vanilla, handle it directly:
	//if (Channel.substr(0, 3) == "MC|")
	//{
		HandleVanillaPluginMessage(a_ByteBuffer, Channel);

	//	// Skip any unread data (vanilla sometimes sends garbage at the end of a packet; #1692):
	//	if (a_ByteBuffer.GetReadableSpace() > 1)
	//	{
	//		LOGD("Protocol 1.8: Skipping garbage data at the end of a vanilla PluginMessage packet, %u bytes",
	//			static_cast<unsigned>(a_ByteBuffer.GetReadableSpace() - 1)
	//		);
	//		a_ByteBuffer.SkipRead(a_ByteBuffer.GetReadableSpace() - 1);
	//	}

	//	return;
	//}

	//// Read the plugin message and relay to clienthandle:
	//AString Data;
	//VERIFY(a_ByteBuffer.ReadString(Data, a_ByteBuffer.GetReadableSpace() - 1));  // Always succeeds
	//m_Client->HandlePluginMessage(Channel, Data);
}

void cProtocol::SendChunkData(const std::string_view a_ChunkData)
{
	ASSERT(m_State == 3);  // In game mode?

	cCSLock Lock(m_CSPacket);
	SendData(a_ChunkData.data(), a_ChunkData.size());
}

void cProtocol::HandleVanillaPluginMessage(cByteBuffer & a_ByteBuffer, const AString & a_Channel)
{
	if (a_Channel == "MC|AdvCdm")
	{
		HANDLE_READ(a_ByteBuffer, ReadBEUInt8, UInt8, Mode);
		switch (Mode)
		{
			case 0x00:
			{
				HANDLE_READ(a_ByteBuffer, ReadBEInt32, Int32, BlockX);
				HANDLE_READ(a_ByteBuffer, ReadBEInt32, Int32, BlockY);
				HANDLE_READ(a_ByteBuffer, ReadBEInt32, Int32, BlockZ);
				HANDLE_READ(a_ByteBuffer, ReadVarUTF8String, AString, Command);
				//m_Client->HandleCommandBlockBlockChange(BlockX, BlockY, BlockZ, Command);
				break;
			}

			default:
			{
				//m_Client->SendChat(Printf("Failure setting command block command; unhandled mode %u (0x%02x)", Mode, Mode), mtFailure);
				LOG("Unhandled MC|AdvCdm packet mode.");
				return;
			}
		}  // switch (Mode)
		return;
	}
	else if (a_Channel == "MC|Brand")
	{
		HANDLE_READ(a_ByteBuffer, ReadVarUTF8String, AString, Brand);
		//m_Client->SetClientBrand(Brand);
		// Send back our brand, including the length:
		SendPluginMessage("MC|Brand", "\x08""Cuberite");
		return;
	}
}

void cProtocol::AddReceivedData(cByteBuffer & a_Buffer, const char * a_Data, size_t a_Size)
{
	// Write the incoming data into the comm log file:
	//将传入数据写入通信日志文件：
	if (g_ShouldLogCommIn && m_CommLogFile.IsOpen())
	{
		if (a_Buffer.GetReadableSpace() > 0)
		{
			AString AllData;
			size_t OldReadableSpace = a_Buffer.GetReadableSpace();
			a_Buffer.ReadAll(AllData);
			a_Buffer.ResetRead();
			a_Buffer.SkipRead(a_Buffer.GetReadableSpace() - OldReadableSpace);
			ASSERT(a_Buffer.GetReadableSpace() == OldReadableSpace);
			AString Hex;
			CreateHexDump(Hex, AllData.data(), AllData.size(), 16);
			m_CommLogFile.Printf("Incoming data, %zu (0x%zx) unparsed bytes already present in buffer:\n%s\n",
				AllData.size(), AllData.size(), Hex.c_str()
			);
		}
		AString Hex;
		CreateHexDump(Hex, a_Data, a_Size, 16);
		m_CommLogFile.Printf("Incoming data: %u (0x%x) bytes: \n%s\n",
			static_cast<unsigned>(a_Size), static_cast<unsigned>(a_Size), Hex.c_str()
		);
		m_CommLogFile.Flush();
	}

	if (!a_Buffer.Write(a_Data, a_Size))
	{
		// Too much data in the incoming queue, report to caller:
		//传入队列中的数据太多，请向调用者报告：
		//m_Client->PacketBufferFull();
		return;
	}
	// Handle all complete packets:
	//处理所有完整的数据包：
	for (;;)
	{
		UInt32 PacketLen;
		if (!a_Buffer.ReadVarInt(PacketLen))
		{
			// Not enough data
			//数据不足
			a_Buffer.ResetRead();
			break;
		}
		if (!a_Buffer.CanReadBytes(PacketLen))
		{
			// The full packet hasn't been received yet
			a_Buffer.ResetRead();
			break;
		}

		// Check packet for compression:
		UInt32 UncompressedSize = 0;
		AString UncompressedData;
		if (m_State == 3)
		{
			UInt32 NumBytesRead = static_cast<UInt32>(a_Buffer.GetReadableSpace());

			if (!a_Buffer.ReadVarInt(UncompressedSize))
			{
				//m_Client->Kick("Compression packet incomplete");
				return;
			}

			NumBytesRead -= static_cast<UInt32>(a_Buffer.GetReadableSpace());  // How many bytes has the UncompressedSize taken up?
			ASSERT(PacketLen > NumBytesRead);
			PacketLen -= NumBytesRead;

			if (UncompressedSize > 0)
			{
				// Decompress the data:
				AString CompressedData;
				VERIFY(a_Buffer.ReadString(CompressedData, PacketLen));
				if (InflateString(CompressedData.data(), PacketLen, UncompressedData) != Z_OK)
				{
					//m_Client->Kick("Compression failure");
					return;
				}
				PacketLen = static_cast<UInt32>(UncompressedData.size());
				if (PacketLen != UncompressedSize)
				{
					//m_Client->Kick("Wrong uncompressed packet size given");
					return;
				}
			}
		}
		// Move the packet payload to a separate cByteBuffer, bb:
		//将数据包有效负载移动到单独的cByteBuffer，bb:
		cByteBuffer bb(PacketLen + 1);
		if (UncompressedSize == 0)
		{
			// No compression was used, move directly
			//未使用压缩，直接移动
			VERIFY(a_Buffer.ReadToByteBuffer(bb, static_cast<size_t>(PacketLen)));
		}
		else
		{
			// Compression was used, move the uncompressed data:
			//已使用压缩，请移动未压缩的数据：
			VERIFY(bb.Write(UncompressedData.data(), UncompressedData.size()));
		}
		a_Buffer.CommitRead();

		UInt32 PacketType;
		if (!bb.ReadVarInt(PacketType))
		{
			// Not enough data
			//数据不足
			break;
		}

		// Write one NUL extra, so that we can detect over-reads
		//多写一个NUL，这样我们就可以检测到过度读取
		bb.Write("\0", 1);

		if (!HandlePacket(bb, PacketType));

	}
}

#define HANDLE_READ(ByteBuf, Proc, Type, Var) \
	Type Var; \
	do { \
		if (!ByteBuf.Proc(Var))\
		{\
			return;\
		} \
	} while (false)

void cProtocol::HandlePacketStatusPing(cByteBuffer & a_ByteBuffer)
{
	HANDLE_READ(a_ByteBuffer, ReadBEInt64, Int64, Timestamp);

	cPacketizer Pkt(*this, pktPingResponse);
	Pkt.WriteBEInt64(Timestamp);
}

void cProtocol::HandlePacketStatusRequest(cByteBuffer & a_ByteBuffer)
{
	cServer * Server = cRoot::Get()->GetServer();
	AString ServerDescription = Server->GetDescription();
	auto NumPlayers = static_cast<signed>(Server->GetNumPlayers());
	auto MaxPlayers = static_cast<signed>(Server->GetMaxPlayers());
	AString Favicon = Server->GetFaviconData();
	//cRoot::Get()->GetPluginManager()->CallHookServerPing(*m_Client, ServerDescription, NumPlayers, MaxPlayers, Favicon);

	// Version:
	Json::Value Version;
	const auto ProtocolVersion = GetProtocolVersion();
	Version["name"] = "Cuberite " + cMultiVersionProtocol::GetVersionTextFromInt(ProtocolVersion);
	Version["protocol"] = static_cast<std::underlying_type_t<cProtocol::Version>>(ProtocolVersion);

	// Players:
	Json::Value Players;
	Players["online"] = NumPlayers;
	Players["max"] = MaxPlayers;
	// TODO: Add "sample"

	// Description:
	Json::Value Description;
	Description["text"] = ServerDescription.c_str();


	// Create the response:
	Json::Value ResponseValue;
	ResponseValue["version"] = Version;
	ResponseValue["players"] = Players;
	ResponseValue["description"] = Description;
	//m_Client->ForgeAugmentServerListPing(ResponseValue);
	if (!Favicon.empty())
	{
		//ResponseValue["favicon"] = Printf("data:image/png;base64,%s", Favicon.c_str());
	}

	// Serialize the response into a packet:
	//将响应序列化为数据包：
	cPacketizer Pkt(*this, pktStatusResponse);
	Pkt.WriteString(JsonUtils::WriteFastString(ResponseValue));
	
}

void cProtocol::HandlePacketLoginEncryptionResponse(cByteBuffer & a_ByteBuffer)
{
}

void cProtocol::HandlePacketLoginStart(cByteBuffer & a_ByteBuffer)
{
	AString Username;
	if (!a_ByteBuffer.ReadVarUTF8String(Username))
	{
		//m_Client->Kick("Bad username");
		return;
	}

	if (!m_Client->HandleHandshake(Username))
	{
		// The client is not welcome here, they have been sent a Kick packet already
		return;
	}

	cServer * Server = cRoot::Get()->GetServer();
	// If auth is required, then send the encryption request:
	//如果需要身份验证，则发送加密请求：
	//if (Server->ShouldAuthenticate())
	//{
	//	cPacketizer Pkt(*this, pktEncryptionRequest);
	//	Pkt.WriteString(Server->GetServerID());
	//	const AString & PubKeyDer = Server->GetPublicKeyDER();
	//	Pkt.WriteVarInt32(static_cast<UInt32>(PubKeyDer.size()));
	//	Pkt.WriteBuf(PubKeyDer.data(), PubKeyDer.size());
	//	Pkt.WriteVarInt32(4);
	//	Pkt.WriteBEInt32(static_cast<int>(reinterpret_cast<intptr_t>(this)));  // Using 'this' as the cryptographic nonce, so that we don't have to generate one each time :)
	//	m_Client->SetUsername(Username);
	//	return;
	//}
		struct MemCallbacks:
		cAllocationPool<cChunkData::sChunkSection>::cStarvationCallbacks
	{
		virtual void OnStartUsingReserve() override {}
		virtual void OnEndUsingReserve() override {}
		virtual void OnOutOfReserve() override {}
	};
	m_Client->HandleLogin(Username);
	cListAllocationPool<cChunkData::sChunkSection> *m_Pool = new cListAllocationPool<cChunkData::sChunkSection>(cpp14::make_unique<MemCallbacks>(), 0, cChunkData::NumSections);
	cChunkData *CD = new cChunkData(*m_Pool);
		CD->m_Sections[3] = new cChunkData::sChunkSection;
		memset((void *)CD->m_Sections[3]->m_BlockTypes, 35, 257);
		memset((void *)CD->m_Sections[3]->m_BlockMetas, 0, sizeof(CD->m_Sections[1]->m_BlockMetas));
		memset((void *)CD->m_Sections[3]->m_BlockLight, 15, sizeof(CD->m_Sections[1]->m_BlockLight));
		memset((void *)CD->m_Sections[3]->m_BlockSkyLight, 15, sizeof(CD->m_Sections[1]->m_BlockSkyLight));

	eDimension *d = new eDimension();
	cChunkDataSerializer *cz = new cChunkDataSerializer(0,0,*CD,(const unsigned char *)new char(1),*d);
	
	std::vector<cClientHandle *> * a_SendTo = new std::vector<cClientHandle *>();
	a_SendTo->push_back(m_Client);
	cz->Serialize110(a_SendTo);

	

}

UInt32 cProtocol::GetPacketID(ePacketType a_Packet)
{
	switch (a_Packet)
	{
		//ping阶段
		case pktStatusResponse:		 return 0x00;//获取服务器列表信息
		case pktPingResponse:        return 0x01;//ping信息
		//登录阶段
		case pktLoginSuccess:          return 0x02;//登录成功
		case pktStartCompression:      return 0x03;//开始压缩
		case pktJoinGame:              return 0x23;//加入游戏
		case pktSpawnPosition:       return 0x46;//玩家位置与角度
		case pktDifficulty:            return 0x0d;//难度
		case pktPlayerAbilities:     return 0x2c;//玩家能力（客户端）
		case pktPluginMessage:         return 0x18;//
	}
}

void cProtocol::SendPacket(cPacketizer & a_Packet)
{
	UInt32 PacketLen = static_cast<UInt32>(m_OutPacketBuffer.GetUsedSpace());
	AString PacketData, CompressedPacket;
	m_OutPacketBuffer.ReadAll(PacketData);
	m_OutPacketBuffer.CommitRead();

	if (m_State == 3)
	{
		// Compress the packet payload:
		//压缩数据包有效负载：
		if (!cProtocol::CompressPacket(PacketData, CompressedPacket))
		{
			return;
		}


		// Send the packet's payload compressed:
		//发送压缩的数据包有效负载：
		SendData(CompressedPacket.data(), CompressedPacket.size());
	}
	else
	{
		// Compression doesn't apply to this state, send raw data:
		//压缩不适用于此状态，请发送原始数据：
		m_OutPacketLenBuffer.WriteVarInt32(PacketLen);
		AString LengthData;
		m_OutPacketLenBuffer.ReadAll(LengthData);
		SendData(LengthData.data(), LengthData.size());//要先发送数据包大小

		// Send the packet's payload directly:
		//直接发送数据包的有效负载：
		m_OutPacketLenBuffer.CommitRead();
		SendData(PacketData.data(), PacketData.size());
	}
}

void cProtocol::SendData(const char * a_Data, size_t a_Size)
{
	m_Client->SendData(a_Data, a_Size);
}

bool cProtocol::CompressPacket(const AString & a_Packet, AString & a_CompressedData)
{
	const auto UncompressedSize = static_cast<size_t>(a_Packet.size());

	if (UncompressedSize < CompressionThreshold)
	{
		/* Size doesn't reach threshold, not worth compressing.

		--------------- Packet format ----------------
		|--- Header ---------------------------------|
		| PacketSize: Size of all fields below       |
		| DataSize: Zero, means below not compressed |
		|--- Body -----------------------------------|
		| a_Packet: copy of uncompressed data        |
		----------------------------------------------
		*/
		const UInt32 DataSize = 0;
		const auto PacketSize = cByteBuffer::GetVarIntSize(DataSize) + UncompressedSize;

		cByteBuffer LengthHeaderBuffer(
			cByteBuffer::GetVarIntSize(PacketSize) +
			cByteBuffer::GetVarIntSize(DataSize)
		);

		LengthHeaderBuffer.WriteVarInt32(PacketSize);
		LengthHeaderBuffer.WriteVarInt32(DataSize);

		AString LengthData;
		LengthHeaderBuffer.ReadAll(LengthData);

		a_CompressedData.reserve(LengthData.size() + UncompressedSize);
		a_CompressedData.append(LengthData.data(), LengthData.size());
		a_CompressedData.append(a_Packet);

		return true;
	}

	/* Definitely worth compressing.

	--------------- Packet format ----------------
	|--- Header ---------------------------------|
	| PacketSize: Size of all fields below       |
	| DataSize: Size of uncompressed a_Packet    |
	|--- Body -----------------------------------|
	| CompressedData: compressed a_Packet        |
	----------------------------------------------
	*/

	// Compress the data:
	char CompressedData[MAX_COMPRESSED_PACKET_LEN];

	uLongf CompressedSize = compressBound(static_cast<uLongf>(a_Packet.size()));
	if (CompressedSize >= MAX_COMPRESSED_PACKET_LEN)
	{
		ASSERT(!"Too high packet size.");
		return false;
	}

	if (
		compress2(
			reinterpret_cast<Bytef *>(CompressedData), &CompressedSize,
			reinterpret_cast<const Bytef *>(a_Packet.data()), static_cast<uLongf>(a_Packet.size()), Z_DEFAULT_COMPRESSION
		) != Z_OK
	)
	{
		return false;
	}

	const UInt32 DataSize = UncompressedSize;
	const auto PacketSize = cByteBuffer::GetVarIntSize(DataSize) + CompressedSize;

	cByteBuffer LengthHeaderBuffer(
		cByteBuffer::GetVarIntSize(PacketSize) +
		cByteBuffer::GetVarIntSize(DataSize)
	);

	LengthHeaderBuffer.WriteVarInt32(PacketSize);
	LengthHeaderBuffer.WriteVarInt32(DataSize);

	AString LengthData;
	LengthHeaderBuffer.ReadAll(LengthData);

	a_CompressedData.reserve(LengthData.size() + DataSize);
	a_CompressedData.append(LengthData.data(), LengthData.size());
	a_CompressedData.append(CompressedData, CompressedSize);

	return true;
}

cProtocol::Version cProtocol::GetProtocolVersion()
{
	return Version::Version_1_12_2;
}

void cProtocol::SendPlayerAbilities(void)
{
	ASSERT(m_State == 3);  // In game mode?

	cPacketizer Pkt(*this, pktPlayerAbilities);
	Byte Flags = 0;
	cPlayer * Player = m_Client->GetPlayer();
	//if (Player->IsGameModeCreative())
	//{
	//	Flags |= 0x01;
	//	Flags |= 0x08;  // Godmode, used for creative
	//}
	//if (Player->IsFlying())
	//{
	//	Flags |= 0x02;
	//}
	//if (Player->CanFly())
	//{
	//	Flags |= 0x04;
	//}
	Pkt.WriteBEUInt8(15);
	Pkt.WriteBEFloat(0.05);
	Pkt.WriteBEFloat(0.1);
}



bool cProtocol::HandlePacket(cByteBuffer & a_ByteBuffer, UInt32 a_PacketType)
{
	switch (m_State)
	{
		case 1:
		{
			// Status
			switch (a_PacketType)
			{
				case 0x00: HandlePacketStatusRequest(a_ByteBuffer); return true;
				case 0x01: HandlePacketStatusPing(a_ByteBuffer); return true;
			}
			break;
		}

		case 2:
		{
			// Login
			switch (a_PacketType)
			{
				case 0x00: HandlePacketLoginStart(a_ByteBuffer); return true;
				case 0x01: HandlePacketLoginEncryptionResponse(a_ByteBuffer); return true;
			}
			break;
		}

		case 3:
		{
			// Game
			switch (a_PacketType)
			{
				case 0x09: HandlePacketPluginMessage(a_ByteBuffer); return true;
			}
			break;
		}
	}
}
