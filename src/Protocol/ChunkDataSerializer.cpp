#include "Globals.h"
#include "ChunkDataSerializer.h"
#include "zlib/zlib.h"
#include "Protocol.h"
#include "../ByteBuffer.h"
#include "../ClientHandle.h"







/** Calls the given function with every present chunk section. */
/**使用每个当前块段调用给定函数*/
template <class Func>
void ForEachSection(const cChunkData & a_Data, Func a_Func)
{
	for (size_t SectionIdx = 0; SectionIdx < cChunkData::NumSections;
		 ++SectionIdx)
	{
		auto Section = a_Data.GetSection(SectionIdx);
		if (Section != nullptr)
		{
			a_Func(*Section);
		}
	}
}





////////////////////////////////////////////////////////////////////////////////
// cChunkDataSerializer:
//块数据序列化程序
cChunkDataSerializer::cChunkDataSerializer(
	int a_ChunkX, int a_ChunkZ, const cChunkData & a_Data,
	const unsigned char * a_BiomeData, const eDimension a_Dimension) :
	m_ChunkX(a_ChunkX),
	m_ChunkZ(a_ChunkZ),
	m_Data(a_Data),
	m_BiomeData(a_BiomeData),
	m_Dimension(a_Dimension)
{
}





void cChunkDataSerializer::SendToClients(
	const std::unordered_set<cClientHandle *> & a_SendTo)
{
	std::unordered_map<cProtocol::Version, std::vector<cClientHandle *>>
		ClientProtocolVersions;

	for (const auto Client : a_SendTo)
	{
		const auto ClientProtocol =
			static_cast<cProtocol::Version>(Client->GetProtocolVersion());
		ClientProtocolVersions[ClientProtocol].emplace_back(Client);
	}

	for (const auto & Entry : ClientProtocolVersions)
	{

				Serialize110(&Entry.second);

	}
}









void cChunkDataSerializer::Serialize110(
	const std::vector<cClientHandle *> * a_SendTo)
{


	// This function returns the fully compressed packet (including packet
	// size), not the raw packet!
	//此函数返回完全压缩的数据包（包括数据包大小），而不是原始数据包！
	// Create the packet:
	//创建数据包：
	cByteBuffer Packet(512 KiB);
	Packet.WriteVarInt32(0x20);  // Packet id (Chunk Data packet) //数据包id（数据块数据包）
	Packet.WriteBEInt32(m_ChunkX);
	Packet.WriteBEInt32(m_ChunkZ);
	Packet.WriteBool(
		true);  // "Ground-up continuous", or rather, "biome data present" flag
				// //“地面向上连续”，或者更确切地说，“生物群落数据显示”标志
	Packet.WriteVarInt32(m_Data.GetSectionBitmask());
	// Write the chunk size:
	//写入块大小：
	const size_t BitsPerEntry = 13;
	const size_t Mask =(1 << BitsPerEntry) -1;  // Creates a mask that is 13 bits long, ie 0b1111111111111 //创建13位长的掩码，即0B1111111111
	const size_t ChunkSectionDataArraySize = (cChunkData::SectionBlockCount * BitsPerEntry) / 8 /8;  // Convert from bit count to long count //从位计数转换为长计数
	size_t ChunkSectionSize =
		(1 +  // Bits per block - set to 13, so the global palette is used and //每块位-设置为13，因此使用全局调色板
			  // the palette has a length of 0 //调色板的长度为0
		 1 +  // Palette length //调色板长度
		 2 +  // Data array length VarInt - 2 bytes for the current value //数据数组长度变量-当前值为2字节
		 ChunkSectionDataArraySize * 8 +  // Actual block data - multiplied by 8 //实际块数据-乘以8
										  // because first number is longs //因为第一个数字是长的
		 cChunkData::SectionBlockCount / 2  // Block light //遮光灯
		);

	if (m_Dimension == dimOverworld)
	{
		// Sky light is only sent in the overworld. //天光只在超世界发出。
		ChunkSectionSize += cChunkData::SectionBlockCount / 2;
	}

	const size_t BiomeDataSize = cChunkDef::Width * cChunkDef::Width;
	size_t ChunkSize =
		(ChunkSectionSize * m_Data.NumPresentSections() + BiomeDataSize);
	Packet.WriteVarInt32(static_cast<UInt32>(ChunkSize));

	// Write each chunk section...
	//写下每个区块部分。。。
	int a = 0;
	ForEachSection(m_Data, [&](const cChunkData::sChunkSection & a_Section) {
		//if (a < 3)
		//{
		//	//memset((void *)a_section.m_blocktypes, 0, sizeof(a_section.m_blocktypes));
		//	//memset((void *)a_section.m_blockmetas, 0, sizeof(a_section.m_blockmetas));
		//	memset((void *)a_section.m_blocklight, 15, sizeof(a_section.m_blocklight));
		//	memset((void *)a_section.m_blockskylight, 15, sizeof(a_section.m_blockskylight));
		//}
		//else
		//{
		//	//memset((void *)a_section.m_blocktypes, 0, sizeof(a_section.m_blocktypes));
		//	//memset((void *)a_section.m_blocktypes, 35, 257);
		//	//memset((void *)a_section.m_blockmetas, 0, sizeof(a_section.m_blockmetas));
		//	memset((void *)a_section.m_blocklight, 15, sizeof(a_section.m_blocklight));
		//	memset((void *)a_section.m_blockskylight, 15, sizeof(a_section.m_blockskylight));
		//}

		//a++;


		Packet.WriteBEUInt8(static_cast<UInt8>(BitsPerEntry));
		Packet.WriteVarInt32(0);  // Palette length is 0 //调色板长度为0
		Packet.WriteVarInt32(static_cast<UInt32>(
			ChunkSectionDataArraySize));  // 块段数据数组大小

		UInt64 TempLong = 0;  // Temporary value that will be stored into /将存储到中的临时值
		UInt64 CurrentlyWrittenIndex = 0;  // "Index" of the long that would be written to //“索引”将要写入的long

		for (size_t Index = 0; Index < cChunkData::SectionBlockCount; Index++)
		{
			UInt64 Value = static_cast<UInt64>(a_Section.m_BlockTypes[Index] << 4);
			if (Index % 2 == 0)
			{
				Value |= a_Section.m_BlockMetas[Index/2] & 0x0f;
			}
			else
			{
				Value |= a_Section.m_BlockMetas[Index/2] >> 4;
			}
			Value &= Mask;  // It shouldn't go out of bounds, but it's still
							// worth being careful
							//它不应该出界，但它仍然是

							//值得小心
			// Painful part where we write data into the long array.  Based off
			// of the normal code.
			// 将数据写入长数组的痛苦部分。基于

			// 正常代码的一部分。


			size_t BitPosition = Index * BitsPerEntry;
			size_t FirstIndex = BitPosition / 64;
			size_t SecondIndex = ((Index + 1) * BitsPerEntry - 1) / 64;
			size_t BitOffset = BitPosition % 64;

			if (FirstIndex != CurrentlyWrittenIndex)
			{
				// Write the current data before modifiying it.
				//在修改前写入当前数据。
				Packet.WriteBEUInt64(TempLong);
				TempLong = 0;
				CurrentlyWrittenIndex = FirstIndex;
			}

			TempLong |= (Value << BitOffset);

			if (FirstIndex != SecondIndex)
			{
				// Part of the data is now in the second long; write the first
				// one first
				//部分数据现在处于第二长；先写第一个
				Packet.WriteBEUInt64(TempLong);
				CurrentlyWrittenIndex = SecondIndex;

				TempLong = (Value >> (64 - BitOffset));
			}
		}
		// The last long will generally not be written
		//最后一段一般不会写
		Packet.WriteBEUInt64(TempLong);

		// Write lighting:
		//书写照明：
		Packet.WriteBuf(a_Section.m_BlockLight, sizeof(a_Section.m_BlockLight));
		if (m_Dimension == dimOverworld)
		{
			// Skylight is only sent in the overworld; the nether and end do not
			// use it
			//天光只在超世界发出；阴间和尽头不使用它
			Packet.WriteBuf(a_Section.m_BlockSkyLight, sizeof(a_Section.m_BlockSkyLight));
		}
	});

	// Write the biome data
	//写生物群落数据
	Packet.WriteBuf(m_BiomeData, BiomeDataSize);
	//Packet.WriteBuf(new char{}, 256);
	// Identify 1.9.4's tile entity list as empty
	//将1.9.4的瓷砖实体列表标识为空
	Packet.WriteBEUInt8(0);

	CompressAndSend(Packet, *a_SendTo);
}



inline void cChunkDataSerializer::WriteSectionDataSeamless(cByteBuffer & a_Packet, const cChunkData::sChunkSection & a_Section, const UInt8 a_BitsPerEntry)
{
}

void cChunkDataSerializer::CompressAndSend(
	cByteBuffer & a_Packet, const std::vector<cClientHandle *> & a_SendTo)
{
	AString PacketData;
	a_Packet.ReadAll(PacketData);

	AString ToSend;
	if (!cProtocol::CompressPacket(PacketData, ToSend))
	{
		ASSERT(!"Packet compression failed.");
		return;
	}

	for (const auto Client : a_SendTo)
	{
		Client->SendChunkData(m_ChunkX, m_ChunkZ, ToSend);
	}
}
