
// Protocol.h

#include "Globals.h"
// Interfaces to the cProtocol class representing the generic interface that a protocol
// parser and serializer must implement
// cProtocol类的接口，该类表示协议所包含的通用接口

//解析器和序列化程序必须实现




#pragma once

#include "../Defines.h"
//#include "../Scoreboard.h"
#include "../ByteBuffer.h"
//#include "../EffectID.h"
//#include "../World.h"





class cMap;
class cExpOrb;
class cPlayer;
class cEntity;
class cWindow;
class cPickup;
class cPainting;
class cWorld;
class cMonster;
class cChunkDataSerializer;
class cFallingBlock;
class cCompositeChat;
class cStatManager;
class cPacketizer;

class cClientHandle;



typedef unsigned char Byte;





class cProtocol
{
public:
	cProtocol(cClientHandle * a_Client, const AString & a_ServerAddress, UInt16 a_ServerPort, UInt32 a_State) :
		m_Client(a_Client),
		m_OutPacketBuffer(64 KiB),
		m_OutPacketLenBuffer(20),
		m_ServerAddress(a_ServerAddress),
		m_ServerPort(a_ServerPort),
		m_State(a_State),
		m_IsEncrypted(false)// 20 bytes is more than enough for one VarInt
	{
		
	}

	~cProtocol() {}
	AString m_ServerAddress;
	UInt16 m_ServerPort;
	bool m_IsEncrypted;

	/** The logfile where the comm is logged, when g_ShouldLogComm is true */
	/**当g_ShouldLogComm为true时，记录通信的日志文件*/
	cFile m_CommLogFile;



	virtual void SendLoginSuccess               (void);
	virtual AString GetAuthServerID(void){ return m_AuthServerID; }
	AString m_AuthServerID;
   /** Logical types of outgoing packets.
   These values get translated to on-wire packet IDs in GetPacketID(), specific for each protocol.
   This is mainly useful for protocol sub-versions that re-number the packets while using mostly the same packet layout. */
   /**传出数据包的逻辑类型。

   这些值将转换为GetPacketID（）中的在线数据包ID，具体针对每个协议。

   这主要适用于协议子版本，这些协议子版本在使用基本相同的数据包布局时对数据包重新编号*/
	enum ePacketType
	{
		pktAttachEntity,
		pktBlockAction,
		pktBlockBreakAnim,
		pktBlockChange,
		pktBlockChanges,
		pktCameraSetTo,
		pktChatRaw,
		pktCollectEntity,
		pktDestroyEntity,
		pktDifficulty,
		pktDisconnectDuringLogin,
		pktDisconnectDuringGame,
		pktDisplayObjective,
		pktEditSign,
		pktEncryptionRequest,
		pktEntityAnimation,
		pktEntityEffect,
		pktEntityEquipment,
		pktEntityHeadLook,
		pktEntityLook,
		pktEntityMeta,
		pktEntityProperties,
		pktEntityRelMove,
		pktEntityRelMoveLook,
		pktEntityStatus,
		pktEntityVelocity,
		pktExperience,
		pktExplosion,
		pktGameMode,
		pktHeldItemChange,
		pktInventorySlot,
		pktJoinGame,
		pktKeepAlive,
		pktLeashEntity,
		pktLoginSuccess,
		pktMapData,
		pktParticleEffect,
		pktPingResponse,
		pktPlayerAbilities,
		pktPlayerList,
		pktPlayerMaxSpeed,
		pktPlayerMoveLook,
		pktPluginMessage,
		pktRemoveEntityEffect,
		pktResourcePack,
		pktRespawn,
		pktScoreboardObjective,
		pktSpawnObject,
		pktSoundEffect,
		pktSoundParticleEffect,
		pktSpawnExperienceOrb,
		pktSpawnGlobalEntity,
		pktSpawnMob,
		pktSpawnOtherPlayer,
		pktSpawnPainting,
		pktSpawnPosition,
		pktStartCompression,
		pktStatistics,
		pktStatusResponse,
		pktTabCompletionResults,
		pktTeleportEntity,
		pktTimeUpdate,
		pktTitle,
		pktUnloadChunk,
		pktUnlockRecipe,
		pktUpdateBlockEntity,
		pktUpdateHealth,
		pktUpdateScore,
		pktUpdateSign,
		pktUseBed,
		pktWeather,
		pktWindowItems,
		pktWindowClose,
		pktWindowOpen,
		pktWindowProperty
	};

	enum class eEntityMetadata //实体元数据
	{
		EntityFlags,
		EntityAir,
		EntityCustomName,
		EntityCustomNameVisible,
		EntitySilent,
		EntityNoGravity,
		EntityPose,

		PotionThrown,

		FallingBlockPosition,

		AreaEffectCloudRadius,
		AreaEffectCloudColor,
		AreaEffectCloudSinglePointEffect,
		AreaEffectCloudParticleId,
		AreaEffectCloudParticleParameter1,
		AreaEffectCloudParticleParameter2,

		ArrowFlags,
		TippedArrowColor,

		BoatLastHitTime,
		BoatForwardDirection,
		BoatDamageTaken,
		BoatType,
		BoatLeftPaddleTurning,
		BoatRightPaddleTurning,
		BoatSplashTimer,

		EnderCrystalBeamTarget,
		EnderCrystalShowBottom,

		WitherSkullInvulnerable,

		FireworkInfo,
		FireworkBoostedEntityId,

		ItemFrameItem,
		ItemFrameRotation,

		ItemItem,

		LivingActiveHand,
		LivingHealth,
		LivingPotionEffectColor,
		LivingPotionEffectAmbient,
		LivingNumberOfArrows,

		PlayerAdditionalHearts,
		PlayerScore,
		PlayerDisplayedSkinParts,
		PlayerMainHand,

		ArmorStandStatus,
		ArmorStandHeadRotation,
		ArmorStandBodyRotation,
		ArmorStandLeftArmRotation,
		ArmorStandRightArmRotation,
		ArmorStandLeftLegRotation,
		ArmorStandRightLegRotation,

		InsentientFlags,

		BatHanging,

		AgeableIsBaby,

		AbstractHorseFlags,
		AbstractHorseOwner,

		HorseVariant,
		HorseArmour,

		ChestedHorseChested,

		LlamaStrength,
		LlamaCarpetColor,
		LlamaVariant,

		PigHasSaddle,
		PigTotalCarrotOnAStickBoost,

		RabbitType,

		PolarBearStanding,

		SheepFlags,

		TameableAnimalFlags,
		TameableAnimalOwner,

		OcelotType,

		WolfDamageTaken,
		WolfBegging,
		WolfCollarColour,

		VillagerProfession,

		IronGolemPlayerCreated,

		ShulkerFacingDirection,
		ShulkerAttachmentFallingBlockPosition,
		ShulkerShieldHeight,

		BlazeOnFire,

		CreeperState,
		CreeperPowered,
		CreeperIgnited,

		GuardianStatus,
		GuardianTarget,

		IllagerFlags,
		SpeIlagerSpell,

		VexFlags,

		AbstractSkeletonArmsSwinging,

		SpiderClimbing,

		WitchAggresive,

		WitherFirstHeadTarget,
		WitherSecondHeadTarget,
		WitherThirdHeadTarget,
		WitherInvulnerableTimer,

		ZombieIsBaby,
		ZombieUnusedWasType,
		ZombieHandsRisedUp,

		ZombieVillagerConverting,
		ZombieVillagerProfession,

		EndermanCarriedBlock,
		EndermanScreaming,

		EnderDragonDragonPhase,

		GhastAttacking,

		SlimeSize,

		MinecartShakingPower,
		MinecartShakingDirection,
		MinecartShakingMultiplier,
		MinecartBlockIDMeta,
		MinecartBlockY,
		MinecartShowBlock,

		MinecartCommandBlockCommand,
		MinecartCommandBlockLastOutput,

		MinecartFurnacePowered,

		TNTPrimedFuseTime
	};

	enum class eEntityMetadataType //实体元数据类型
	{
		Byte,
		VarInt,
		Float,
		String,
		Chat,
		OptChat,
		Item,
		Boolean,
		Rotation,
		Position,
		OptPosition,
		Direction,
		OptUUID,
		OptBlockID,
		NBT,
		Particle,
		VillagerData,
		OptVarInt,
		Pose
	};

	enum class Version
	{
		Version_1_8_0 = 47,
		Version_1_9_0 = 107,
		Version_1_9_1 = 108,
		Version_1_9_2 = 109,
		Version_1_9_4 = 110,
		Version_1_10_0 = 210,
		Version_1_11_0 = 315,
		Version_1_11_1 = 316,
		Version_1_12 = 335,
		Version_1_12_1 = 338,
		Version_1_12_2 = 340,
		Version_1_13 = 393,
		Version_1_13_1 = 401,
		Version_1_13_2 = 404,  // TODO: this constant should be in WebServer //TODO:此常量应位于Web服务器中
		Version_1_14 = 477
	};

	/** Called when client sends some data */
	/**当客户端发送一些数据时调用*/
	void DataReceived(cByteBuffer & a_Buffer, const char * a_Data, size_t a_Size);

   // Sending stuff to clients (alphabetically sorted):
   //向客户端发送内容（按字母顺序排列）：
   // void SendAttachEntity               (const cEntity & a_Entity, const cEntity & a_Vehicle);
   //// void SendBlockAction                (int a_BlockX, int a_BlockY, int a_BlockZ, char a_Byte1, char a_Byte2, BLOCKTYPE a_BlockType);
   // void SendBlockBreakAnim             (UInt32 a_EntityID, int a_BlockX, int a_BlockY, int a_BlockZ, char a_Stage);
   //// void SendBlockChange                (int a_BlockX, int a_BlockY, int a_BlockZ, BLOCKTYPE a_BlockType, NIBBLETYPE a_BlockMeta);
   // void SendBlockChanges               (int a_ChunkX, int a_ChunkZ, const sSetBlockVector & a_Changes);
   // void SendCameraSetTo                (const cEntity & a_Entity);
   // void SendChat                       (const AString & a_Message, eChatType a_Type);
   // void SendChat                       (const cCompositeChat & a_Message, eChatType a_Type, bool a_ShouldUseChatPrefixes);
   // void SendChatRaw                    (const AString & a_MessageRaw, eChatType a_Type);
   // void SendChunkData                  (const std::string_view a_ChunkData);
   // void SendCollectEntity              (const cEntity & a_Collected, const cEntity & a_Collector, unsigned a_Count);
   // void SendDestroyEntity              (const cEntity & a_Entity);
   // void SendDetachEntity               (const cEntity & a_Entity, const cEntity & a_PreviousVehicle);
   // void SendDisconnect                 (const AString & a_Reason);
   // void SendEditSign                   (int a_BlockX, int a_BlockY, int a_BlockZ);  ///< Request the client to open up the sign editor for the sign (1.6+)
   // void SendEntityEffect               (const cEntity & a_Entity, int a_EffectID, int a_Amplifier, int a_Duration);
   // void SendEntityAnimation            (const cEntity & a_Entity, char a_Animation);
   //// void SendEntityEquipment            (const cEntity & a_Entity, short a_SlotNum, const cItem & a_Item);
   // void SendEntityHeadLook             (const cEntity & a_Entity);
   // void SendEntityLook                 (const cEntity & a_Entity);
   // void SendEntityMetadata             (const cEntity & a_Entity);
   // void SendEntityPosition             (const cEntity & a_Entity);
   // void SendEntityProperties           (const cEntity & a_Entity);
   // void SendEntityStatus               (const cEntity & a_Entity, char a_Status);
   // void SendEntityVelocity             (const cEntity & a_Entity);
   // void SendExplosion                  (double a_BlockX, double a_BlockY, double a_BlockZ, float a_Radius, const cVector3iArray & a_BlocksAffected, const Vector3d & a_PlayerMotion);
   // void SendGameMode                   (eGameMode a_GameMode);
   // void SendHealth                     (void);
   // void SendHeldItemChange             (int a_ItemIndex);
   // void SendHideTitle                  (void);
   //// void SendInventorySlot              (char a_WindowID, short a_SlotNum, const cItem & a_Item);
   // void SendKeepAlive                  (UInt32 a_PingID);
   // void SendLeashEntity                (const cEntity & a_Entity, const cEntity & a_EntityLeashedTo);
   // void SendLogin                      (const cPlayer & a_Player, const cWorld & a_World);
   // void SendLoginSuccess               (void);
   // void SendMapData                    (const cMap & a_Map, int a_DataStartX, int a_DataStartY);
   // void SendPaintingSpawn              (const cPainting & a_Painting);
   // void SendPlayerAbilities            (void);
   // void SendParticleEffect             (const AString & a_SoundName, float a_SrcX, float a_SrcY, float a_SrcZ, float a_OffsetX, float a_OffsetY, float a_OffsetZ, float a_ParticleData, int a_ParticleAmount);
   // void SendParticleEffect             (const AString & a_SoundName, Vector3f a_Src, Vector3f a_Offset, float a_ParticleData, int a_ParticleAmount, std::array<int, 2> a_Data);
   // void SendPlayerListAddPlayer        (const cPlayer & a_Player);
   // void SendPlayerListRemovePlayer     (const cPlayer & a_Player);
   // void SendPlayerListUpdateGameMode   (const cPlayer & a_Player);
   // void SendPlayerListUpdatePing       (const cPlayer & a_Player);
   // void SendPlayerListUpdateDisplayName(const cPlayer & a_Player, const AString & a_CustomName);
   // void SendPlayerMaxSpeed             (void);  ///< Informs the client of the maximum player speed (1.6.1+)
   // void SendPlayerMoveLook             (void);
   // void SendPlayerPosition             (void);
   // void SendPlayerSpawn                (const cPlayer & a_Player);
   // void SendPluginMessage              (const AString & a_Channel, const AString & a_Message);
   // void SendRemoveEntityEffect         (const cEntity & a_Entity, int a_EffectID);
   // void SendResetTitle                 (void);
   // void SendResourcePack               (const AString & a_ResourcePackUrl);
   // void SendRespawn                    (eDimension a_Dimension);
   // void SendExperience                 (void);
   // void SendExperienceOrb              (const cExpOrb & a_ExpOrb);
   // void SendScoreboardObjective        (const AString & a_Name, const AString & a_DisplayName, Byte a_Mode);
   //// void SendScoreUpdate                (const AString & a_Objective, const AString & a_Player, cObjective::Score a_Score, Byte a_Mode);
   //// void SendDisplayObjective           (const AString & a_Objective, cScoreboard::eDisplaySlot a_Display);
   // void SendSetSubTitle                (const cCompositeChat & a_SubTitle);
   // void SendSetRawSubTitle             (const AString & a_SubTitle);
   // void SendSetTitle                   (const cCompositeChat & a_Title);
   // void SendSetRawTitle                (const AString & a_Title);
   // void SendSoundEffect                (const AString & a_SoundName, double a_X, double a_Y, double a_Z, float a_Volume, float a_Pitch);
   //// void SendSoundParticleEffect        (const EffectID a_EffectID, int a_SrcX, int a_SrcY, int a_SrcZ, int a_Data);
   // void SendSpawnEntity                (const cEntity & a_Entity);
   // void SendSpawnMob                   (const cMonster & a_Mob);
   // void SendStatistics                 (const cStatManager & a_Manager);
   // void SendTabCompletionResults       (const AStringVector & a_Results);
   // void SendThunderbolt                (int a_BlockX, int a_BlockY, int a_BlockZ);
   // void SendTitleTimes                 (int a_FadeInTicks, int a_DisplayTicks, int a_FadeOutTicks);
   // void SendTimeUpdate                 (Int64 a_WorldAge, Int64 a_TimeOfDay, bool a_DoDaylightCycle);
   // void SendUnleashEntity              (const cEntity & a_Entity);
   // void SendUnloadChunk                (int a_ChunkX, int a_ChunkZ);
   // void SendUpdateBlockEntity          (cBlockEntity & a_BlockEntity);
   // void SendUpdateSign                 (int a_BlockX, int a_BlockY, int a_BlockZ, const AString & a_Line1, const AString & a_Line2, const AString & a_Line3, const AString & a_Line4);
   // void SendUseBed                     (const cEntity & a_Entity, int a_BlockX, int a_BlockY, int a_BlockZ);
   // void SendUnlockRecipe               (UInt32 a_RecipeID);
   // void SendInitRecipes                (UInt32 a_RecipeID);
   // void SendWeather                    (eWeather a_Weather);
   // void SendWholeInventory             (const cWindow    & a_Window);
   // void SendWindowClose                (const cWindow    & a_Window);
   // void SendWindowOpen                 (const cWindow & a_Window);
   // void SendWindowProperty             (const cWindow & a_Window, short a_Property, short a_Value);
   virtual void SendPluginMessage              (const AString & a_Channel, const AString & a_Message);
	void SendLogin                      (const cPlayer & a_Player, const cWorld & a_World);
	virtual void HandlePacketPluginMessage          (cByteBuffer & a_ByteBuffer);
	virtual void SendChunkData                  (const std::string_view a_ChunkData);
	/** Parses Vanilla plugin messages into specific ClientHandle calls.
	The message payload is still in the bytebuffer, the handler reads it specifically for each handled channel */
	/**将普通插件消息解析为特定的ClientHandle调用。

	消息负载仍然在bytebuffer中，处理程序专门为每个处理的通道读取它*/
	virtual void HandleVanillaPluginMessage(cByteBuffer & a_ByteBuffer, const AString & a_Channel);
   ///** Returns the ServerID used for authentication through session.minecraft.net */
   ///**返回用于通过session.minecraft.net进行身份验证的服务器ID*/
   // AString GetAuthServerID(void);

   	/** Adds the received (unencrypted) data to m_ReceivedData, parses complete packets */
	/**将接收到的（未加密的）数据添加到m_ReceivedData，解析完整的数据包 */
	virtual void AddReceivedData(cByteBuffer & a_Buffer, const char * a_Data, size_t a_Size);

	/** Reads and handles the packet. The packet length and type have already been read.
	Returns true if the packet was understood, false if it was an unknown packet
	*/
	/**读取和处理数据包。已读取数据包长度和类型。

	如果数据包已被理解，则返回true；如果数据包是未知数据包，则返回false

	*/
	virtual bool HandlePacket(cByteBuffer & a_ByteBuffer, UInt32 a_PacketType);

	/** State of the protocol. 1 = status, 2 = login, 3 = game */
	/**议定书的状况。1=状态，2=登录，3=游戏*/
	UInt32 m_State;

	// Packet handlers while in the Status state (m_State == 1):
	//处于状态（m_state==1）时的数据包处理程序：
	virtual void HandlePacketStatusPing(cByteBuffer & a_ByteBuffer);
	virtual void HandlePacketStatusRequest(cByteBuffer & a_ByteBuffer);

	// Packet handlers while in the Login state (m_State == 2):
	//处于登录状态（m_state==2）时的数据包处理程序：
	virtual void HandlePacketLoginEncryptionResponse(cByteBuffer & a_ByteBuffer);
	virtual void HandlePacketLoginStart(cByteBuffer & a_ByteBuffer);


	virtual UInt32 GetPacketID(ePacketType a_Packet);
	/** Sends the packet to the client. Called by the cPacketizer's destructor. */
	/**将数据包发送到客户端。由打包机的析构函数调用*/
	virtual void SendPacket(cPacketizer & a_Packet);
	/** Sends the data to the client, encrypting them if needed. */
	/**将数据发送到客户端，必要时对其进行加密*/
	virtual void SendData(const char * a_Data, size_t a_Size);
	/** Compress the packet. a_Packet must be without packet length.
	a_Compressed will be set to the compressed packet includes packet length and data length.
	If compression fails, the function returns false. */
	/**压缩数据包。_数据包必须没有数据包长度。

	压缩后的数据包包括数据包长度和数据长度。

	如果压缩失败，函数将返回false*/
	static bool CompressPacket(const AString & a_Packet, AString & a_Compressed);
protected:

	friend class cPacketizer;

	cClientHandle * m_Client;

	/** Provides synchronization for sending the entire packet at once.
	Each SendXYZ() function must acquire this CS in order to send the whole packet at once.
	Automated via cPacketizer class. */
	/**提供一次发送整个数据包的同步。

	每个SendXYZ（）函数都必须获取此CS，以便一次发送整个数据包。

	通过cPacketizer类实现自动化*/
	cCriticalSection m_CSPacket;

	/** Buffer for composing the outgoing packets, through cPacketizer */
	/**通过cPacketizer构成传出数据包的缓冲区*/
	cByteBuffer m_OutPacketBuffer;

	/** Buffer for composing packet length (so that each cPacketizer instance doesn't allocate a new cPacketBuffer) */
	/**用于组合数据包长度的缓冲区（以便每个cPacketizer实例不分配新的cPacketBuffer）*/
	cByteBuffer m_OutPacketLenBuffer;

	/** Returns the protocol-specific packet ID given the protocol-agnostic packet enum. */
	/**返回给定协议不可知数据包枚举的协议特定数据包ID*/
	 //UInt32 GetPacketID(ePacketType a_Packet);

	/** Returns the current protocol's version, for handling status requests. */
	/**返回当前协议的版本，用于处理状态请求*/
	Version GetProtocolVersion();

   /** A generic data-sending routine, all outgoing packet data needs to be routed through this so that descendants may override it. */
   /**作为一个通用的数据发送例程，所有传出的包数据都需要通过该例程进行路由，以便后代可以覆盖它*/
	//void SendData(const char * a_Data, size_t a_Size);

   /** Sends a single packet contained within the cPacketizer class.
   The cPacketizer's destructor calls this to send the contained packet; protocol may transform the data (compression in 1.8 etc). */
   /**发送cPacketizer类中包含的单个数据包。

   cPacketizer的析构函数调用它来发送包含的数据包；协议可以转换数据（1.8等中的压缩）*/
	//void SendPacket(cPacketizer & a_Packet);

	virtual void SendPlayerAbilities (void);
};
