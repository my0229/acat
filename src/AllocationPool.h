
#pragma once





template <class T>
class cAllocationPool 
{
public:
	class cStarvationCallbacks
	{
	public:
		virtual ~cStarvationCallbacks() {}

		/** Is called when the reserve buffer starts to be used */
		/**在开始使用保留缓冲区时调用*/
		virtual void OnStartUsingReserve() = 0;

		/** Is called once the reserve buffer has returned to normal size */
		/**在保留缓冲区恢复到正常大小后调用*/
		virtual void OnEndUsingReserve() = 0;

		/** Is called when the allocation pool is unable to allocate memory. Will be repeatedly
			called if it does not free sufficient memory */
		/**当分配池无法分配内存时调用。会反复
		如果没有释放足够的内存，则调用*/
		virtual void OnOutOfReserve() = 0;
	};

	virtual ~cAllocationPool() {}

	/** Allocates a pointer to T */
	/**将指针分配给T*/
	virtual T * Allocate() = 0;

	/** Frees the pointer passed in a_ptr, invalidating it */
	/**释放在\u ptr中传递的指针，使其无效*/
	virtual void Free(T * a_ptr) = 0;

	/** Two pools compare equal if memory allocated by one can be freed by the other */
	/**如果一个池分配的内存可以由另一个池释放，则两个池比较相等*/
	bool IsEqual(const cAllocationPool & a_Other) const noexcept
	{
		return ((this == &a_Other) || DoIsEqual(a_Other) || a_Other.DoIsEqual(*this));
	}

	friend bool operator == (const cAllocationPool & a_Lhs, const cAllocationPool & a_Rhs)
	{
		return a_Lhs.IsEqual(a_Rhs);
	}

	friend bool operator != (const cAllocationPool & a_Lhs, const cAllocationPool & a_Rhs)
	{
		return !a_Lhs.IsEqual(a_Rhs);
	}

private:
	virtual bool DoIsEqual(const cAllocationPool & a_Other) const noexcept = 0;
};


//加载世界时调用


/** Allocates memory storing unused elements in a linked list. Keeps at least NumElementsInReserve
elements in the list unless malloc fails so that the program has a reserve to handle OOM. */
/**分配存储链接列表中未使用元素的内存。至少保留Numelements储备
元素，除非malloc失败，这样程序就有了处理OOM的保留*/
template <class T>
class cListAllocationPool:
	public cAllocationPool<T>
{
public:

	cListAllocationPool(std::unique_ptr<typename cAllocationPool<T>::cStarvationCallbacks> a_Callbacks, size_t a_MinElementsInReserve, size_t a_MaxElementsInReserve) :
		m_MinElementsInReserve(a_MinElementsInReserve),
		m_MaxElementsInReserve(a_MaxElementsInReserve),
		m_Callbacks(std::move(a_Callbacks))
	{
		for (size_t i = 0; i < m_MinElementsInReserve; i++)
		{
			void * space = malloc(sizeof(T));
			if (space == nullptr)
			{
				m_Callbacks->OnStartUsingReserve();
				break;
			}
			m_FreeList.push_front(space);
		}
	}


	virtual ~cListAllocationPool() override
	{
		while (!m_FreeList.empty())
		{
			free(m_FreeList.front());
			m_FreeList.pop_front();
		}
	}


	virtual T * Allocate() override
	{
		if (m_FreeList.size() <= m_MinElementsInReserve)
		{
			void * space = malloc(sizeof(T));
			if (space != nullptr)
			{
				#if defined(_MSC_VER) && defined(_DEBUG)
					// The debugging-new that is set up using macros in Globals.h doesn't support the placement-new syntax used here.
					// Temporarily disable the macro
					//在Globals.h中使用宏设置的新调试不支持此处使用的placement new语法。
					//暂时禁用宏
					#pragma push_macro("new")
					#undef new
				#endif

				return new(space) T;

				#if defined(_MSC_VER) && defined(_DEBUG)
					// Re-enable the debugging-new macro
					//重新启用调试新宏
					#pragma pop_macro("new")
				#endif
			}
			else if (m_FreeList.size() == m_MinElementsInReserve)
			{
				m_Callbacks->OnStartUsingReserve();
			}
			else if (m_FreeList.empty())
			{
				m_Callbacks->OnOutOfReserve();
				// Try again until the memory is avalable
				//请重试，直到内存可用
				return Allocate();
			}
		}
		// placement new, used to initalize the object
		//放置新建，用于初始化对象

		#if defined(_MSC_VER) && defined(_DEBUG)
			// The debugging-new that is set up using macros in Globals.h doesn't support the placement-new syntax used here.
			// Temporarily disable the macro
			//??Globals.h????��???????��?????????????placement new????

			//??????��?
			#pragma push_macro("new")
			#undef new
		#endif

		T * ret = new (m_FreeList.front()) T;

		#if defined(_MSC_VER) && defined(_DEBUG)
			// Re-enable the debugging-new macro
			//????????????o?
			#pragma pop_macro("new")
		#endif

		m_FreeList.pop_front();
		return ret;
	}


	virtual void Free(T * a_ptr) override
	{
		if (a_ptr == nullptr)
		{
			return;
		}

		a_ptr->~T();  // placement destruct.	//位置自毁。

		if (m_FreeList.size() >= m_MaxElementsInReserve)
		{
			free(a_ptr);
			return;
		}

		m_FreeList.push_front(a_ptr);
		if (m_FreeList.size() == m_MinElementsInReserve)
		{
			m_Callbacks->OnEndUsingReserve();
		}
	}

private:
	/** The minimum number of elements to keep in the free list before malloc fails */
	/**在malloc失败之前保留在空闲列表中的最小元素数*/
	size_t m_MinElementsInReserve;
	/** Maximum free list size before returning memory to the OS */
	/**将内存返回操作系统之前的最大可用列表大小*/
	size_t m_MaxElementsInReserve;
	std::list<void *> m_FreeList;
	std::unique_ptr<typename cAllocationPool<T>::cStarvationCallbacks> m_Callbacks;

	virtual bool DoIsEqual(const cAllocationPool<T> & a_Other) const noexcept override
	{
		return (dynamic_cast<const cListAllocationPool<T>*>(&a_Other) != nullptr);
	}
};
