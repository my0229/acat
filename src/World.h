#pragma once
#include "Globals.h"
#include "ChunkSender.h"
#include "Defines.h"
class cWorld
{
public:
	cWorld();
	/** The dimension of the world, used by the client to provide correct
	 * lighting scheme */
	/**世界的维度，由客户用来提供正确的

	*照明方案*/
	eDimension m_Dimension;

	cChunkSender m_ChunkSender;

	virtual eDimension GetDimension(void) const
	{
		return m_Dimension;
	} 
	double m_SpawnX;
	double m_SpawnY;
	double m_SpawnZ;
	double GetSpawnX(void) const { return m_SpawnX; }
	double GetSpawnY(void) const { return m_SpawnY; }
	double GetSpawnZ(void) const { return m_SpawnZ; }

	/** Starts threads that belong to this world. */
	void Start();
};