#include "Globals.h"

#include "ChunkSender.h"
#include "World.h"
#include "Protocol/ChunkDataSerializer.h"

#include "ClientHandle.h"


cChunkSender::cChunkSender(cWorld & a_World) :
	Super("ChunkSender"),
	m_World(a_World)
{

}


void cChunkSender::Execute(void)
{

}

void cChunkSender::SendChunk(int a_ChunkX, int a_ChunkZ, std::unordered_set<cClientHandle*> a_Clients)
{
		cChunkDataSerializer Data(a_ChunkX, a_ChunkZ, m_Data, m_BiomeMap, m_World.GetDimension());
		Data.SendToClients(a_Clients);
}
