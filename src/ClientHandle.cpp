#pragma once
#include "Globals.h"

#include "ClientHandle.h"
#include "JsonUtils.h"
#include "Protocol/Packetizer.h"
#include "Root.h"
#include "Server.h"
#include "ChunkDef.h"
//#include "Entities/Player.h"

int cClientHandle::s_ClientCount = 0;


/*
	m_ForgeHandshake(this),
	m_Player(nullptr),
	m_CachedSentChunk(0, 0),  //缓存发送块
	*/

cClientHandle::cClientHandle(const AString & a_IPString, int a_ViewDistance) :
	m_LastSentDimension(dimNotSet),			  //上次发送维度
			  //伪造握手
	m_CurrentViewDistance(a_ViewDistance),	//当前视距
	m_RequestedViewDistance(a_ViewDistance),  //请求的视距
	m_IPString(a_IPString),					  // IP String

	m_HasSentDC(false),
	m_LastStreamedChunkX(
		0x7fffffff),  // bogus chunk coords to force streaming upon login
					  // //在登录时强制流式传输的伪造区块协调
	m_LastStreamedChunkZ(0x7fffffff),
	m_TicksSinceLastPacket(
		0), /**自收到最后一个网络数据包以来的刻度数（在刻度（）中增加，在OnReceivedData（）中重置）*/
	m_Ping(1000),
	m_PingID(1),
	m_BlockDigAnimStage(-1),  //块挖掘动画阶段
	m_BlockDigAnimSpeed(0),   //块挖掘动画速度
	m_BlockDigAnimX(0),		  //块挖掘动画X
	m_BlockDigAnimY(
		cChunkDef::Height + 1),  // Invalid Y, so that the coords don't get
								 // picked up //无效的Y，因此coords不会被拾取
	m_BlockDigAnimZ(0),
	m_HasStartedDigging(false),  //已经开始挖了
	m_LastDigBlockX(0),
	m_LastDigBlockY(
		cChunkDef::Height + 1),  // Invalid Y, so that the coords don't get
								 // picked up //无效的Y，因此coords不会被拾取
	m_LastDigBlockZ(0),
	m_State(csConnected),					  //状态
	m_NumExplosionsThisTick(0),				  //这个滴答声是多少
	m_NumBlockChangeInteractionsThisTick(0),  //此勾号的Num块更改交互
	m_UniqueID(0),							  //唯一ID
	m_HasSentPlayerChunk(false),			  //已发送玩家区块
	m_Locale("en_GB"),
	m_LastPlacedSign(0, -1, 0),  //末位标志
	m_ProtocolVersion(0)		 //协议版本
{
	s_ClientCount++;  // Not protected by CS because clients are always
					  // constructed from the same thread
					  // //不受CS保护，因为客户端总是从同一线程构造
	m_UniqueID = s_ClientCount;
	m_PingStartTime = std::chrono::steady_clock::now();

	LOGD("New ClientHandle created at %p", static_cast<void *>(this));
}

cClientHandle::~cClientHandle()
{
	ASSERT(
		m_State ==
		csDestroyed);  // Has Destroy() been called? //调用Destroy（）了吗？

	LOGD("Deleting client \"%s\" at %p", GetUsername().c_str(), static_cast<void *>(this));

	LOGD("ClientHandle at %p deleted", static_cast<void *>(this));
	LOGD("客户析构函数-------------------");
}



void cClientHandle::ServerTick(float a_Dt)
{

	std::cout << "tick " << std::endl;


	ProcessProtocolInOut();  //进程协议输入输出

	// If destruction is queued, destroy now:
	//如果销毁已排队，请立即销毁：
	if (m_State == csQueuedForDestruction)
	{
		LOGD(
			"Client %s @ %s (%p) has been queued for destruction, destroying ""now.", m_Username.c_str(), m_IPString.c_str(), static_cast<void *>(this));
		Destroy();
		return;
	}

	{
		/*
		cCSLock lock(m_CSState);
		if (m_State == csAuthenticated)
		{
			StreamNextChunk();

			// Remove the client handle from the server, it will be ticked from
			// its cPlayer object from now on
			//从服务器上删除客户端句柄，从现在起，它将从其cPlayer对象中勾选
			cRoot::Get()->GetServer()->ClientMovedToWorld(this);

			// Add the player to the world (start ticking from there):
			//将玩家添加到世界（从那里开始滴答）：
			m_State = csDownloadingWorld;
			m_Player->Initialize(
				std::move(m_PlayerPtr), *(m_Player->GetWorld()));
			return;
		}
		*/
	}  // lock(m_CSState)

	m_TicksSinceLastPacket += 1;
	if (m_TicksSinceLastPacket > 600)  // 30 seconds
	{
		//SendDisconnect("Nooooo!! You timed out! D: Come back!");
	}
}

void cClientHandle::InvalidateCachedSentChunk()
{
	//ASSERT(m_Player != nullptr);
	// Sets this to a junk value different from the player's current chunk,
	// which invalidates it and ensures its value will not be used.
	//将其设置为与玩家当前区块不同的垃圾值，

	//这将使其无效，并确保其值不会被使用。
	//m_CachedSentChunk = cChunkCoords(m_Player->GetChunkX() + 500, m_Player->GetChunkZ());
}

void cClientHandle::SendChunkData(int a_ChunkX, int a_ChunkZ, const std::string_view a_ChunkData)
{
	m_Protocol->SendChunkData(a_ChunkData);
}

void cClientHandle::FinishAuthenticate(const AString & a_Name, const cUUID & a_UUID, const Json::Value & a_Properties)
{
	cWorld * World;
	//{
		// Spawn player (only serversided, so data is loaded)
		//繁殖玩家（仅服务器端，因此加载数据）
		//m_PlayerPtr = cpp14::make_unique<cPlayer>(m_Self, GetUsername());
	m_Player = m_PlayerPtr.get();
	InvalidateCachedSentChunk();
	m_Self.reset();


	// New player use default world
	// Player who can load from disk, use loaded world
	//新玩家使用默认世界

	//玩家谁可以从磁盘加载，使用加载的世界
	//if (m_Player->GetWorld() == nullptr)
	{
		//World = cRoot::Get()->GetWorld(m_Player->GetLoadedWorldName());
		//if (World == nullptr)
		{
			//World = cRoot::Get()->GetDefaultWorld();
			//m_Player->SetPosition(
				//World->GetSpawnX(), World->GetSpawnY(), World->GetSpawnZ());
		}
		//m_Player->SetWorld(World);
	}
	//else
	{
		//World = m_Player->GetWorld();
	}

	//m_Player->SetIP(m_IPString);

	//if (!cRoot::Get()->GetPluginManager()->CallHookPlayerJoined(*m_Player))
	//{
	//	cRoot::Get()->BroadcastChatJoin(
	//		Printf("%s has joined the game", GetUsername().c_str()));
	//	LOGINFO("Player %s has joined the game", m_Username.c_str());
	//}

	//m_ConfirmPosition = m_Player->GetPosition();

	// Return a server login packet
	//返回服务器登录数据包
	World = new cWorld();
	m_Protocol->SendLogin(*m_Player, *World);
	//m_LastSentDimension = World->GetDimension();

//	// Send Weather if raining:
//	//如果下雨，发送天气信息：
//	//if ((World->GetWeather() == 1) || (World->GetWeather() == 2))
//	{
//		//m_Protocol->SendWeather(World->GetWeather());
//	}

//	// Send time:
//	//发送时间：
//	m_Protocol->SendTimeUpdate(
//		World->GetWorldAge(), World->GetTimeOfDay(),
//		World->IsDaylightCycleEnabled());

//	// Send contents of the inventory window
//	//发送库存窗口的内容
//	m_Protocol->SendWholeInventory(*m_Player->GetWindow());

//	// Send health
//	//发送健康信息
//	m_Player->SendHealth();

//	// Send experience
//	//发送经验
//	m_Player->SendExperience();

//	// Send hotbar active slot
//	//发送热条活动插槽
//	m_Player->SendHotbarActiveSlot();

//	// Send player list items
//	//发送玩家列表项目
//	SendPlayerListAddPlayer(*m_Player);
//	cRoot::Get()->BroadcastPlayerListsAddPlayer(*m_Player);
//	cRoot::Get()->SendPlayerLists(m_Player);

//	SetState(csAuthenticated);
//}

//// Query player team
////询问球员团队
//m_Player->UpdateTeam();

//// Send scoreboard data
////发送记分板数据
//World->GetScoreBoard().SendTo(*this);

//// Send statistics
////发送统计数据
//SendStatistics(m_Player->GetStatManager());

//// Delay the first ping until the client "settles down"
//// This should fix #889, "BadCast exception, cannot convert bit to fm" error
//// in client
////延迟第一次ping，直到客户端“稳定下来”

////这将修复#889“BadCast异常，无法将位转换为fm”错误

////客户
//m_PingStartTime = std::chrono::steady_clock::now() +std::chrono::seconds(3);  // Send the first KeepAlive packet in 3 seconds //在3秒内发送第一个KeepAlive数据包

//cRoot::Get()->GetPluginManager()->CallHookPlayerSpawned(*m_Player);
	SetState(csAuthenticated);
}

cUUID cClientHandle::GenerateOfflineUUID(const AString & a_Username)
{
	// Online UUIDs are always version 4 (random)
	// We use Version 3 (MD5 hash) UUIDs for the offline UUIDs
	// This guarantees that they will never collide with an online UUID and can
	// be distinguished. This is also consistent with the vanilla offline UUID
	// scheme.

	return cUUID::GenerateVersion3("OfflinePlayer:" + a_Username);
}

void cClientHandle::Authenticate(const AString & a_Name, const cUUID & a_UUID, const Json::Value & a_Properties)
{
	// Atomically increment player count (in server thread)
	cRoot::Get()->GetServer()->PlayerCreated();

	{
		cCSLock lock(m_CSState);
		/*
		LOGD("Processing authentication for client %s @ %s (%p), state = %d",
			m_Username.c_str(), m_IPString.c_str(), static_cast<void *>(this),
		m_State.load()
		);
		//*/

		if (m_State != csAuthenticating)
		{
			return;
		}

		//ASSERT(m_Player == nullptr);

		m_Username = a_Name;

		// Only assign UUID and properties if not already pre-assigned
		// (BungeeCord sends those in the Handshake packet):
		if (m_UUID.IsNil())
		{
			m_UUID = a_UUID;
		}
		if (m_Properties.empty())
		{
			m_Properties = a_Properties;
		}

		// Send login success (if the protocol supports it):
		//发送登录成功（如果协议支持）：
		m_Protocol->SendLoginSuccess();

		//if (m_ForgeHandshake.m_IsForgeClient)
		//{
		//	m_ForgeHandshake.BeginForgeHandshake(a_Name, a_UUID, a_Properties);
		//}
		//else
		//{
		FinishAuthenticate(a_Name, a_UUID, a_Properties);
	//}
	}
}

void cClientHandle::SetSelf(cClientHandlePtr a_Self)
{
	ASSERT(m_Self == nullptr);
	m_Self = std::move(a_Self);
}

void cClientHandle::Kick(const AString & a_Reason)
{
	if (m_State >= csAuthenticating)  // Don't log pings
	{
		LOGINFO(
			"Kicking player %s for \"%s\"", m_Username.c_str(),
			StripColorCodes(a_Reason).c_str());
	}
	SendDisconnect(a_Reason);
}

void cClientHandle::SendDisconnect(const AString & a_Reason)
{
		// Destruction (Destroy()) is called when the client disconnects, not
		// when a disconnect packet (or anything else) is sent Otherwise, the
		// cClientHandle instance is can be unexpectedly removed from the
		// associated player - Core/#142
	if (!m_HasSentDC)
	{
		LOGD("Sending a DC: \"%s\"", StripColorCodes(a_Reason).c_str());
		m_Protocol.SendDisconnect(*this, a_Reason);
		m_HasSentDC = true;
		// csKicked means m_Link will be shut down on the next tick. The
		// disconnect packet data is sent in the tick thread so the
		// connection is closed there after the data is sent.
		SetState(csKicked);
	}
}

//发送缓冲区的数据，接收缓冲区的数据
#include <iostream>
void cClientHandle::ProcessProtocolInOut(void)
{
	// Process received network data:
	//处理接收到的网络数据：
	AString IncomingData;
	{
		cCSLock Lock(m_CSIncomingData);
		std::swap(IncomingData, m_IncomingData);
	}

	if (!IncomingData.empty())
	{
		m_Protocol.HandleIncomingData(*this, IncomingData);  //处理传入数据
	}

	// Send any queued outgoing data:
	//发送任何排队的传出数据：
	AString OutgoingData;
	{
		cCSLock Lock(m_CSOutgoingData);
		std::swap(OutgoingData, m_OutgoingData);
	}

	// Capture the link to prevent it being reset between the null check and the
	// Send:
	//捕获链接以防止其在空检查和发送之间重置：
	auto Link = m_Link;

	if ((Link != nullptr) && !OutgoingData.empty())
	{
		std::cout << Link->Send(OutgoingData.data(), OutgoingData.size()) << std::endl;
	}


}

const AString & cClientHandle::GetUsername(void) const { return m_Username; }

void cClientHandle::SocketClosed(void)
{
	// The socket has been closed for any reason
	/*
	LOGD("SocketClosed for client %s @ %s (%p), state = %d, m_Player = %p",
		m_Username.c_str(), m_IPString.c_str(), static_cast<void *>(this),
	m_State.load(), static_cast<void *>(m_Player)
	);
	//*/

	// Log into console, unless it's a client ping:
	if (!m_Username.empty())
	{
		LOGD(
			"Client %s @ %s disconnected", m_Username.c_str(),
			m_IPString.c_str());
		//cRoot::Get()->GetPluginManager()->CallHookDisconnect(*this, "Player disconnected");
	}

	// Queue self for destruction:
	SetState(csQueuedForDestruction);
}

bool cClientHandle::SetState(eState a_NewState)
{
	cCSLock Lock(m_CSState);
	if (a_NewState <= m_State)
	{
		return false;  // Can only advance the state machine
	}
	m_State = a_NewState;
	return true;
}

void cClientHandle::SendData(const char * a_Data, size_t a_Size)
{

	if (m_HasSentDC)
	{
		// This could crash the client, because they've already unloaded the
		// world etc., and suddenly a wild packet appears (#31)
		//这可能会使客户端崩溃，因为他们已经卸载了world等等，然后突然出现一个野生数据包（#31）
		return;
	}

	cCSLock Lock(m_CSOutgoingData);
	m_OutgoingData.append(a_Data, a_Size);

}

bool cClientHandle::CheckMultiLogin(const AString & a_Username)
{
	// If the multilogin is allowed, skip this check entirely:
	//如果允许多重登录，则完全跳过此检查：
	//if ((cRoot::Get()->GetServer()->DoesAllowMultiLogin()))
	{
		//return true;
	}

	// Check if the player is waiting to be transferred to the World.
	//检查玩家是否正在等待被转移到世界。
	//if (cRoot::Get()->GetServer()->IsPlayerInQueue(a_Username))
	{
		//Kick("A player of the username is already logged in");
		//return false;
	}

	// Check if the player is in any World.
	//检查玩家是否在任何世界。
	//if (cRoot::Get()->DoWithPlayer(a_Username, [](cPlayer &) { return true; }))
	{
		//Kick("A player of the username is already logged in");
		//return false;
	}
	return true;
}

bool cClientHandle::HandleLogin(const AString & a_Username)
{
	{
		cCSLock lock(m_CSState);
		if (m_State != csConnected)
		{
			/*
			LOGD("Client %s @ %s (%p, state %d) has disconnected before logging
			in, bailing out of login", a_Username.c_str(), m_IPString.c_str(),
			static_cast<void *>(this), m_State.load()
			);
			//*/
			return false;
		}

		// LOGD("Handling login for client %s @ %s (%p), state = %d",
		// a_Username.c_str(), m_IPString.c_str(), static_cast<void *>(this),
		// m_State.load());

		m_Username = a_Username;

		// Let the plugins know about this event, they may refuse the player:
		//让插件知道此事件，他们可能会拒绝玩家：
		//if (cRoot::Get()->GetPluginManager()->CallHookLogin(
		//		*this, m_ProtocolVersion, a_Username))
		//{
		//	SendDisconnect("Login Rejected!");
		//	return false; 
		//}

		m_State = csAuthenticating;
	}  // lock(m_CSState)

	// Schedule for authentication; until then, let the player wait (but do not
	// block)
	// 认证时间表；在此之前，让玩家等待（但不要等待）

		// 块）
	cRoot::Get()->GetAuthenticator().Authenticate(GetUniqueID(), GetUsername(), m_Protocol->GetAuthServerID());
	return true;
}

bool cClientHandle::HandleHandshake(const AString & a_Username)
{
	if (a_Username.length() > 16)
	{
		//Kick("Your username is too long (>16 characters)");
		return false;
	}

	//if (cRoot::Get()->GetPluginManager()->CallHookHandshake(*this, a_Username))
	{
		//Kick("Entry denied by plugin");
		//return false;
	}

	return CheckMultiLogin(a_Username);
}

//销毁
void cClientHandle::Destroy(void)
{
	{
		cCSLock Lock(m_CSOutgoingData);
		m_Link.reset();
	}

	if (!SetState(csDestroying))
	{
		// Already called
		LOGD("%s: client %p, \"%s\" already destroyed, bailing out", __FUNCTION__, static_cast<void *>(this), m_Username.c_str());
		return;
	}

	LOGD("%s: destroying client %p, \"%s\" @ %s", __FUNCTION__, static_cast<void *>(this), m_Username.c_str(), m_IPString.c_str());
	auto Self = std::move(m_Self);  // Keep ourself alive for at least as long as this functionSetState(csDestroyed);

	SetState(csDestroyed);
}

//连接
void cClientHandle::OnLinkCreated(cTCPLinkPtr a_Link) { m_Link = a_Link; }

//接收数据
void cClientHandle::OnReceivedData(const char * a_Data, size_t a_Length)
{
	// Reset the timeout:
	//重置超时：
	m_TicksSinceLastPacket = 0;

	// Queue the incoming data to be processed in the tick thread:
	//将要在勾选线程中处理的传入数据排队：
	cCSLock Lock(m_CSIncomingData);
	m_IncomingData.append(a_Data, a_Length);
}

//关闭网络连接
void cClientHandle::OnRemoteClosed(void)
{
	/*
	LOGD("Client socket for %s @ %s has been closed.",
		m_Username.c_str(), m_IPString.c_str()
	);
	//*/
	{
		cCSLock Lock(m_CSOutgoingData);
		m_Link.reset();
	}
	SocketClosed();

}

void cClientHandle::OnError(int a_ErrorCode, const AString & a_ErrorMsg)
{
}
