
// NetworkSingleton.h

// Declares the cNetworkSingleton class representing the storage for global data pertaining to network API
// such as a list of all connections, all listening sockets and the LibEvent dispatch thread.
//声明cNetworkSingleton类，该类表示与网络API相关的全局数据的存储

//例如，所有连接、所有侦听套接字和LibEvent调度线程的列表。

// This is an internal header, no-one outside OSSupport should need to include it; use Network.h instead;
// the only exception being the main app entrypoint that needs to call Terminate before quitting.
//这是一个内部标题，OSSupport之外的任何人都不需要包含它；改用Network.h；

//唯一的例外是主应用程序入口点需要在退出前调用Terminate。




#pragma once

#include <event2/event.h>
#include "NetworkLookup.h"		
#include "CriticalSection.h"
#include "Event.h"





// fwd:
struct event_base;
class cTCPLink;
typedef std::shared_ptr<cTCPLink> cTCPLinkPtr;
typedef std::vector<cTCPLinkPtr> cTCPLinkPtrs;
class cServerHandle;
typedef std::shared_ptr<cServerHandle> cServerHandlePtr;
typedef std::vector<cServerHandlePtr> cServerHandlePtrs;





class cNetworkSingleton
{
public:
	cNetworkSingleton();
	~cNetworkSingleton() noexcept(false);

	/** Returns the singleton instance of this class */
	/**返回该类的单例实例*/
	static cNetworkSingleton & Get(void);

	/** Initialises all network-related threads.
	To be called on first run or after app restart. */
	/**初始化所有与网络相关的线程。

	在第一次运行或应用程序重新启动后调用*/
	void Initialise(void);

	/** Terminates all network-related threads.
	To be used only on app shutdown or restart.
	MSVC runtime requires that the LibEvent networking be shut down before the main() function is exitted; this is the way to do it. */
	/**终止所有与网络相关的线程。

	仅在应用程序关闭或重新启动时使用。

	MSVC运行时要求在退出main（）函数之前关闭LibEvent网络；这是做这件事的方法*/
	void Terminate(void);

	/** Returns the main LibEvent handle for event registering. */
	/**返回用于事件注册的主LibEvent句柄*/
	event_base * GetEventBase(void) { return m_EventBase; }

	/** Returns the thread used to perform hostname and IP lookups */
	/**返回用于执行主机名和IP查找的线程*/
	cNetworkLookup & GetLookupThread() { return m_LookupThread; }

	/** Adds the specified link to m_Connections.
	Used by the underlying link implementation when a new link is created. */

	/**将指定的链接添加到m_连接。

	创建新链接时由基础链接实现使用*/
	void AddLink(const cTCPLinkPtr & a_Link);

	/** Removes the specified link from m_Connections.
	Used by the underlying link implementation when the link is closed / errored. */
	/**从m_连接中删除指定的链接。

	当链接关闭/出错时，由基础链接实现使用*/
	void RemoveLink(const cTCPLink * a_Link);

	/** Adds the specified link to m_Servers.
	Used by the underlying server handle implementation when a new listening server is created.
	Only servers that succeed in listening are added. */
	/**将指定的链接添加到m_服务器。

	在创建新的侦听服务器时由基础服务器句柄实现使用。

	仅添加成功侦听的服务器*/
	void AddServer(const cServerHandlePtr & a_Server);

	/** Removes the specified server from m_Servers.
	Used by the underlying server handle implementation when the server is closed. */
	/**从m_服务器中删除指定的服务器。

	在服务器关闭时由基础服务器句柄实现使用*/
	void RemoveServer(const cServerHandle * a_Server);

protected:

	/** The main LibEvent container for driving the event loop. */
	/**用于驱动事件循环的主LibEvent容器*/
	event_base * m_EventBase;

	/** Container for all client connections, including ones with pending-connect. */
	/**用于所有客户端连接的容器，包括具有挂起连接的客户端连接*/
	cTCPLinkPtrs m_Connections;

	/** Container for all servers that are currently active. */
	/**当前处于活动状态的所有服务器的容器*/
	cServerHandlePtrs m_Servers;

	/** Mutex protecting all containers against multithreaded access. */
	/**互斥锁保护所有容器不受多线程访问*/
	cCriticalSection m_CS;

	/** Set to true if Terminate has been called. */
	/**如果已调用Terminate，则设置为true*/
	std::atomic<bool> m_HasTerminated;

	/** The thread in which the main LibEvent loop runs. */
	/**运行主LibEvent循环的线程*/
	std::thread m_EventLoopThread;

	/** Event that is signalled once the startup is finished and the LibEvent loop is running. */
	/**事件，该事件在启动完成且LibEvent循环正在运行时发出信号*/
	cEvent m_StartupEvent;

	/** The thread on which hostname and ip address lookup is performed. */
	/**在其上执行主机名和ip地址查找的线程*/
	cNetworkLookup m_LookupThread;


	/** Converts LibEvent-generated log events into log messages in MCS log. */
	/**将LibEvent生成的日志事件转换为MCS日志中的日志消息*/
	static void LogCallback(int a_Severity, const char * a_Msg);

	/** Implements the thread that runs LibEvent's event dispatcher loop. */
	/**实现运行LibEvent的事件调度程序循环的线程*/
	static void RunEventLoop(cNetworkSingleton * a_Self);

	/** Callback called by LibEvent when the event loop is started. */
	/**启动事件循环时，LibEvent调用回调*/
	static void SignalizeStartup(evutil_socket_t a_Socket, short a_Events, void * a_Self);
};
