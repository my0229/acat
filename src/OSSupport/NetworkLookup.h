
// NetworkLookup.h

// Declares the cNetworkLookup class representing an executor for asynchronous lookup tasks
//声明表示异步查找任务执行器的cNetworkLookup类

#pragma once

#include <functional>

#include "IsThread.h"
#include "Queue.h"





class cNetworkLookup :
	public cIsThread
{
public:

	cNetworkLookup();
	virtual ~cNetworkLookup() override;

	/** Schedule a lookup task for execution. */
	/**安排要执行的查找任务*/
	void ScheduleLookup(std::function<void()> a_Lookup);

	/** Cancels any scheduled lookups and joins the lookup thread. */
	/**取消所有计划的查找并加入查找线程*/
	void Stop();

protected:

	/** Process the queue until the thread is stopped. */
	/**处理队列，直到线程停止*/
	virtual void Execute() override final;

private:

	/** The queue of lookup tasks waiting to be executed. */
	/**等待执行的查找任务队列*/
	cQueue<std::function<void()>> m_WorkQueue;
};


