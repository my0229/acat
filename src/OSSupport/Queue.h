
// Queue.h

// Implements the cQueue class representing a thread safe queue
//实现表示线程安全队列的cQueue类

#pragma once

/*
Items can be added multiple times to a queue, there are two functions for
adding, EnqueueItem() and EnqueueItemIfNotPresent(). The first one always
enqueues the specified item, the second one checks if the item is already
present and only queues it if it isn't.

Usage:
To create a queue of type T, instantiate a cQueue<T> object. You can also
modify the behavior of the queue when deleting items and when adding items
that are already in the queue by providing a second parameter, a class that
implements the functions Delete() and Combine(). An example is given in
cQueueFuncs and is used as the default behavior. */

/** This empty struct allows for the callback functions to be inlined */
/*

项目可以多次添加到队列中，有两个函数用于

正在添加、EnqueueItem（）和EnqueueItemIfNotPresent（）。第一个总是

将指定的项目排队，第二个项目检查该项目是否已经存在

显示并仅在未显示时将其排队。

用法：

要创建类型为T的队列，请实例化cQueue<T>对象。你也可以

在删除项目和添加项目时修改队列的行为

通过提供第二个参数（一个

实现函数Delete（）和Combine（）。文中给出了一个例子

CqueFuncs和用作默认行为*/

/**此空结构允许内联回调函数*/
template <class T>
struct cQueueFuncs
{
public:

	/** Called when an Item is deleted from the queue without being returned */
	/** Called when an Item is deleted from the queue without being returned */
	static void Delete(T) {}

	/** Called when an Item is inserted with EnqueueItemIfNotPresent and there is another equal value already inserted */
	/**当使用EnqueueItemIfNotPresent插入项且已插入另一个相等值时调用*/
	static void Combine(T & a_existing, const T & a_new)
	{
		UNUSED(a_existing);
		UNUSED(a_new);
	}
};





template <class ItemType, class Funcs = cQueueFuncs<ItemType> >
class cQueue
{
	// The actual storage type for the queue
	//队列的实际存储类型
	typedef typename std::list<ItemType> QueueType;

	// Make iterator an alias for the QueueType's iterator
	//使迭代器成为队列类型迭代器的别名
	typedef typename QueueType::iterator iterator;

public:
	cQueue() {}
	~cQueue() {}


	/** Enqueues an item to the queue, may block if other threads are accessing the queue. */
	/**将项目排入队列，如果其他线程正在访问队列，则可能会阻塞*/
	void EnqueueItem(ItemType a_Item)
	{
		cCSLock Lock(m_CS);
		m_Contents.push_back(a_Item);
		m_evtAdded.Set();
	}


	/** Enqueues an item in the queue if not already present (as determined by operator ==). Blocks other threads from accessing the queue. */
	/**将队列中尚未存在的项目排队（由运算符==确定）。阻止其他线程访问队列*/
	void EnqueueItemIfNotPresent(ItemType a_Item)
	{
		cCSLock Lock(m_CS);

		for (iterator itr = m_Contents.begin(); itr != m_Contents.end(); ++itr)
		{
			if ((*itr) == a_Item)
			{
				Funcs::Combine(*itr, a_Item);
				return;
			}
		}
		m_Contents.push_back(a_Item);
		m_evtAdded.Set();
	}


	/** Dequeues an item from the queue if any are present.
	Returns true if successful. Value of item is undefined if dequeuing was unsuccessful. */
	/**从队列中退出项目（如果存在）。

	如果成功，则返回true。如果退出队列失败，则未定义项的值*/
		
	bool TryDequeueItem(ItemType & item)
	{
		cCSLock Lock(m_CS);
		if (m_Contents.empty())
		{
			return false;
		}
		item = m_Contents.front();
		m_Contents.pop_front();
		m_evtRemoved.Set();
		return true;
	}


	/** Dequeues an item from the queue, blocking until an item is available. */
	/**将项目从队列中移出队列，直到项目可用为止*/

	ItemType DequeueItem(void)
	{
		cCSLock Lock(m_CS);
		while (m_Contents.empty())
		{
			cCSUnlock Unlock(Lock);
			m_evtAdded.Wait();
		}
		ItemType item = m_Contents.front();
		m_Contents.pop_front();
		m_evtRemoved.Set();
		return item;
	}


	/** Blocks until the queue is empty. */
	/**阻塞，直到队列为空*/
	void BlockTillEmpty(void)
	{
		cCSLock Lock(m_CS);
		while (!m_Contents.empty())
		{
			cCSUnlock Unlock(Lock);
			m_evtRemoved.Wait();
		}
	}


	/** Removes all Items from the Queue, calling Delete on each of them. */
	/**从队列中删除所有项目，并对每个项目调用Delete*/
	void Clear(void)
	{
		cCSLock Lock(m_CS);
		while (!m_Contents.empty())
		{
			Funcs::Delete(m_Contents.front());
			m_Contents.pop_front();
		}
	}


	/** Returns the size at time of being called.
	Do not use to determine whether to call DequeueItem(), use TryDequeueItem() instead */
	/**返回调用时的大小。

	不要使用来确定是否调用DequeueItem（），请改用TryDequeueItem（）*/
	size_t Size(void)
	{
		cCSLock Lock(m_CS);
		return m_Contents.size();
	}


	/** Removes the item from the queue. If there are multiple such items, only the first one is removed.
	Returns true if the item has been removed, false if no such item found. */
	/**从队列中删除该项。如果有多个这样的项目，则仅删除第一个项目。

	如果项目已删除，则返回true；如果未找到此类项目，则返回false*/
	bool Remove(ItemType a_Item)
	{
		cCSLock Lock(m_CS);
		for (iterator itr = m_Contents.begin(); itr != m_Contents.end(); ++itr)
		{
			if ((*itr) == a_Item)
			{
				m_Contents.erase(itr);
				m_evtRemoved.Set();
				return true;
			}
		}
		return false;
	}


	/** Removes all items for which the predicate returns true. */
	/**删除谓词返回true的所有项*/
	template <class Predicate>
	void RemoveIf(Predicate a_Predicate)
	{
		cCSLock Lock(m_CS);
		for (auto itr = m_Contents.begin(); itr != m_Contents.end();)
		{
			if (a_Predicate(*itr))
			{
				auto itr2 = itr;
				++itr2;
				m_Contents.erase(itr);
				m_evtRemoved.Set();
				itr = itr2;
			}
			else
			{
				++itr;
			}
		}  // for itr - m_Contents[]
	}

private:
	/** The contents of the queue */
	/**队列的内容*/
	QueueType m_Contents;

	/** Mutex that protects access to the queue contents */
	/**保护对队列内容的访问的互斥体*/
	cCriticalSection m_CS;

	/** Event that is signalled when an item is added */
	/**添加项时发出信号的事件*/
	cEvent m_evtAdded;

	/** Event that is signalled when an item is removed (both dequeued or erased) */
	/**移除项时发出信号的事件（退出队列或擦除）*/
	cEvent m_evtRemoved;
};




