
// IsThread.h

// Interfaces to the cIsThread class representing an OS-independent wrapper for a class that implements a thread.
// This class will eventually suupersede the old cThread class
//接口到cIsThread类，该类表示实现线程的类的独立于操作系统的包装器。

//这个类最终将超越旧的cThread类

/*
Usage:
To have a new thread, declare a class descending from cIsClass.
Then override its Execute() method to provide your thread processing.
In the descending class' constructor call the Start() method to start the thread once you're finished with initialization.
*/


/*

用法：

要拥有一个新线程，请声明一个从cIsClass降序而来的类。

然后重写其Execute（）方法以提供线程处理。

在降序类的构造函数中，一旦完成初始化，就调用Start（）方法来启动线程。

*/



#pragma once





class cIsThread
{
protected:
	/** This is the main thread entrypoint.
	This function, overloaded by the descendants, is called in the new thread. */

	/**这是主线程入口点。

	此函数由子体重载，在新线程中调用*/
	virtual void Execute(void) = 0;

	/** The overriden Execute() method should check this value periodically and terminate if this is true. */
	/**重写的Execute（）方法应定期检查此值，如果为真，则终止*/
	std::atomic<bool> m_ShouldTerminate;

public:
	cIsThread(const AString & a_ThreadName);
	virtual ~cIsThread();

	/** Starts the thread; returns without waiting for the actual start. */
	/**启动线程；返回而不等待实际启动*/
	bool Start(void);

	/** Signals the thread to terminate and waits until it's finished. */
	/**向线程发出终止信号并等待它完成*/
	void Stop(void);

	/** Waits for the thread to finish. Doesn't signalize the ShouldTerminate flag. */
	/**等待线程完成。不应该终止该标志*/
	bool Wait(void);

	/** Returns true if the thread calling this function is the thread contained within this object. */
	/**如果调用此函数的线程是此对象中包含的线程，则返回true*/
	bool IsCurrentThread(void) const { return std::this_thread::get_id() == m_Thread.get_id(); }

private:

	/** The name of the thread, used to aid debugging in IDEs which support named threads */
	/**线程的名称，用于在支持命名线程的IDE中帮助调试*/
	AString m_ThreadName;

	/** The thread object which holds the created thread for later manipulation */
	/**保存已创建线程以供以后操作的线程对象*/
	std::thread m_Thread;

	/** The event that is used to wait with the thread's execution until the thread object is fully initialized.
	This prevents the IsCurrentThread() call to fail because of a race-condition where the thread starts before m_Thread has been fully assigned. */
	/**用于等待线程执行直到线程对象完全初始化的事件。

	这可以防止IsCurrentThread（）调用因争用条件而失败，在争用条件下，线程在完全分配m_线程之前启动*/
	cEvent m_evtStart;

	/** Wrapper for Execute() that waits for the initialization event, to prevent race conditions in thread initialization. */
	/**等待初始化事件的Execute（）包装，以防止线程初始化中出现争用条件*/
	void DoExecute(void);
} ;
