
// TCPLinkImpl.h

// Declares the cTCPLinkImpl class implementing the TCP link functionality
//声明实现TCP链路功能的cTCPLinkImpl类
// This is an internal header, no-one outside OSSupport should need to include it; use Network.h instead
//这是一个内部标题，OSSupport之外的任何人都不需要包含它；改用Network.h




#pragma once

#include "Network.h"
#include <event2/event.h>
#include <event2/bufferevent.h>
#include "../mbedTLS++/SslContext.h"





// fwd:
class cServerHandleImpl;
typedef std::shared_ptr<cServerHandleImpl> cServerHandleImplPtr;
class cTCPLinkImpl;
typedef std::shared_ptr<cTCPLinkImpl> cTCPLinkImplPtr;
typedef std::vector<cTCPLinkImplPtr> cTCPLinkImplPtrs;





class cTCPLinkImpl:
	public cTCPLink
{
	using Super = cTCPLink;

public:

	/** Creates a new link based on the given socket.
	Used for connections accepted in a server using cNetwork::Listen().
	a_Address and a_AddrLen describe the remote peer that has connected.
	The link is created disabled, you need to call Enable() to start the regular communication. */
  /**基于给定套接字创建新链接。

	用于使用cNetwork:：Listen（）在服务器中接受的连接。

	地址和地址描述已连接的远程对等方。

	链接已被禁用，您需要调用Enable（）来启动常规通信*/
	cTCPLinkImpl(evutil_socket_t a_Socket, cCallbacksPtr a_LinkCallbacks, cServerHandleImplPtr a_Server, const sockaddr * a_Address, socklen_t a_AddrLen);

	/** Destroys the LibEvent handle representing the link. */
	/**销毁表示链接的LibEvent句柄*/
	virtual ~cTCPLinkImpl() override;

	/** Queues a connection request to the specified host.
	a_ConnectCallbacks must be valid.
	Returns a link that has the connection request queued, or NULL for failure. */
	/**将连接请求排入指定主机的队列。

	a_ConnectCallbacks必须有效。

	返回连接请求排队的链接，如果失败，返回NULL*/
	static cTCPLinkImplPtr Connect(const AString & a_Host, UInt16 a_Port, cTCPLink::cCallbacksPtr a_LinkCallbacks, cNetwork::cConnectCallbacksPtr a_ConnectCallbacks);

	/** Enables communication over the link.
	Links are created with communication disabled, so that creation callbacks can be called first.
	This function then enables the regular communication to be reported.
	The a_Self parameter is used so that the socket can keep itself alive as long as the callbacks are coming. */
	/**启用链路上的通信。

	链接是在禁用通信的情况下创建的，因此可以首先调用创建回调。

	然后，此功能可报告定期通信。

	使用a_Self参数，以便套接字可以在回调到来时保持自身活动*/
	void Enable(cTCPLinkImplPtr a_Self);

	// cTCPLink overrides:
	// cTCPLink覆盖：
	virtual bool Send(const void * a_Data, size_t a_Length) override;
	virtual AString GetLocalIP(void) const override { return m_LocalIP; }
	virtual UInt16 GetLocalPort(void) const override { return m_LocalPort; }
	virtual AString GetRemoteIP(void) const override { return m_RemoteIP; }
	virtual UInt16 GetRemotePort(void) const override { return m_RemotePort; }
	virtual void Shutdown(void) override;
	virtual void Close(void) override;
	virtual AString StartTLSClient(
		cX509CertPtr a_OwnCert,
		cCryptoKeyPtr a_OwnPrivKey
	) override;
	virtual AString StartTLSServer(
		cX509CertPtr a_OwnCert,
		cCryptoKeyPtr a_OwnPrivKey,
		const AString & a_StartTLSData
	) override;

protected:

	// fwd:
	class cLinkTlsContext;
	typedef std::shared_ptr<cLinkTlsContext> cLinkTlsContextPtr;
	typedef std::weak_ptr<cLinkTlsContext> cLinkTlsContextWPtr;

	/** Wrapper around cSslContext that is used when this link is being encrypted by SSL. */
	/**通过SSL加密此链接时使用的围绕cSslContext的包装*/
	class cLinkTlsContext :
		public cSslContext
	{
		cTCPLinkImpl & m_Link;

		/** Buffer for storing the incoming encrypted data until it is requested by the SSL decryptor. */
		/**用于存储传入加密数据的缓冲区，直到SSL解密程序请求它为止*/
		AString m_EncryptedData;

		/** Buffer for storing the outgoing cleartext data until the link has finished handshaking. */
		/**用于存储传出明文数据的缓冲区，直到链接完成握手*/
		AString m_CleartextData;

		/** Shared ownership of self, so that this object can keep itself alive for as long as it needs. */
		/**共享自我的所有权，这样这个对象就可以在需要的时候保持自己的生命*/
		cLinkTlsContextWPtr m_Self;

	public:
		cLinkTlsContext(cTCPLinkImpl & a_Link);

		/** Shares ownership of self, so that this object can keep itself alive for as long as it needs. */
		/**分享自我的所有权，这样这个物体就可以在它需要的时候保持它自己的生命*/
		void SetSelf(cLinkTlsContextWPtr a_Self);

		/** Removes the self ownership so that we can detect the SSL closure. */
		/**删除自所有权，以便我们可以检测SSL关闭*/
		void ResetSelf(void);

		/** Stores the specified block of data into the buffer of the data to be decrypted (incoming from remote).
		Also flushes the SSL buffers by attempting to read any data through the SSL context. */
		/**将指定的数据块存储到要解密的数据的缓冲区中（从远程传入）。

		还通过尝试通过SSL上下文读取任何数据来刷新SSL缓冲区*/
		void StoreReceivedData(const char * a_Data, size_t a_NumBytes);

		/** Tries to read any cleartext data available through the SSL, reports it in the link. */
		/**尝试通过SSL读取任何可用的明文数据，并在链接中报告*/
		void FlushBuffers(void);

		/** Tries to finish handshaking the SSL. */
		/**尝试完成与SSL的握手*/
		void TryFinishHandshaking(void);

		/** Sends the specified cleartext data over the SSL to the remote peer.
		If the handshake hasn't been completed yet, queues the data for sending when it completes. */
		/**通过SSL将指定的明文数据发送到远程对等方。

		如果握手尚未完成，则将数据排入队列，以便在握手完成时发送*/
		void Send(const void * a_Data, size_t a_Length);

		// cSslContext overrides:
		virtual int ReceiveEncrypted(unsigned char * a_Buffer, size_t a_NumBytes) override;
		virtual int SendEncrypted(const unsigned char * a_Buffer, size_t a_NumBytes) override;

		/** Returns true if the context's associated TCP link is the same link as a_Link. */
		/**如果上下文关联的TCP链接与_链接相同，则返回true*/
		bool IsLink(cTCPLinkImpl * a_Link)
		{
			return (a_Link == &m_Link);
		}
	};


	/** Callbacks to call when the connection is established.
	May be NULL if not used. Only used for outgoing connections (cNetwork::Connect()). */
	/**建立连接时调用的回调。

	如果未使用，则可能为空。仅用于传出连接（cNetwork:：Connect（））*/
	cNetwork::cConnectCallbacksPtr m_ConnectCallbacks;

	/** The LibEvent handle representing this connection. */
	/**表示此连接的LibEvent句柄*/
	bufferevent * m_BufferEvent;

	/** The server handle that has created this link.
	Only valid for incoming connections, nullptr for outgoing connections. */
	/**创建此链接的服务器句柄。

	仅对传入连接有效，nullptr对传出连接有效*/
	cServerHandleImplPtr m_Server;

	/** The IP address of the local endpoint. Valid only after the socket has been connected. */
	/**本地终结点的IP地址。仅在连接套接字后有效*/
	AString m_LocalIP;

	/** The port of the local endpoint. Valid only after the socket has been connected. */
	/**本地终结点的端口。仅在连接套接字后有效*/
	UInt16 m_LocalPort;

	/** The IP address of the remote endpoint. Valid only after the socket has been connected. */
	/**远程终结点的IP地址。仅在连接套接字后有效*/
	AString m_RemoteIP;

	/** The port of the remote endpoint. Valid only after the socket has been connected. */
	/**远程终结点的端口。仅在连接套接字后有效*/
	UInt16 m_RemotePort;

	/** SharedPtr to self, used to keep this object alive as long as the callbacks are coming.
	Initialized in Enable(), cleared in Close() and EventCallback(RemoteClosed). */
	/**SharedPtr到self，用于在回调到来时保持此对象的活动状态。

	在Enable（）中初始化，在Close（）和EventCallback（RemoteClose）中清除*/
	cTCPLinkImplPtr m_Self;

	/** If true, Shutdown() has been called and is in queue.
	No more data is allowed to be sent via Send() and after all the currently buffered
	data is sent to the OS TCP stack, the socket gets shut down. */
	/**如果为true，则Shutdown（）已被调用且在队列中。

	不允许通过Send（）发送更多数据，并且在所有当前缓冲的

	数据被发送到操作系统TCP堆栈，套接字被关闭*/
	bool m_ShouldShutdown;

	/** The SSL context used for encryption, if this link uses SSL.
	If valid, the link uses encryption through this context. */
	/**用于加密的SSL上下文（如果此链接使用SSL）。

	如果有效，链接将通过此上下文使用加密*/
	cLinkTlsContextPtr m_TlsContext;


	/** Creates a new link to be queued to connect to a specified host:port.
	Used for outgoing connections created using cNetwork::Connect().
	To be used only by the Connect() factory function.
	The link is created disabled, you need to call Enable() to start the regular communication. */
	/**创建要排队以连接到指定主机的新链接：端口。

	用于使用cNetwork:：Connect（）创建的传出连接。

	仅由Connect（）工厂函数使用。

	链接已被禁用，您需要调用Enable（）来启动常规通信*/
	cTCPLinkImpl(const cCallbacksPtr a_LinkCallbacks);

	/** Callback that LibEvent calls when there's data available from the remote peer. */
	/**当远程对等方有可用数据时，LibEvent调用的回调*/
	static void ReadCallback(bufferevent * a_BufferEvent, void * a_Self);

	/** Callback that LibEvent calls when the remote peer can receive more data. */
	/**当远程对等方可以接收更多数据时，LibEvent调用的回调*/
	static void WriteCallback(bufferevent * a_BufferEvent, void * a_Self);

	/** Callback that LibEvent calls when there's a non-data-related event on the socket. */
	/**当套接字上存在非数据相关事件时，LibEvent调用的回调*/
	static void EventCallback(bufferevent * a_BufferEvent, short a_What, void * a_Self);

	/** Sets a_IP and a_Port to values read from a_Address, based on the correct address family. */
	/**根据正确的地址族，将_IP和_端口设置为从_地址读取的值*/
	static void UpdateAddress(const sockaddr * a_Address, socklen_t a_AddrLen, AString & a_IP, UInt16 & a_Port);

	/** Updates m_LocalIP and m_LocalPort based on the metadata read from the socket. */
	/**根据从套接字读取的元数据更新m_LocalIP和m_LocalPort*/
	void UpdateLocalAddress(void);

	/** Updates m_RemoteIP and m_RemotePort based on the metadata read from the socket. */
	/**根据从套接字读取的元数据更新m_RemoteIP和m_RemotePort*/
	void UpdateRemoteAddress(void);

	/** Calls shutdown on the link and disables LibEvent writing.
	Called after all data from LibEvent buffers is sent to the OS TCP stack and shutdown() has been called before. */
	/**调用链接上的shutdown并禁用LibEvent写入。

	将LibEvent缓冲区中的所有数据发送到OS TCP堆栈并在之前调用shutdown（）后调用*/
	void DoActualShutdown(void);

	/** Sends the data directly to the socket (without the optional TLS). */
	/**将数据直接发送到套接字（不带可选TLS）*/
	bool SendRaw(const void * a_Data, size_t a_Length);

	/** Called by the TLS when it has decoded a piece of incoming cleartext data from the socket. */
	/**TLS在解码来自套接字的一段传入明文数据时调用*/
	void ReceivedCleartextData(const char * a_Data, size_t a_Length);
};




