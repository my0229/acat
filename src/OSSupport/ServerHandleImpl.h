
// ServerHandleImpl.h
//服务启动时会使用到

// Declares the cServerHandleImpl class implementing the TCP server functionality
//声明实现TCP服务器功能的CSServerHandleImpl类
// This is an internal header, no-one outside OSSupport should need to include it; use Network.h instead
//这是一个内部标题，OSSupport之外的任何人都不需要包含它；改用Network.h




#pragma once

#include "Network.h"
#include <event2/listener.h>
#include "CriticalSection.h"





// fwd:
class cTCPLinkImpl;
typedef std::shared_ptr<cTCPLinkImpl> cTCPLinkImplPtr;
typedef std::vector<cTCPLinkImplPtr> cTCPLinkImplPtrs;
class cServerHandleImpl;
typedef std::shared_ptr<cServerHandleImpl> cServerHandleImplPtr;
typedef std::vector<cServerHandleImplPtr> cServerHandleImplPtrs;





class cServerHandleImpl:
	public cServerHandle
{
	using Super = cServerHandle;
	friend class cTCPLinkImpl;

public:

	/** Closes the server, dropping all the connections. */
	/**关闭服务器，删除所有连接*/
	virtual ~cServerHandleImpl() override;

	/** Creates a new server instance listening on the specified port.
	Both IPv4 and IPv6 interfaces are used, if possible.
	Always returns a server instance; in the event of a failure, the instance holds the error details. Use IsListening() to query success. */
	/**创建在指定端口上侦听的新服务器实例。

	如果可能，同时使用IPv4和IPv6接口。

	始终返回一个服务器实例；如果发生故障，实例将保存错误详细信息。使用IsListening（）查询成功*/
	static cServerHandleImplPtr Listen(
		UInt16 a_Port,
		cNetwork::cListenCallbacksPtr a_ListenCallbacks
	);

	// cServerHandle overrides:
	// cServerHandle覆盖：
	virtual void Close(void) override;
	virtual bool IsListening(void) const override { return m_IsListening; }

protected:
	/** The callbacks used to notify about incoming connections. */
	 /**用于通知传入连接的回调*/
	cNetwork::cListenCallbacksPtr m_ListenCallbacks;

	/** The LibEvent handle representing the main listening socket. */
	/**表示主侦听套接字的LibEvent句柄*/
	evconnlistener * m_ConnListener;

	/** The LibEvent handle representing the secondary listening socket (only when side-by-side listening is needed, such as WinXP). */
	/**表示辅助侦听套接字的LibEvent句柄（仅当需要并行侦听时，如WinXP）*/
	evconnlistener * m_SecondaryConnListener;

	/** Set to true when the server is initialized successfully and is listening for incoming connections. */
	/**当服务器成功初始化并且正在侦听传入连接时，设置为true*/
	bool m_IsListening;

	/** Container for all currently active connections on this server. */
	/**此服务器上所有当前活动连接的容器*/
	cTCPLinkImplPtrs m_Connections;

	/** Mutex protecting m_Connections againt multithreaded access. */
	/**互斥保护m_连接不受多线程访问的影响*/
	cCriticalSection m_CS;

	/** Contains the error code for the failure to listen. Only valid for non-listening instances. */
	/**包含侦听失败的错误代码。仅对非侦听实例有效*/
	int m_ErrorCode;

	/** Contains the error message for the failure to listen. Only valid for non-listening instances. */
	/**包含侦听失败的错误消息。仅对非侦听实例有效*/
	AString m_ErrorMsg;

	/** The SharedPtr to self, so that it can be passed to created links. */
	/**将SharedPtr传递给self，以便将其传递给已创建的链接*/
	cServerHandleImplPtr m_SelfPtr;



	/** Creates a new instance with the specified callbacks.
	Initializes the internals, but doesn't start listening yet. */
	/**使用指定的回调创建新实例。

	初始化内部，但尚未开始侦听*/
	cServerHandleImpl(cNetwork::cListenCallbacksPtr a_ListenCallbacks);

	/** Starts listening on the specified port.
	Returns true if successful, false on failure. On failure, sets m_ErrorCode and m_ErrorMsg. */
	/**开始在指定端口上侦听。

	成功时返回true，失败时返回false。失败时，设置m_ErrorCode和m_ErrorMsg*/
	bool Listen(UInt16 a_Port);

	/** The callback called by LibEvent upon incoming connection. */
	/**LibEvent在传入连接时调用的回调*/
	static void Callback(evconnlistener * a_Listener, evutil_socket_t a_Socket, sockaddr * a_Addr, int a_Len, void * a_Self);

	/** Removes the specified link from m_Connections.
	Called by cTCPLinkImpl when the link is terminated. */

	/**从m_连接中删除指定的链接。

	链接终止时由cTCPLinkImpl调用*/
	void RemoveLink(const cTCPLinkImpl * a_Link);
};





