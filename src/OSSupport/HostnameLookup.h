
// HostnameLookup.h

// Declares the cHostnameLookup class representing an in-progress hostname-to-IP lookup

// This is an internal header, no-one outside OSSupport should need to include it; use Network.h instead
//声明cHostnameLookup类，该类表示正在进行的主机名到IP查找

//这是一个内部标题，OSSupport之外的任何人都不需要包含它；改用Network.h




#pragma once

#include "Network.h"





/** Holds information about an in-progress Hostname-to-IP lookup. */
/**保存有关正在进行的主机名到IP查找的信息*/
class cHostnameLookup
{
public:
	/** Creates a lookup object and schedules the lookup. */
	/**创建查找对象并计划查找*/
	static void Lookup(const AString & a_Hostname, cNetwork::cResolveNameCallbacksPtr a_Callbacks);

protected:

	/** Creates the lookup object. Doesn't start the lookup yet. */
	/**创建查找对象。尚未启动查找*/
	cHostnameLookup(const AString & a_Hostname, cNetwork::cResolveNameCallbacksPtr a_Callbacks);

	/** The callbacks to call for resolved names / errors. */
	/**调用已解析名称/错误的回调*/
	cNetwork::cResolveNameCallbacksPtr m_Callbacks;

	/** The hostname that was queried (needed for the callbacks). */
	/**查询的主机名（回调所需）*/
	AString m_Hostname;

	void Callback(int a_ErrCode, struct addrinfo * a_Addr);
};
typedef std::shared_ptr<cHostnameLookup> cHostnameLookupPtr;
typedef std::vector<cHostnameLookupPtr> cHostnameLookupPtrs;





