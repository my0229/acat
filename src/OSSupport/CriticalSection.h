
#pragma once




//临界截面
class cCriticalSection
{
	friend class cDeadlockDetect;  // Allow the DeadlockDetect to read the internals, so that it may output some statistics
	//允许DeadlockDetect读取内部数据，以便它可以输出一些统计信息

public:
	void Lock(void);
	void Unlock(void);

	cCriticalSection(void);

	/** Returns true if the CS is currently locked.
	Note that since it relies on the m_RecursionCount value, it is inherently thread-unsafe, prone to false positives.
	Also, due to multithreading, the state can change between this when function is evaluated and the returned value is used.
	To be used in ASSERT(IsLocked()) only. */
	/**如果CS当前已锁定，则返回true。

	注意，由于它依赖于m_RecursionCount值，因此它本质上是线程不安全的，容易出现误报。

	此外，由于采用多线程，在计算函数和使用返回值之间，状态可能会发生变化。

	仅在ASSERT（IsLocked（））中使用*/
	bool IsLocked(void);

	/** Returns true if the CS is currently locked by the thread calling this function.
	Note that since it relies on the m_RecursionCount value, it is inherently thread-unsafe, prone to false positives.
	Also, due to multithreading, the state can change between this when function is evaluated and the returned value is used.
	To be used in ASSERT(IsLockedByCurrentThread()) only. */
	/**如果CS当前被调用此函数的线程锁定，则返回true。

	注意，由于它依赖于m_RecursionCount值，因此它本质上是线程不安全的，容易出现误报。

	此外，由于采用多线程，在计算函数和使用返回值之间，状态可能会发生变化。

	仅在断言（IsLockedByCurrentThread（））中使用*/
	bool IsLockedByCurrentThread(void);

private:


	/**此CS当前被锁定的次数（递归级别）。如果未锁定，则为零。

	注意，仅当CS被锁定时，该值才应被视为真值；如果没有锁，它甚至无法读取，

	但是将其设置为std：：atomic会带来太多的运行时惩罚。

	在死锁检测中，只有在没有锁的情况下才能读取它，此时服务器仍将终止*/
	int m_RecursionCount;

	/** ID of the thread that is currently holding the CS.
	Note that this value should be considered true only when the CS is locked; without the lock, it is UndefinedBehavior to even read it,
	but making it std::atomic would impose too much of a runtime penalty.
	When unlocked, the value stored here has no meaning, it may be an ID of a previous holder, or it could be any garbage.
	It is only ever read without the lock in the DeadlockDetect, where the server is terminating anyway. */
	/**当前持有CS的线程的ID。

	注意，仅当CS被锁定时，该值才应被视为真值；如果没有锁，它甚至无法读取，

	但是将其设置为std：：atomic会带来太多的运行时惩罚。

	解锁时，此处存储的值没有任何意义，它可能是以前持有者的ID，也可能是任何垃圾。

	在死锁检测中，只有在没有锁的情况下才能读取它，此时服务器仍将终止*/
	std::thread::id m_OwningThreadID;

	std::recursive_mutex m_Mutex;
};




/** RAII for cCriticalSection - locks the CS on creation, unlocks on destruction*/
/**CCCriticalSection的RAII-在创建时锁定CS，在销毁时解锁*/
class cCSLock
{
	cCriticalSection * m_CS;

	// Unlike a cCriticalSection, this object should be used from a single thread, therefore access to m_IsLocked is not threadsafe
	//与cCriticalSection不同，此对象应该从单个线程使用，因此对m_IsLocked的访问不是线程安全的
	// In Windows, it is an error to call cCriticalSection::Unlock() multiple times if the lock is not held,
	//在Windows中，如果未持有锁，多次调用cCriticalSection:：Unlock（）是错误的，
	// therefore we need to check this value whether we are locked or not.
	//因此，我们需要检查该值是否已锁定。
	bool m_IsLocked;

public:
	cCSLock(cCriticalSection * a_CS);
	cCSLock(cCriticalSection & a_CS);
	~cCSLock();

	// Temporarily unlock or re-lock:
	//临时解锁或重新锁定：
	void Lock(void);
	void Unlock(void);

private:
	DISALLOW_COPY_AND_ASSIGN(cCSLock);
} ;





/** Temporary RAII unlock for a cCSLock. Useful for unlock-wait-relock scenarios */
/**cCSLock的临时RAII解锁。用于解锁等待重新锁定场景*/
class cCSUnlock
{
	cCSLock & m_Lock;
public:
	cCSUnlock(cCSLock & a_Lock);
	~cCSUnlock();

private:
	DISALLOW_COPY_AND_ASSIGN(cCSUnlock);
} ;




