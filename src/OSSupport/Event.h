
// Event.h

//到cEvent对象的接口，表示可以等待的同步原语

//使用C++11条件变量和互斥体实现




#pragma once





class cEvent
{
  public:
	cEvent(void);

	/** Waits until the event has been set.
	If the event has been set before it has been waited for, Wait() returns
	immediately. */
	/**等待事件设置完毕。

	如果事件在等待之前已设置，Wait（）将返回

	马上*/
	void Wait(void);

	/** Sets the event - releases one thread that has been waiting in Wait().
	If there was no thread waiting, the next call to Wait() will not block. */
	/**设置事件-释放一个一直在等待（）的线程。

	如果没有线程等待，那么对Wait（）的下一个调用将不会阻塞*/
	void Set(void);

	/** Sets the event - releases all threads that have been waiting in Wait().
	If there was no thread waiting, the next call to Wait() will not block. */
	/**设置事件-释放一直在等待（）中等待的所有线程。

	如果没有线程等待，那么对Wait（）的下一个调用将不会阻塞*/
	void SetAll(void);

	/** Waits for the event until either it is signalled, or the (relative)
	timeout is passed. Returns true if the event was signalled, false if the
	timeout was hit or there was an error. */
	/**等待事件，直到发出信号或（相对）

	超时已过。如果发出了事件信号，则返回true；如果

	超时被命中或出现错误*/
	bool Wait(unsigned a_TimeoutMSec);

  private:
	/** Used for checking for spurious wakeups. */
	/**用于检查虚假唤醒*/
	bool m_ShouldContinue;

	/** Mutex protecting m_ShouldContinue from multithreaded access. */
	/**保护m_的互斥应继续进行多线程访问*/
	std::mutex m_Mutex;

	/** The condition variable used as the Event. */
	/**用作事件的条件变量*/
	std::condition_variable m_CondVar;
};
