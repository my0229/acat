
// IPLookup.h

// Declares the cIPLookup class representing an IP-to-hostname lookup in progress.

// This is an internal header, no-one outside OSSupport should need to include it; use Network.h instead
//声明表示正在进行的IP到主机名查找的cIPLookup类。

//这是一个内部标题，OSSupport之外的任何人都不需要包含它；改用Network.h




#pragma once

#include "Network.h"





/** Holds information about an in-progress IP-to-Hostname lookup. */
/**保存有关正在进行的IP到主机名查找的信息*/
class cIPLookup
{
public:

	/** Creates a lookup object and schedules the lookup. */
	/**创建查找对象并计划查找*/
	static void Lookup(const AString & a_IP, cNetwork::cResolveNameCallbacksPtr a_Callbacks);

protected:

	/** The callbacks to call for resolved names / errors. */
	/**调用已解析名称/错误的回调*/
	cNetwork::cResolveNameCallbacksPtr m_Callbacks;

	/** The IP that was queried (needed for the callbacks). */
	/**查询的IP（回调所需）*/


	AString m_IP;

	/** Creates the lookup object. Doesn't start the lookup yet. */
	/**创建查找对象。尚未启动查找*/
	cIPLookup(const AString & a_IP, cNetwork::cResolveNameCallbacksPtr a_Callbacks);

	/** Callback that is called by LibEvent when there's an event for the request. */
	/**当请求发生事件时，LibEvent调用的回调*/
	void Callback(int a_Result, const char * a_Address);
};
typedef std::shared_ptr<cIPLookup> cIPLookupPtr;
typedef std::vector<cIPLookupPtr> cIPLookupPtrs;





