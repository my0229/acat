
// Network.h

// Declares the classes used for the Network API
/* 声明用于网络API的类 */


#pragma once





// fwd:
class cTCPLink;
typedef std::shared_ptr<cTCPLink> cTCPLinkPtr;
typedef std::vector<cTCPLinkPtr> cTCPLinkPtrs;
class cServerHandle;
typedef std::shared_ptr<cServerHandle> cServerHandlePtr;
typedef std::vector<cServerHandlePtr> cServerHandlePtrs;
class cCryptoKey;//加密钥匙
typedef std::shared_ptr<cCryptoKey> cCryptoKeyPtr;
class cX509Cert;
typedef std::shared_ptr<cX509Cert> cX509CertPtr;





/** Interface that provides the methods available on a single TCP connection. */
/** 接口，该接口提供单个TCP连接上可用的方法 */
class cTCPLink
{
	friend class cNetwork;

public:
	class cCallbacks
	{
	public:
		// Force a virtual destructor for all descendants:
		//强制所有子体使用虚拟析构函数：
		virtual ~cCallbacks() {}

		/** Called when the cTCPLink for the connection is created.
		The callback may store the cTCPLink instance for later use, but it should remove it in OnError(), OnRemoteClosed() or right after Close(). */

		
		/**创建连接的cTCPLink时调用。

		回调可以存储cTCPLink实例供以后使用，但它应该在OnError（）、OnRemoteClosed（）或Close（）之后立即将其删除*/
		virtual void OnLinkCreated(cTCPLinkPtr a_Link) = 0;

		/** Called when there's data incoming from the remote peer. */
		/**当有数据从远程对等方传入时调用*/
		virtual void OnReceivedData(const char * a_Data, size_t a_Length) = 0;

		/** Called when the remote end closes the connection.
		The link is still available for connection information query (IP / port).
		Sending data on the link is not an error, but the data won't be delivered. */

		/**当远程端关闭连接时调用。

		链接仍可用于连接信息查询（IP/端口）。

		在链接上发送数据不是错误，但数据不会被传递*/
		virtual void OnRemoteClosed(void) = 0;

		/** Called when the TLS handshake has been completed and communication can continue regularly.
		Has an empty default implementation, so that link callback descendants don't need to specify TLS handlers when they don't use TLS at all. */
		/**TLS握手完成且通信可以正常继续时调用。

		具有空的默认实现，因此链接回调子体在根本不使用TLS时不需要指定TLS处理程序*/
		virtual void OnTlsHandshakeCompleted(void) {}

		/** Called when an error is detected on the connection. */
		/**在连接上检测到错误时调用*/
		virtual void OnError(int a_ErrorCode, const AString & a_ErrorMsg) = 0;
	};
	typedef std::shared_ptr<cCallbacks> cCallbacksPtr;


	// Force a virtual destructor for all descendants:
	//强制所有子体使用虚拟析构函数：
	virtual ~cTCPLink() {}

	/** Queues the specified data for sending to the remote peer.
	Returns true on success, false on failure. Note that this success or failure only reports the queue status, not the actual data delivery. */
	/**将指定的数据排入队列以发送到远程对等方。

	成功时返回true，失败时返回false。请注意，此成功或失败仅报告队列状态，而不是实际的数据传递*/
	virtual bool Send(const void * a_Data, size_t a_Length) = 0;

	/** Queues the specified data for sending to the remote peer.
	Returns true on success, false on failure. Note that this success or failure only reports the queue status, not the actual data delivery. */
	/**将指定的数据排入队列以发送到远程对等方。

	成功时返回true，失败时返回false。请注意，此成功或失败仅报告队列状态，而不是实际的数据传递*/
	bool Send(const AString & a_Data)
	{
		return Send(a_Data.data(), a_Data.size());
	}

	/** Returns the IP address of the local endpoint of the connection. */
	/**返回连接的本地终结点的IP地址*/
	virtual AString GetLocalIP(void) const = 0;

	/** Returns the port used by the local endpoint of the connection. */
	/**返回连接的本地端点使用的端口*/
	virtual UInt16 GetLocalPort(void) const = 0;

	/** Returns the IP address of the remote endpoint of the connection. */
	/**返回连接的远程端点的IP地址*/
	virtual AString GetRemoteIP(void) const = 0;

	/** Returns the port used by the remote endpoint of the connection. */
	/**返回连接的远程端点使用的端口*/
	virtual UInt16 GetRemotePort(void) const = 0;

	/** Closes the link gracefully.
	The link will send any queued outgoing data, then it will send the FIN packet.
	The link will still receive incoming data from remote until the remote closes the connection. */

	/**优雅地关闭链接。

	链路将发送任何排队的传出数据，然后发送FIN数据包。

	在远程设备关闭连接之前，链路仍将接收来自远程设备的传入数据*/
	virtual void Shutdown(void) = 0;

	/** Drops the connection without any more processing.
	Sends the RST packet, queued outgoing and incoming data is lost. */

	/**在不进行任何处理的情况下断开连接。

	发送RST数据包，排队的传出和传入数据丢失*/
	virtual void Close(void) = 0;

	/** Starts a TLS handshake as a client connection.
	If a client certificate should be used for the connection, set the certificate into a_OwnCertData and
	its corresponding private key to a_OwnPrivKeyData. If both are empty, no client cert is presented.
	a_OwnPrivKeyPassword is the password to be used for decoding PrivKey, empty if not passworded.
	Returns empty string on success, non-empty error description on failure. */
	/**作为客户端连接启动TLS握手。

	如果客户端证书应用于连接，请将证书设置为\u OwnCertData，然后

	它对应的私钥是一个_OwnPrivKeyData。如果两者都为空，则不显示客户端证书。

	a_OwnPrivKeyPassword是用于解码私钥的密码，如果没有密码，则为空。

	成功时返回空字符串，失败时返回非空错误描述*/
	virtual AString StartTLSClient(
		cX509CertPtr a_OwnCert,
		cCryptoKeyPtr a_OwnPrivKey
	) = 0;

	/** Starts a TLS handshake as a server connection.
	Set the server certificate into a_CertData and its corresponding private key to a_OwnPrivKeyData.
	a_OwnPrivKeyPassword is the password to be used for decoding PrivKey, empty if not passworded.
	a_StartTLSData is any data that should be pushed into the TLS before reading more data from the remote.
	This is used mainly for protocols starting TLS in the middle of communication, when the TLS start command
	can be received together with the TLS Client Hello message in one OnReceivedData() call, to re-queue the
	Client Hello message into the TLS handshake buffer.
	Returns empty string on success, non-empty error description on failure. */

	/**作为服务器连接启动TLS握手。

	将服务器证书设置为\u CertData，并将其相应的私钥设置为\u OwnPrivKeyData。

	a_OwnPrivKeyPassword是用于解码私钥的密码，如果没有密码，则为空。

	_StartTLSData是在从远程读取更多数据之前应推入TLS的任何数据。

	这主要用于在通信中间启动TLS的协议，当TLS开始命令时

	可以在一个OnReceivedData（）调用中与TLS客户端Hello消息一起接收，以重新排队

	将客户机Hello消息放入TLS握手缓冲区。

	成功时返回空字符串，失败时返回非空错误描述*/
	virtual AString StartTLSServer(
		cX509CertPtr a_OwnCert,
		cCryptoKeyPtr a_OwnPrivKey,
		const AString & a_StartTLSData
	) = 0;

	/** Returns the callbacks that are used. */
	/**返回所使用的回调*/
	cCallbacksPtr GetCallbacks(void) const { return m_Callbacks; }

protected:
	/** Callbacks to be used for the various situations. */
	 /**用于各种情况的回调*/
	cCallbacksPtr m_Callbacks;


	/** Creates a new link, with the specified callbacks. */
	/**使用指定的回调创建新链接*/
	cTCPLink(cCallbacksPtr a_Callbacks):
		m_Callbacks(std::move(a_Callbacks))
	{
	}
};
 




/** Interface that provides the methods available on a listening server socket. */
/**接口，该接口提供侦听服务器套接字上可用的方法*/
class cServerHandle
{
	friend class cNetwork;
public:

	// Force a virtual destructor for all descendants:
	//强制所有子体使用虚拟析构函数：
	virtual ~cServerHandle() {}

	/** Stops the server, no more incoming connections will be accepted.
	All current connections will be shut down (cTCPLink::Shutdown()). */
	/**停止服务器，将不再接受传入连接。

	所有当前连接都将关闭（cTCPLink:：Shutdown（））*/
	virtual void Close(void) = 0;

	/** Returns true if the server has been started correctly and is currently listening for incoming connections. */
	/**如果服务器已正确启动并且当前正在侦听传入连接，则返回true*/
	virtual bool IsListening(void) const = 0;
};





/** Interface that provides methods available on UDP communication endpoints. */

/**接口，该接口提供UDP通信端点上可用的方法*/
class cUDPEndpoint
{
public:
	/** Interface for the callbacks for events that can happen on the endpoint. */
	/**接口，用于回调端点上可能发生的事件*/
	class cCallbacks
	{
	public:
		// Force a virtual destructor in all descendants:

		//在所有子体中强制执行虚拟析构函数：
		virtual ~cCallbacks() {}

		/** Called when an error occurs on the endpoint. */
		/**在端点上发生错误时调用*/
		virtual void OnError(int a_ErrorCode, const AString & a_ErrorMsg) = 0;

		/** Called when there is an incoming datagram from a remote host. */
		/**当有来自远程主机的传入数据报时调用*/
		virtual void OnReceivedData(const char * a_Data, size_t a_Size, const AString & a_RemoteHost, UInt16 a_RemotePort) = 0;
	};


	// Force a virtual destructor for all descendants:
	//强制所有子体使用虚拟析构函数：
	virtual ~cUDPEndpoint() {}

	/** Closes the underlying socket.
	Note that there still might be callbacks in-flight after this method returns. */

	/**关闭基础套接字。

	请注意，在该方法返回后，仍可能有正在进行的回调*/
	virtual void Close(void) = 0;

	/** Returns true if the endpoint is open. */
	/**如果端点打开，则返回true*/
	virtual bool IsOpen(void) const = 0;

	/** Returns the local port to which the underlying socket is bound. */
	/**返回基础套接字绑定到的本地端口*/
	virtual UInt16 GetPort(void) const = 0;

	/** Sends the specified payload in a single UDP datagram to the specified host + port combination.
	Note that in order to send to a broadcast address, you need to call EnableBroadcasts() first. */
	/**将单个UDP数据报中的指定负载发送到指定的主机+端口组合。

	请注意，要发送到广播地址，首先需要调用EnableBroadcasts（）*/
	virtual bool Send(const AString & a_Payload, const AString & a_Host, UInt16 a_Port) = 0;

	/** Marks the socket as capable of sending broadcast, using whatever OS API is needed.
	Without this call, sending to a broadcast address using Send() may fail. */
	/**将套接字标记为能够使用所需的任何OS API发送广播。

	如果没有此调用，使用Send（）发送到广播地址可能会失败*/
	virtual void EnableBroadcasts(void) = 0;

protected:
	/** The callbacks used for various events on the endpoint. */
	/**用于端点上各种事件的回调*/
	cCallbacks & m_Callbacks;


	/** Creates a new instance of an endpoint, with the specified callbacks. */
	/**使用指定的回调创建端点的新实例*/
	cUDPEndpoint(cCallbacks & a_Callbacks):
		m_Callbacks(a_Callbacks)
	{
	}
};

typedef std::shared_ptr<cUDPEndpoint> cUDPEndpointPtr;





class cNetwork
{
public:
	/** Callbacks used for connecting to other servers as a client. */
	/**用于作为客户端连接到其他服务器的回调*/
	class cConnectCallbacks
	{
	public:
		// Force a virtual destructor for all descendants:
		//强制所有子体使用虚拟析构函数：
		virtual ~cConnectCallbacks() {}

		/** Called when the Connect call succeeds.
		Provides the newly created link that can be used for communication. */
		/**当连接调用成功时调用。

		提供新创建的可用于通信的链接*/
		virtual void OnConnected(cTCPLink & a_Link) = 0;

		/** Called when the Connect call fails. */
		/**当连接调用失败时调用*/
		virtual void OnError(int a_ErrorCode, const AString & a_ErrorMsg) = 0;
	};
	typedef std::shared_ptr<cConnectCallbacks> cConnectCallbacksPtr;


	/** Callbacks used when listening for incoming connections as a server. */
	/**作为服务器侦听传入连接时使用的回调*/
	class cListenCallbacks
	{
	public:
		// Force a virtual destructor for all descendants:
		//强制所有子体使用虚拟析构函数：
		virtual ~cListenCallbacks() {}

		/** Called when the TCP server created with Listen() receives a new incoming connection.
		Returns the link callbacks that the server should use for the newly created link.
		If a nullptr is returned, the connection is dropped immediately;
		otherwise a new cTCPLink instance is created and OnAccepted() is called. */

		
		/**当使用Listen（）创建的TCP服务器接收到新的传入连接时调用。

		返回服务器应用于新创建的链接的链接回调。

		如果返回null ptr，则立即断开连接；

		否则将创建一个新的cTCPLink实例并调用OnAccepted（）*/
		virtual cTCPLink::cCallbacksPtr OnIncomingConnection(const AString & a_RemoteIPAddress, UInt16 a_RemotePort) = 0;

		/** Called when the TCP server created with Listen() creates a new link for an incoming connection.
		Provides the newly created Link that can be used for communication.
		Called right after a successful OnIncomingConnection(). */
		/**当使用Listen（）创建的TCP服务器为传入连接创建新链接时调用。

		提供新创建的可用于通信的链接。

		在OnIncomingConnection（）成功后立即调用*/
		virtual void OnAccepted(cTCPLink & a_Link) = 0;

		/** Called when the socket fails to listen on the specified port. */
		/**当套接字无法侦听指定端口时调用*/
		virtual void OnError(int a_ErrorCode, const AString & a_ErrorMsg) = 0;
	};
	typedef std::shared_ptr<cListenCallbacks> cListenCallbacksPtr;


	/** Callbacks used when resolving names to IPs. */
	/**将名称解析为IP时使用的回调*/
	class cResolveNameCallbacks
	{
	public:
		// Force a virtual destructor for all descendants:
		//强制所有子体使用虚拟析构函数：
		virtual ~cResolveNameCallbacks() {}

		/** Called when the hostname is successfully resolved into an IP address.
		May be called multiple times if a name resolves to multiple addresses.
		a_IP may be either an IPv4 or an IPv6 address with their proper formatting.
		Each call to OnNameResolved() is preceded by a call to either OnNameResolvedV4() or OnNameResolvedV6(). */
		/**当主机名成功解析为IP地址时调用。

		如果名称解析为多个地址，则可以多次调用。

		IP可以是IPv4地址，也可以是具有正确格式的IPv6地址。

		每次调用onnamesolved（）之前都会调用onnamesolvedv4（）或onnamesolvedv6（）*/
		virtual void OnNameResolved(const AString & a_Name, const AString & a_IP) = 0;

		/** Called when the hostname is successfully resolved into an IPv4 address.
		May be called multiple times if a name resolves to multiple addresses.
		Each call to OnNameResolvedV4 is followed by OnNameResolved with the IP address serialized to a string.
		If this callback returns false, the OnNameResolved() call is skipped for this address. */
		/**当主机名成功解析为IPv4地址时调用。

		如果名称解析为多个地址，则可以多次调用。

		每次调用OnNameResolvedV4后都会出现OnNameResolved，IP地址序列化为字符串。

		如果此回调返回false，则跳过此地址的onnamesolved（）调用*/
		virtual bool OnNameResolvedV4(const AString & a_Name, const sockaddr_in * a_IP) { return true; }

		/** Called when the hostname is successfully resolved into an IPv6 address.
		May be called multiple times if a name resolves to multiple addresses.
		Each call to OnNameResolvedV4 is followed by OnNameResolved with the IP address serialized to a string.
		If this callback returns false, the OnNameResolved() call is skipped for this address. */

		/**当主机名成功解析为IPv6地址时调用。

		如果名称解析为多个地址，则可以多次调用。

		每次调用OnNameResolvedV4后都会出现OnNameResolved，IP地址序列化为字符串。

		如果此回调返回false，则跳过此地址的onnamesolved（）调用*/
		virtual bool OnNameResolvedV6(const AString & a_Name, const sockaddr_in6 * a_IP) { return true; }

		/** Called when an error is encountered while resolving.
		If an error is reported, the OnFinished() callback is not called. */

		
		/**在解析时遇到错误时调用。

		如果报告错误，则不会调用OnFinished（）回调*/
		virtual void OnError(int a_ErrorCode, const AString & a_ErrorMsg) = 0;

		/** Called when all the addresses resolved have been reported via the OnNameResolved() callback.
		Only called if there was no error reported. */
		/**通过onnamesolved（）回调报告所有解析的地址时调用。

		仅在未报告错误时调用*/
		virtual void OnFinished(void) = 0;
	};
	typedef std::shared_ptr<cResolveNameCallbacks> cResolveNameCallbacksPtr;


	/** Queues a TCP connection to be made to the specified host.
	Calls one the connection callbacks (success, error) when the connection is successfully established, or upon failure.
	The a_LinkCallbacks is passed to the newly created cTCPLink.
	Returns true if queueing was successful, false on failure to queue.
	Note that the return value doesn't report the success of the actual connection; the connection is established asynchronously in the background.
	Implemented in TCPLinkImpl.cpp. */

	/**将要与指定主机建立的TCP连接排队。

	当连接成功建立或失败时，调用其中一个连接回调（成功、错误）。

	a_link回调被传递给新创建的cTCPLink。

	如果排队成功，则返回true；如果排队失败，则返回false。

	请注意，返回值并不报告实际连接的成功；连接是在后台异步建立的。

	在TCPLinkImpl.cpp中实现*/
	static bool Connect(
		const AString & a_Host,
		UInt16 a_Port,
		cConnectCallbacksPtr a_ConnectCallbacks,
		cTCPLink::cCallbacksPtr a_LinkCallbacks
	);


	/** Opens up the specified port for incoming connections.
	Calls an OnAccepted callback for each incoming connection.
	A cTCPLink with the specified link callbacks is created for each connection.
	Returns a cServerHandle that can be used to query the operation status and close the server.
	Implemented in ServerHandleImpl.cpp. */
	/**为传入连接打开指定端口。

为每个传入连接调用OnAccepted回调。

为每个连接创建具有指定链接回调的cTCPLink。

返回可用于查询操作状态和关闭服务器的CSServerHandle。

在ServerHandleImpl.cpp中实现*/
	static cServerHandlePtr Listen(
		UInt16 a_Port,
		cListenCallbacksPtr a_ListenCallbacks
	);


	/** Queues a DNS query to resolve the specified hostname to IP address.
	Calls one of the callbacks when the resolving succeeds, or when it fails.
	Returns true if queueing was successful, false if not.
	Note that the return value doesn't report the success of the actual lookup; the lookup happens asynchronously on the background.
	Implemented in HostnameLookup.cpp. */
	/**将DNS查询排队以将指定的主机名解析为IP地址。

	解析成功或失败时调用其中一个回调。

	如果排队成功，则返回true，否则返回false。

	请注意，返回值并不报告实际查找的成功；查找在后台异步进行。

	在HostnameLookup.cpp中实现*/
	static bool HostnameToIP(
		const AString & a_Hostname,
		cResolveNameCallbacksPtr a_Callbacks
	);


	/** Queues a DNS query to resolve the specified IP address to a hostname.
	Calls one of the callbacks when the resolving succeeds, or when it fails.
	Returns true if queueing was successful, false if not.
	Note that the return value doesn't report the success of the actual lookup; the lookup happens asynchronously on the background.
	Implemented in IPLookup.cpp. */
	/**将DNS查询排队以将指定的IP地址解析为主机名。

	解析成功或失败时调用其中一个回调。

	如果排队成功，则返回true，否则返回false。

	请注意，返回值并不报告实际查找的成功；查找在后台异步进行。

	在IPLookup.cpp中实现*/
	static bool IPToHostName(
		const AString & a_IP,
		cResolveNameCallbacksPtr a_Callbacks
	);

	/** Opens up an UDP endpoint for sending and receiving UDP datagrams on the specified port.
	If a_Port is 0, the OS is free to assign any port number it likes to the endpoint.
	Returns the endpoint object that can be interacted with. */
	/**打开UDP端点，用于在指定端口上发送和接收UDP数据报。

	如果某个_端口为0，则操作系统可以自由地向端点分配它喜欢的任何端口号。

	返回可与之交互的端点对象*/
	static cUDPEndpointPtr CreateUDPEndpoint(UInt16 a_Port, cUDPEndpoint::cCallbacks & a_Callbacks);

	/** Returns all local IP addresses for network interfaces currently available. */
	/**返回当前可用网络接口的所有本地IP地址*/
	static AStringVector EnumLocalIPAddresses(void);
};
