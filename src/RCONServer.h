
// RCONServer.h

// Declares the cRCONServer class representing the RCON server
//声明表示RCON服务器的cRCONServer类
/*Rcon是一种协议,是被Source专用服务器使用的一个基于TCP/IP协议的通信协议,
可以通过rcon向服务器发送控制台命令,
rcon的作用最常见的用法是让服主在不接触服务器的情况下控制自己的游戏服务器.
为了让命令能被接受，
建立的连接必须先通过服务器的rcon密码验证.*/



#pragma once

#include "OSSupport/Network.h"





// fwd:
class cServer;
class cSettingsRepositoryInterface; //设置存储库接口


//创建服务时调用


class cRCONServer
{
public:
	cRCONServer(cServer & a_Server);
	virtual ~cRCONServer();

	void Initialize(cSettingsRepositoryInterface & a_Settings);

protected:
	friend class cRCONCommandOutput;
	friend class cRCONListenCallbacks;

	class cConnection :
		public cTCPLink::cCallbacks
	{
	public:
		cConnection(cRCONServer & a_RCONServer, const AString & a_IPAddress);

	protected:
		friend class cRCONCommandOutput;

		/** Set to true if the client has successfully authenticated */
		/**如果客户端已成功进行身份验证，则设置为true*/
		bool m_IsAuthenticated;

		/** Buffer for the incoming data */
		/**输入数据的缓冲区*/
		AString m_Buffer;

		/** Server that owns this connection and processes requests */
		/**拥有此连接并处理请求的服务器*/
		cRCONServer & m_RCONServer;

		/** The TCP link to the client */
		/**到客户端的TCP链接*/
		cTCPLinkPtr m_Link;

		/** Address of the client */

		/**客户地址*/
		AString m_IPAddress;


		// cTCPLink::cCallbacks overrides:
		virtual void OnLinkCreated(cTCPLinkPtr a_Link) override;
		virtual void OnReceivedData(const char * a_Data, size_t a_Length) override;
		virtual void OnRemoteClosed(void) override;
		virtual void OnError(int a_ErrorCode, const AString & a_ErrorMsg) override;

		/** Processes the given packet and sends the response; returns true if successful, false if the connection is to be dropped */
		/**处理给定分组并发送响应；如果成功，则返回true；如果要断开连接，则返回false*/
		bool ProcessPacket(UInt32 a_RequestID, UInt32 a_PacketType, UInt32 a_PayloadLength, const char * a_Payload);

		/** Reads 4 bytes from a_Buffer and returns the LE UInt32 they represent */
		/**从_缓冲区读取4个字节，并返回它们表示的LE UInt32*/
		UInt32 UIntFromBuffer(const char * a_Buffer);

		/** Puts 4 bytes representing the int into the buffer */
		/**将表示int的4个字节放入缓冲区*/
		void UIntToBuffer(UInt32 a_Value, char * a_Buffer);

		/** Sends a RCON packet back to the client */
		/**将RCON数据包发送回客户端*/
		void SendResponse(UInt32 a_RequestID, UInt32 a_PacketType, UInt32 a_PayloadLength, const char * a_Payload);
	} ;


	/** The server object that will process the commands received */
	/**将处理接收到的命令的服务器对象*/
	cServer & m_Server;

	/** The sockets for accepting RCON connections (one socket per port). */
	/**用于接受RCON连接的插座（每个端口一个插座）*/
	cServerHandlePtrs m_ListenServers;

	/** Password for authentication */
	/**用于身份验证的密码*/
	AString m_Password;
} ;





