
// ByteStream.h

// Interfaces to the cByteBuffer class representing a ringbuffer of bytes
//表示字节的ringbuffer的cByteBuffer类的接口




#pragma once



// fwd:
class cUUID;


/** An object that can store incoming bytes and lets its clients read the bytes sequentially
The bytes are stored in a ringbuffer of constant size; if more than that size
is requested, the write operation fails.
The bytes stored can be retrieved using various ReadXXX functions; these assume that the needed
number of bytes are present in the buffer (ASSERT; for performance reasons).
The reading doesn't actually remove the bytes, it only moves the internal read ptr.
To remove the bytes, call CommitRead().
To re-start reading from the beginning, call ResetRead().
This class doesn't implement thread safety, the clients of this class need to provide
their own synchronization.
*/
/**一个可以存储传入字节并允许其客户端按顺序读取字节的对象

字节存储在恒定大小的环形缓冲区中；如果超过那个尺寸

请求时，写入操作失败。

可以使用各种ReadXXX函数检索存储的字节；这些假设是需要

缓冲区中存在的字节数（断言；出于性能原因）。

读取实际上并不删除字节，它只移动内部读取ptr。

要删除字节，请调用CommitRead（）。

要从头开始读取，请调用ResetRead（）。

此类不实现线程安全，此类的客户端需要提供

他们自己的同步。

*/
class cByteBuffer
{
public:
	cByteBuffer(size_t a_BufferSize);
	~cByteBuffer();

	/** Writes the bytes specified to the ringbuffer. Returns true if successful, false if not */
	/**将指定的字节写入ringbuffer。如果成功，则返回true；如果不成功，则返回false*/
	bool Write(const void * a_Bytes, size_t a_Count);

	/** Returns the number of bytes that can be successfully written to the ringbuffer */
	/**返回可成功写入ringbuffer的字节数*/
	size_t GetFreeSpace(void) const;

	/** Returns the number of bytes that are currently in the ringbuffer. Note GetReadableBytes() */
	/**返回当前在ringbuffer中的字节数。注意GetReadableBytes（）*/
	size_t GetUsedSpace(void) const;

	/** Returns the number of bytes that are currently available for reading (may be less than UsedSpace due to some data having been read already) */
	/**返回当前可读取的字节数（由于某些数据已被读取，可能小于UsedSpace）*/
	size_t GetReadableSpace(void) const;

	/** Returns the current data start index. For debugging purposes. */
	/**返回当前数据开始索引。用于调试目的*/
	size_t  GetDataStart(void) const { return m_DataStart; }

	/** Returns true if the specified amount of bytes are available for reading */
	/**如果指定的字节数可供读取，则返回true*/
	bool CanReadBytes(size_t a_Count) const;

	/** Returns true if the specified amount of bytes are available for writing */
	/**如果指定的字节数可供写入，则返回true*/
	bool CanWriteBytes(size_t a_Count) const;

	// Read the specified datatype and advance the read pointer; return true if successfully read:
	//读取指定的数据类型并前进读取指针；如果成功读取，则返回true：
	bool ReadBEInt8         (Int8 & a_Value);
	bool ReadBEInt16        (Int16 & a_Value);
	bool ReadBEInt32        (Int32 & a_Value);
	bool ReadBEInt64        (Int64 & a_Value);
	bool ReadBEUInt8        (UInt8 & a_Value);
	bool ReadBEUInt16       (UInt16 & a_Value);
	bool ReadBEUInt32       (UInt32 & a_Value);
	bool ReadBEUInt64       (UInt64 & a_Value);
	bool ReadBEFloat        (float & a_Value);
	bool ReadBEDouble       (double & a_Value);
	bool ReadBool           (bool & a_Value);
	bool ReadVarInt32       (UInt32 & a_Value);
	bool ReadVarInt64       (UInt64 & a_Value);
	bool ReadVarUTF8String  (AString & a_Value);  // string length as VarInt, then string as UTF-8 //字符串长度作为变量，然后字符串作为UTF-8
	bool ReadLEInt          (int & a_Value);
	bool ReadXYZPosition64  (int & a_BlockX, int & a_BlockY, int & a_BlockZ);
	bool ReadXZYPosition64  (int & a_BlockX, int & a_BlockY, int & a_BlockZ);
	bool ReadUUID           (cUUID & a_Value);

	/** Reads VarInt, assigns it to anything that can be assigned from an UInt64 (unsigned short, char, Byte, double, ...) */
	/**读取变量，将其分配给可以从UInt64分配的任何内容（无符号短、字符、字节、双精度等）*/
	template <typename T> bool ReadVarInt(T & a_Value)
	{
		UInt64 v;
		bool res = ReadVarInt64(v);
		if (res)
		{
			a_Value = static_cast<T>(v);
		}
		return res;
	}

	// Write the specified datatype; return true if successfully written
	//写入指定的数据类型；如果成功写入，则返回true
	bool WriteBEInt8         (Int8   a_Value);
	bool WriteBEInt16        (Int16  a_Value);
	bool WriteBEInt32        (Int32  a_Value);
	bool WriteBEInt64        (Int64  a_Value);
	bool WriteBEUInt8        (UInt8  a_Value);
	bool WriteBEUInt16       (UInt16 a_Value);
	bool WriteBEUInt32       (UInt32 a_Value);
	bool WriteBEUInt64       (UInt64 a_Value);
	bool WriteBEFloat        (float  a_Value);
	bool WriteBEDouble       (double a_Value);
	bool WriteBool           (bool   a_Value);
	bool WriteVarInt32       (UInt32 a_Value);
	bool WriteVarInt64       (UInt64 a_Value);
	bool WriteVarUTF8String  (const AString & a_Value);  // string length as VarInt, then string as UTF-8 //字符串长度作为变量，然后字符串作为UTF-8
	bool WriteLEInt32        (Int32 a_Value);
	bool WriteXYZPosition64  (Int32 a_BlockX, Int32 a_BlockY, Int32 a_BlockZ);
	bool WriteXZYPosition64  (Int32 a_BlockX, Int32 a_BlockY, Int32 a_BlockZ);

	/** Reads a_Count bytes into a_Buffer; returns true if successful */
	/**将\u计数字节读入\u缓冲区；如果成功，则返回true*/
	bool ReadBuf(void * a_Buffer, size_t a_Count);

	/** Writes a_Count bytes into a_Buffer; returns true if successful */
	/**将\u计数字节写入\u缓冲区；如果成功，则返回true*/
	bool WriteBuf(const void * a_Buffer, size_t a_Count);

	/** Reads a_Count bytes into a_String; returns true if successful */
	/**将\u计数字节读入\u字符串；如果成功，则返回true*/
	bool ReadString(AString & a_String, size_t a_Count);

	/** Skips reading by a_Count bytes; returns false if not enough bytes in the ringbuffer */
	/**跳过一个字节计数的读取；如果ringbuffer中没有足够的字节，则返回false*/
	bool SkipRead(size_t a_Count);

	/** Reads all available data into a_Data */
	/**将所有可用数据读入一个_数据*/
	void ReadAll(AString & a_Data);

	/** Reads the specified number of bytes and writes it into the destinatio bytebuffer. Returns true on success. */
	/**读取指定的字节数并将其写入destinatio bytebuffer。成功时返回true*/
	bool ReadToByteBuffer(cByteBuffer & a_Dst, size_t a_NumBytes);

	/** Removes the bytes that have been read from the ringbuffer */
	/**删除已从ringbuffer读取的字节*/
	void CommitRead(void);

	/** Restarts next reading operation at the start of the ringbuffer */
	/**在ringbuffer的开始处重新启动下一个读取操作*/
	void ResetRead(void);

	/** Re-reads the data that has been read since the last commit to the current readpos. Used by ProtoProxy to duplicate communication */
	/**重新读取上次提交到当前readpos后读取的数据。ProtoProxy用于复制通信*/
	void ReadAgain(AString & a_Out);

	/** Checks if the internal state is valid (read and write positions in the correct bounds) using ASSERTs */
	/**使用断言检查内部状态是否有效（读写位置在正确的界限内）*/
	void CheckValid(void) const;

	/** Gets the number of bytes that are needed to represent the given VarInt */
	/**获取表示给定变量所需的字节数*/
	static size_t GetVarIntSize(UInt32 a_Value);

protected:
	char * m_Buffer;
	size_t m_BufferSize;  // Total size of the ringbuffer //环形缓冲区的总大小

	size_t m_DataStart;  // Where the data starts in the ringbuffer //数据在ringbuffer中的起始位置
	size_t m_WritePos;   // Where the data ends in the ringbuffer //其中数据以ringbuffer结尾
	size_t m_ReadPos;    // Where the next read will start in the ringbuffer //下一次读取将在ringbuffer中开始的位置

	#ifdef _DEBUG
		/** The ID of the thread currently accessing the object.
		Used for checking that only one thread accesses the object at a time, via cSingleThreadAccessChecker. */
	/**当前访问对象的线程的ID。

	用于检查一次只有一个线程通过cSingleThreadAccessChecker访问对象*/
		mutable std::thread::id m_ThreadID;
	#endif

	/** Advances the m_ReadPos by a_Count bytes */
	/**将m_ReadPos提前一个字节计数*/
	void AdvanceReadPos(size_t a_Count);
} ;




