#include "Globals.h"

#include "Server.h"
#include "ClientHandle.h" 
#include "OSSupport/Network.h"

#include "IniFile.h"		


#include <fstream>
#include <sstream>
#include <iostream>

typedef std::list< cClientHandle* > ClientList;
////////////////////////////////////////////////////////////////////////////////
// cServerListenCallbacks:
//服务器侦听回调：
class cServerListenCallbacks:
	public cNetwork::cListenCallbacks
{
	cServer & m_Server;
	UInt16 m_Port;

	virtual cTCPLink::cCallbacksPtr OnIncomingConnection(const AString & a_RemoteIPAddress, UInt16 a_RemotePort) override
	{
		return m_Server.OnConnectionAccepted(a_RemoteIPAddress);
	}

	virtual void OnAccepted(cTCPLink & a_Link) override {}

	virtual void OnError(int a_ErrorCode, const AString & a_ErrorMsg) override
	{
		LOGWARNING("Cannot listen on port %d: %d (%s).", m_Port, a_ErrorCode, a_ErrorMsg.c_str());
	}

public:
	cServerListenCallbacks(cServer & a_Server, UInt16 a_Port):
		m_Server(a_Server),
		m_Port(a_Port)
	{
	}
};



bool cServer::Start()
{
	for (const auto & port: m_Ports)
	{
		UInt16 PortNum;
		if (!StringToInteger(port, PortNum))
		{
			LOGWARNING("Invalid port specified for server: \"%s\". Ignoring.", port.c_str());//LOGWARNING（“为服务器指定的端口无效：\%s\”。忽略“”，port.c_str（））；
			continue;
		}
		auto Handle = cNetwork::Listen(PortNum, std::make_shared<cServerListenCallbacks>(*this, PortNum));
		if (Handle->IsListening())
		{
			m_ServerHandles.push_back(Handle);
		}
	}  // for port - Ports[]
	if (m_ServerHandles.empty())
	{
		LOGERROR("Couldn't open any ports. Aborting the server");//LOGERROR（“无法打开任何端口。中止服务器”）；
		return false;
	}
	return m_TickThread.Start();
}

cServer::cServer(void):
	m_PlayerCount(0),
	m_ClientViewDistance(0),
	m_bIsConnected(false),
	m_bRestarting(false),
	m_MaxPlayers(0),
	m_bIsHardcore(false),
	m_TickThread(*this),
	m_ShouldAuthenticate(false),
	m_ShouldLoadOfflinePlayerData(false),
	m_ShouldLoadNamedPlayerData(true)
{
}

bool cServer::InitServer(cSettingsRepositoryInterface & a_Settings, bool a_ShouldAuth)
{
	m_Description = a_Settings.GetValueSet("Server", "Description", "Cuberite - in C++!");
	m_ShutdownMessage = a_Settings.GetValueSet("Server", "ShutdownMessage", "Server shutdown");
	m_MaxPlayers = static_cast<size_t>(a_Settings.GetValueSetI("Server", "MaxPlayers", 100));
	m_bIsHardcore = a_Settings.GetValueSetB("Server", "HardcoreEnabled", false);
	m_bAllowMultiLogin = a_Settings.GetValueSetB("Server", "AllowMultiLogin", false);
	m_ResourcePackUrl = a_Settings.GetValueSet("Server", "ResourcePackUrl", "");

	m_FaviconData = Base64Encode(cFile::ReadWholeFile(AString("favicon.png")));  // Will return empty string if file nonexistant; client doesn't mind
																				 //如果文件不存在，将返回空字符串；客户不介意
	LOGINFO("Compatible clients: %s", "协议正在完善"); //LOGINFO（“兼容客户端：%s”，MCS\u客户端\u版本）；
	LOGD("Compatible protocol versions %s", "协议正在完善");//LOGD（“兼容协议版本%s”，MCS\u协议版本）；

	m_Ports = ReadUpgradeIniPorts(a_Settings, "Server", "Ports", "Port", "PortsIPv6", "25565");

	//m_RCONServer.Initialize(a_Settings);

	m_bIsConnected = true;//???


	m_ServerID = "-";
	m_ShouldAuthenticate = a_ShouldAuth;
	/*
	if (m_ShouldAuthenticate)
	{
		auto & rand = GetRandomProvider();
		unsigned int r1 = rand.RandInt<unsigned int>(1000000000U, 0x7fffffffU);
		unsigned int r2 = rand.RandInt<unsigned int>(1000000000U, 0x7fffffffU);
		std::ostringstream sid;
		sid << std::hex << r1;
		sid << std::hex << r2;
		m_ServerID = sid.str();
		m_ServerID.resize(16, '0');
	}
	*/

	// Check if both BungeeCord and online mode are on, if so, warn the admin:
	//检查BungeeCord和online模式是否都已打开，如果已打开，请警告管理员：
	m_ShouldAllowBungeeCord = a_Settings.GetValueSetB("Authentication", "AllowBungeeCord", false);
	if (m_ShouldAllowBungeeCord && m_ShouldAuthenticate)
	{
		LOGWARNING("WARNING: BungeeCord is allowed and server set to online mode. This is unsafe and will not work properly. Disable either authentication or BungeeCord in settings.ini.");
		//LOGWARNING（“警告：允许使用BungeRecord，服务器设置为联机模式。这是不安全的，不能正常工作。禁用settings.ini中的身份验证或BungeRecord。“）；
	}

	m_ShouldAllowMultiWorldTabCompletion = a_Settings.GetValueSetB("Server", "AllowMultiWorldTabCompletion", true);
	m_ShouldLimitPlayerBlockChanges = a_Settings.GetValueSetB("AntiCheat", "LimitPlayerBlockChanges", true);
	m_ShouldLoadOfflinePlayerData = a_Settings.GetValueSetB("PlayerData", "LoadOfflinePlayerData", false);
	m_ShouldLoadNamedPlayerData   = a_Settings.GetValueSetB("PlayerData", "LoadNamedPlayerData", true);

	m_ClientViewDistance = a_Settings.GetValueSetI("Server", "DefaultViewDistance",10);//默认玩家视距

	//PrepareKeys(); //加载密钥

	return true;
}

void cServer::AuthenticateUser(int a_ClientID, const AString & a_Name, const cUUID & a_UUID, const Json::Value & a_Properties)
{
	cCSLock Lock(m_CSClients);

	// Check max players condition within lock (expect server and authenticator thread to both call here)
	//检查锁内的max players条件（此处除服务器和身份验证程序线程调用外）
	if (GetNumPlayers() >= GetMaxPlayers())
	{
		//KickUser(a_ClientID, "The server is currently full :(" "\n" "Try again later?");
		return;
	}

	for (auto itr = m_Clients.begin(); itr != m_Clients.end(); ++itr)
	{
		if ((*itr)->GetUniqueID() == a_ClientID)
		{
			(*itr)->Authenticate(a_Name, a_UUID, a_Properties);
			return;
		}
	}  // for itr - m_Clients[]


}

void cServer::PlayerCreated()
{
	m_PlayerCount++;
}

void cServer::KickUser(int a_ClientID, const AString & a_Reason)
{
	cCSLock Lock(m_CSClients);
	for (auto itr = m_Clients.begin(); itr != m_Clients.end(); ++itr)
	{
		if ((*itr)->GetUniqueID() == a_ClientID)
		{
			(*itr)->Kick(a_Reason);
		}
	}  // for itr - m_Clients[]
}


bool cServer::Tick(float a_Dt)
{
		// Tick all clients not yet assigned to a world:
	//勾选尚未分配到世界的所有客户端：
	TickClients(a_Dt);

	if (!m_bRestarting)
	{
		return true;
	}
	else
	{
		m_bRestarting = false;
		m_RestartEvent.Set();
		return false;
	}
}

cTCPLink::cCallbacksPtr cServer::OnConnectionAccepted(const AString & a_RemoteIPAddress)
{
	LOGD("Client \"%s\" connected!", a_RemoteIPAddress.c_str());
	cClientHandlePtr NewHandle = std::make_shared<cClientHandle>(a_RemoteIPAddress, m_ClientViewDistance);
	NewHandle->SetSelf(NewHandle);
	cCSLock Lock(m_CSClients);
	m_Clients.push_back(NewHandle);
	return std::move(NewHandle);
}

void cServer::TickClients(float a_Dt)
{
	cClientHandlePtrs RemoveClients;
	{
		cCSLock Lock(m_CSClients);

		// Remove clients that have moved to a world (the world will be ticking them from now on)
		//删除已移动到某个世界的客户端（从现在起，世界将为它们打勾）
		for (auto itr = m_ClientsToRemove.begin(), end = m_ClientsToRemove.end(); itr != end; ++itr)
		{
			for (auto itrC = m_Clients.begin(), endC = m_Clients.end(); itrC != endC; ++itrC)
			{
				if (itrC->get() == *itr)
				{
					m_Clients.erase(itrC);
					//处理迭代器失效的
					break;
				}
			}
		}  // for itr - m_ClientsToRemove[]
		m_ClientsToRemove.clear();	//清理

		// Tick the remaining clients, take out those that have been destroyed into RemoveClients
		//勾选其余的客户端，取出那些已销毁到RemoveClient中的客户端
		for (auto itr = m_Clients.begin(); itr != m_Clients.end();)
		{
			if ((*itr)->IsDestroyed())
			{
				// Delete the client later, when CS is not held, to avoid deadlock: https://forum.cuberite.org/thread-374.html
				//稍后在未持有CS时删除客户端，以避免死锁：https://forum.cuberite.org/thread-374.html
				RemoveClients.push_back(*itr);
				itr = m_Clients.erase(itr);
				continue;
			}
			(*itr)->ServerTick(a_Dt);
			++itr;
		}  // for itr - m_Clients[]
	}

	//清理
	RemoveClients.clear();
}

////////////////////////////////////////////////////////////////////////////////
// cServer::cTickThread:

cServer::cTickThread::cTickThread(cServer & a_Server) :
	Super("ServerTickThread"),
	m_Server(a_Server)
{
}

void cServer::cTickThread::Execute(void)
{
	auto LastTime = std::chrono::steady_clock::now();
	static const auto msPerTick = std::chrono::milliseconds(50);

	while (!m_ShouldTerminate)
	{
		auto NowTime = std::chrono::steady_clock::now();
		auto msec = std::chrono::duration_cast<std::chrono::milliseconds>(NowTime - LastTime).count();
		m_ShouldTerminate = !m_Server.Tick(static_cast<float>(msec));
		auto TickTime = std::chrono::steady_clock::now() - NowTime;

		if (TickTime < msPerTick)
		{
			// Stretch tick time until it's at least msPerTick
			std::this_thread::sleep_for(msPerTick - TickTime);
		}

		LastTime = NowTime;
	}
}


