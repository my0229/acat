
#pragma once

class cSettingsRepositoryInterface
{
public:

	enum errors
	{
		noID = -1,
	};

	virtual ~cSettingsRepositoryInterface() = default;

	/** Returns true iff the specified key exists */
	/**如果指定的密钥存在，则返回true*/
	virtual bool KeyExists(const AString keyname) const = 0;

	/** Returns true iff the specified value exists. */
	/**如果指定的值存在，则返回true*/
	virtual bool HasValue(const AString & a_KeyName, const AString & a_ValueName) const = 0;

	/** Add a key name. Return value is not required to mean anything */
	/**添加一个键名。返回值不要求有任何意义*/
	virtual int AddKeyName(const AString & keyname) = 0;

	/** Add a key comment, will always fail if the repository does not support comments */
	/**如果存储库不支持注释，则添加关键注释将始终失败*/
	virtual bool AddKeyComment(const AString & keyname, const AString & comment) = 0;

	/** Return a key comment, returns "" for repositories that do not return comments */
	/**返回键注释，对于不返回注释的存储库，返回“”*/
	virtual AString GetKeyComment(const AString & keyname, const int commentID) const = 0;

	/** Delete a key comment, will always fail if the repository does not support comments */
	/**如果存储库不支持注释，则删除关键注释将始终失败*/
	virtual bool DeleteKeyComment(const AString & keyname, const int commentID) = 0;

	/** Adds a new value to the specified key.
	If a value of the same name already exists, creates another one */
	/**将新值添加到指定的键。

	如果已存在同名的值，则创建另一个值*/
	virtual void AddValue (const AString & a_KeyName, const AString & a_ValueName, const AString & a_Value) = 0;

	/** returns a vector containing a name, value pair for each value under the key */
	/**返回一个向量，该向量包含键下每个值的名称、值对*/
	virtual std::vector<std::pair<AString, AString>> GetValues(AString a_keyName) = 0;

	/** Get the value at the specified key and value, returns defValue on failure */
	/**获取指定键和值处的值，失败时返回defValue*/
	virtual AString GetValue (const AString & keyname, const AString & valuename, const AString & defValue = "")    const = 0;

	/** Gets the value; if not found, write the default to the repository */
	/**获取值；如果未找到，请将默认值写入存储库*/
	virtual AString GetValueSet (const AString & keyname, const AString & valuename, const AString & defValue = "") = 0;
	virtual int     GetValueSetI(const AString & keyname, const AString & valuename, const int       defValue = 0) = 0;
	virtual Int64   GetValueSetI(const AString & keyname, const AString & valuename, const Int64     defValue = 0) = 0;
	virtual bool    GetValueSetB(const AString & keyname, const AString & valuename, const bool      defValue = false) = 0;

	/** Overwrites the value of the key, value pair
	Specify the optional parameter as false if you do not want the value created if it doesn't exist.
	Returns true if value set, false otherwise. */
	/**覆盖键、值对的值

	如果不希望创建不存在的值，请将可选参数指定为false。

	如果设置了值，则返回true，否则返回false*/
	virtual bool SetValue (const AString & a_KeyName, const AString & a_ValueName, const AString & a_Value, const bool a_CreateIfNotExists = true) = 0;
	virtual bool SetValueI(const AString & a_KeyName, const AString & a_ValueName, const int a_Value, const bool a_CreateIfNotExists = true) = 0;

	/** Deletes the specified key, value pair */
	/**删除指定的键、值对*/
	virtual bool DeleteValue(const AString & keyname, const AString & valuename) = 0;


	/** Writes the changes to the backing store, if the repository has one */
	/**将更改写入备份存储（如果存储库有）*/
	virtual bool Flush() = 0;
};
