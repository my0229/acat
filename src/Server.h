
// cServer.h

// Interfaces to the cServer object representing the network server
//到表示网络服务器的cServer对象的接口




#pragma once

#include "RCONServer.h"
#include "OSSupport/IsThread.h"
#include "OSSupport/Network.h"
#include "json/json.h"

#ifdef _MSC_VER
	#pragma warning(push)
	#pragma warning(disable:4127)
	#pragma warning(disable:4244)
	#pragma warning(disable:4231)
	#pragma warning(disable:4189)
	#pragma warning(disable:4702)
#endif

#include "mbedTLS++/RsaPrivateKey.h"

#ifdef _MSC_VER
	#pragma warning(pop)
#endif





// fwd:
class cClientHandle;
typedef std::shared_ptr<cClientHandle> cClientHandlePtr;
typedef std::list<cClientHandlePtr> cClientHandlePtrs;
typedef std::list<cClientHandle *> cClientHandles;
class cCommandOutputCallback;
class cSettingsRepositoryInterface;
class cUUID;


class cServer
{
public:
	bool Start();

	cServer(void);

	bool InitServer(cSettingsRepositoryInterface & a_Settings, bool a_ShouldAuth);

	const AString & GetDescription(void) const {return m_Description; }

	// Player counts:
	size_t GetMaxPlayers(void) const { return m_MaxPlayers; }

	size_t GetNumPlayers(void) const { return m_PlayerCount; }

	void SetMaxPlayers(size_t a_MaxPlayers) { m_MaxPlayers = a_MaxPlayers; }

	/** Returns base64 encoded favicon data (obtained from favicon.png) */
	/**返回base64编码的favicon数据（从favicon.png获取）*/
	const AString & GetFaviconData(void) const { return m_FaviconData; }

	/** Authenticates the specified user, called by cAuthenticator */
	/**验证由cAuthenticator调用的指定用户*/
	void AuthenticateUser(int a_ClientID, const AString & a_Name, const cUUID & a_UUID, const Json::Value & a_Properties);

	/** Notifies the server that a player was created; the server uses this to adjust the number of players */
	/**通知服务器已创建播放机；服务器使用它来调整玩家的数量*/
	void PlayerCreated();

	void KickUser(int a_ClientID, const AString & a_Reason);

	friend class cServerListenCallbacks;  // Accessing OnConnectionAccepted() //访问OnConnectionAccepted（）
	// Hardcore mode or not:
	bool IsHardcore(void) const { return m_bIsHardcore; }

	/** The network sockets listening for client connections. */
	/**网络套接字正在侦听客户端连接*/
	cServerHandlePtrs m_ServerHandles;

		/** The server tick thread takes care of the players who aren't yet spawned in a world */
	/**服务器tick线程负责那些尚未在世界上诞生的玩家*/
	class cTickThread:
		public cIsThread
	{
		using Super = cIsThread;

	public:

		cTickThread(cServer & a_Server);

	protected:
		cServer & m_Server;

		// cIsThread overrides:
		virtual void Execute(void) override;
	} ;

	/** Protects m_Clients and m_ClientsToRemove against multithreaded access. */
	/**保护m_客户端和m_ClientsToRemove免受多线程访问*/
	cCriticalSection m_CSClients;

	/** Clients that are connected to the server and not yet assigned to a cWorld. */
	/**已连接到服务器但尚未分配给cWorld的客户端*/
	cClientHandlePtrs m_Clients; 

	/** Clients that have just been moved into a world and are to be removed from m_Clients in the next Tick(). */
	/**刚刚被移动到世界中并将在下一次勾选（）时从m_客户端中删除的客户端*/
	cClientHandles m_ClientsToRemove;

	/** Number of players currently playing in the server. */
	/**当前在服务器中玩的玩家数*/
	std::atomic_size_t m_PlayerCount;
	int m_ClientViewDistance;  // The default view distance for clients; settable in Settings.ini //客户端的默认视图距离；可在Settings.ini中设置
	bool m_bIsConnected;  // true - connected false - not connected //true-已连接false-未连接
	std::atomic<bool> m_bRestarting;//???

	//cRCONServer m_RCONServer;

	cTickThread m_TickThread;
	cEvent m_RestartEvent;

	/** If true, players will be online-authenticated agains Mojang servers.
	This setting is the same as the "online-mode" setting in Vanilla. */
	/**如果为真，玩家将通过Mojang服务器的在线身份验证。

	此设置与香草中的“在线模式”设置相同*/
	bool m_ShouldAuthenticate;

	/** True if offline UUIDs should be used to load data for players whose
	normal UUIDs cannot be found. This allows transitions from an offline (no-auth)
	server to an online one. Loaded from the settings.ini
	[PlayerData].LoadOfflinePlayerData setting. */
	/**如果应使用脱机UUID为其

	找不到正常的UUID。这允许从脱机状态转换（无身份验证）

	将服务器连接到联机服务器。从settings.ini加载

	[PlayerData].LoadOfflinePlayerData设置*/
	bool m_ShouldLoadOfflinePlayerData;


	/** True if old-style playernames should be used to load data for players
	whose regular datafiles cannot be found. This allows a seamless transition
	from name-based to UUID-based player storage. Loaded from the settings.ini
	[PlayerData].LoadNamedPlayerData setting. */
	/**如果应使用旧式playernames为玩家加载数据，则为True

	找不到其常规数据文件。这允许无缝过渡

	从基于名称到基于UUID的玩家存储。从settings.ini加载

	[PlayerData].LoadNamedPlayerData设置*/
	bool m_ShouldLoadNamedPlayerData;


	AString m_Description; //m_描述

	AString m_ShutdownMessage; //m_关闭消息？？？

	size_t m_MaxPlayers;//最多玩家数量

	bool m_bIsHardcore;//硬核启用???

	AString m_ResourcePackUrl;//url资源？？？

	AString m_FaviconData;//网站头像

	/** True - allow same username to login more than once False - only once */
	/**True-允许同一用户名登录多次False-仅登录一次*/
	bool m_bAllowMultiLogin;

		/** The list of ports on which the server should listen for connections.
	Initialized in InitServer(), used in Start(). */
	/**服务器应在其上侦听连接的端口列表。

	在InitServer（）中初始化，在Start（）中使用*/
	AStringVector m_Ports;

	/** The server ID used for client authentication */
	/**用于客户端身份验证的服务器ID*/
	AString m_ServerID;

	/** True if BungeeCord handshake packets (with player UUID) should be accepted. */
	/**如果应接受蹦极绳握手包（带玩家UUID），则为True*/ //万人跨服同服联机
	bool m_ShouldAllowBungeeCord;

	/** True if usernames should be completed across worlds. */
	/**如果用户名应跨世界填写，则为True*/ //允许多世界选项卡完成
	bool m_ShouldAllowMultiWorldTabCompletion;

	/** True if limit for number of block changes per tick by a player should be enabled. */
	/**如果应启用玩家每勾选的方块更改数量限制，则为True*/ //应该限制玩家块的变化 //限制玩家的方块变化
	bool m_ShouldLimitPlayerBlockChanges;

	bool Tick(float a_Dt);

	/** Creates a new cClientHandle instance and adds it to the list of clients.
	Returns the cClientHandle reinterpreted as cTCPLink callbacks. */
	/**创建新的cClientHandle实例并将其添加到客户端列表中。

	返回重新解释为cTCPLink回调的cClientHandle*/
	cTCPLink::cCallbacksPtr OnConnectionAccepted(const AString & a_RemoteIPAddress);

	/** Ticks the clients in m_Clients, manages the list in respect to removing clients */
	/**勾选m_客户端中的客户端，管理有关删除客户端的列表*/
	void TickClients(float a_Dt);
};

