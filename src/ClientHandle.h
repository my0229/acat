#pragma once

#include "OSSupport/Network.h"

#include "json/json.h"
#include "Defines.h"

#include "UUID.h" //通用单一标识符

#include "Protocol/ProtocolRecognizer.h"
#include "Entities/Player.h"



// fwd:
class cChunkDataSerializer;//块数据序列化程序
class cMonster;//怪物
class cExpOrb;//
class cPainting;//绘画
class cPickup;
class cPlayer;//玩家
class cProtocol;//协议
class cWindow;
class cFallingBlock;//落块
class cCompositeChat;//复合聊天
class cStatManager;//统计管理
class cMap;//地图
class cClientHandle;
typedef std::shared_ptr<cClientHandle> cClientHandlePtr;


class cClientHandle
	:public cTCPLink::cCallbacks
{
public:
	/** Creates a new client with the specified IP address in its description and the specified initial view distance. */
	/**创建一个新的客户端，该客户端的描述中包含指定的IP地址和指定的初始视图距离*/
	cClientHandle(const AString & a_IPString, int a_ViewDistance);
	~cClientHandle();
	/** Called while the client is being ticked from the cServer object */
	/**从cServer对象勾选客户端时调用*/
	void ServerTick(float a_Dt);

	cMultiVersionProtocol m_Protocol;

	void InvalidateCachedSentChunk();
	/** This is an optimization which saves you an iteration of m_SentChunks if you just want to know
	whether or not the player is standing at a sent chunk.
	If this is equal to the coordinates of the chunk the player is currrently standing at, then this must be a sent chunk
	and a member of m_SentChunks.
	Otherwise, this contains an arbitrary value which should not be used. */
	/**这是一个优化，如果你只是想知道的话，它可以为你节省一次m_sentchunk的迭代

	玩家是否站在已发送的区块上。

	如果这等于玩家当前所在区块的坐标，那么这一定是一个已发送区块

	和m_SentChunks的一名成员。

	否则，它包含一个不应使用的任意值 */
	//cChunkCoords m_CachedSentChunk;

	cPlayer * m_Player;
	Vector3d m_ConfirmPosition;
	cPlayer * GetPlayer(void) { return m_Player; }  // tolua_export

	// Temporary (#3115-will-fix): maintain temporary ownership of created cPlayer objects while they are in limbo
	//临时（#3115将修复）：在创建的cLayer对象处于不确定状态时，维护它们的临时所有权
	std::unique_ptr<cPlayer> m_PlayerPtr;

	/** Returns the player's UUID, as used by the protocol */
	/**返回协议使用的玩家的UUID*/
	const cUUID & GetUUID(void) const { return m_UUID; }  // Exported in ManualBindings.cpp

	/** Contains the UUID used by Mojang to identify the player's account. */
	/**包含Mojang用于识别玩家帐户的UUID*/
	cUUID m_UUID;

	/** Returns the protocol version number of the protocol that the client is talking. Returns zero if the protocol version is not (yet) known. */
	/**返回客户端正在讨论的协议的协议版本号。如果协议版本（尚未）未知，则返回零*/
	UInt32 GetProtocolVersion(void) const { return m_ProtocolVersion; }  // tolua_export

	Json::Value m_Properties;

	cCriticalSection                                   m_CSChunkLists;

	void SendChunkData                  (int a_ChunkX, int a_ChunkZ, const std::string_view a_ChunkData);//发送区块数据

	/** Finish logging the user in after authenticating. */
	/**验证后完成用户登录*/
	void FinishAuthenticate(const AString & a_Name, const cUUID & a_UUID, const Json::Value & a_Properties);

	/** Generates an UUID based on the player name provided.
	This is used for the offline (non-auth) mode, when there's no UUID source.
	Each username generates a unique and constant UUID, so that when the player reconnects with the same name, their UUID is the same. */
	/**根据提供的播放器名称生成UUID。

	当没有UUID源时，这用于脱机（非身份验证）模式。

	每个用户名都会生成一个唯一且恒定的UUID，因此当玩家使用相同的名称重新连接时，他们的UUID是相同的*/
	static cUUID GenerateOfflineUUID(const AString & a_Username);  // Exported in ManualBindings.cpp

	/** Authenticates the specified user, called by cAuthenticator */
	/**验证由cAuthenticator调用的指定用户*/
	void Authenticate(const AString & a_Name, const cUUID & a_UUID, const Json::Value & a_Properties);

	int GetUniqueID(void) const { return m_UniqueID; }
	/** Called right after the instance is created to store its SharedPtr inside. */
	/**在创建实例后立即调用以在其中存储其SharedPtr*/
	void SetSelf(cClientHandlePtr a_Self);
	void Kick(const AString & a_Reason);  // tolua_export
		void SendDisconnect                 (const AString & a_Reason);//发送断开
	bool IsDestroyed (void) const { return (m_State == csDestroyed); }

	//由Tick（）和ServerTick（）调用*/
	void ProcessProtocolInOut(void);

	const AString & GetUsername(void) const;
	void SendData(const char * a_Data, size_t a_Size);

	/** Kicks the client if the same username is already logged in.
	Returns false if the client has been kicked, true otherwise. */
	/**如果已登录相同的用户名，则踢踢客户端。

	如果客户端被踢出，则返回false，否则返回true*/
	bool CheckMultiLogin(const AString & a_Username);

	/** Called when the protocol has finished logging the user in.
	Return true to allow the user in; false to kick them.
	*/
	/**当协议完成用户登录时调用。

	返回true以允许用户进入；踢他们是错误的。

	*/
	bool HandleLogin(const AString & a_Username);

	enum eState
		{
			csConnected,             ///< The client has just connected, waiting for their handshake / login
									// ///<客户端刚刚连接，等待握手/登录
			csAuthenticating,        ///< The client has logged in, waiting for external authentication
										/////<客户端已登录，正在等待外部身份验证
			csAuthenticated,         ///< The client has been authenticated, will start streaming chunks in the next tick
									///<客户端已通过身份验证，将在下一个滴答声中开始流式数据块
			csDownloadingWorld,      ///< The client is waiting for chunks, we're waiting for the loader to provide and send them
								 ///<客户端正在等待块，我们正在等待加载程序提供并发送它们
			csConfirmingPos,         ///< The client has been sent the position packet, waiting for them to repeat the position back
									///<已向客户端发送位置数据包，等待他们重复该位置
			csPlaying,               ///< Normal gameplay
									// ///<正常游戏性
			csKicked,                ///< Disconnect packet sent, awaiting connection closure
										/////<断开发送的数据包，等待连接关闭
			csQueuedForDestruction,  ///< The client will be destroyed in the next tick (flag set when socket closed)
									 ///<客户端将在下一次勾选中被销毁（套接字关闭时设置的标志）
			csDestroying,            ///< The client is being destroyed, don't queue any more packets / don't add to chunks
										/////<客户端正在被销毁，不要再排队等待数据包/不要添加到数据块
			csDestroyed,             ///< The client has been destroyed, the destructor is to be called from the owner thread
									///<客户端已被销毁，将从所有者线程调用析构函数
		} ;
	/** Called when the protocol handshake has been received (for protocol versions that support it;
	otherwise the first instant when a username is received).
	Returns true if the player is to be let in, false if they were disconnected
	*/
	/**在收到协议握手时调用（对于支持握手的协议版本；

	否则，在收到用户名的第一个瞬间）。

	如果要让玩家进入，则返回true；如果玩家已断开连接，则返回false

	*/
	bool HandleHandshake        (const AString & a_Username);

	/** The dimension that was last sent to a player in a Respawn or Login packet.
	Used to avoid Respawning into the same dimension, which confuses the client. */
	 /**上次在重生或登录数据包中发送给玩家的维度。

	用于避免重新进入同一维度，从而混淆客户*/
	eDimension m_LastSentDimension;

	/** The actual view distance used, the minimum of client's requested view distance and world's max view distance. */
	/**使用的实际视距、客户端请求的最小视距和世界最大视距*/
	int m_CurrentViewDistance;

	/** The requested view distance from the player. It isn't clamped with 1 and the max view distance of the world. */
	/**请求的与播放机的视距。它不是用1和世界的最大视距夹紧的*/
	int m_RequestedViewDistance;

	bool m_HasSentDC;  ///< True if a Disconnect packet has been sent in either direction
						/////<如果已向任一方向发送断开连接数据包，则为True

	/** Forge handshake state machine. */
	/**伪造握手状态机*/
	//cForgeHandshake m_ForgeHandshake;

	/** This is an optimization which saves you an iteration of m_SentChunks if you just want to know
	whether or not the player is standing at a sent chunk.
	If this is equal to the coordinates of the chunk the player is currrently standing at, then this must be a sent chunk
	and a member of m_SentChunks.
	Otherwise, this contains an arbitrary value which should not be used. */
	/**这是一个优化，如果你只是想知道的话，它可以为你节省一次m_sentchunk的迭代

	玩家是否站在已发送的区块上。

	如果这等于玩家当前所在区块的坐标，那么这一定是一个已发送区块

	和m_SentChunks的一名成员。

	否则，它包含一个不应使用的任意值*/
	//cChunkCoords m_CachedSentChunk;

	// Chunk position when the last StreamChunks() was called; used to avoid re-streaming while in the same chunk
	//调用最后一个StreamChunks（）时的块位置；用于避免在同一块中重新流式处理
	int m_LastStreamedChunkX;
	int m_LastStreamedChunkZ;

	
	/** Number of ticks since the last network packet was received (increased in Tick(), reset in OnReceivedData()) */
	/**自收到最后一个网络数据包以来的刻度数（在刻度（）中增加，在OnReceivedData（）中重置）*/
	std::atomic<int> m_TicksSinceLastPacket;

	/** Duration of the last completed client ping. */
	/**上次完成的客户端ping的持续时间*/
	std::chrono::steady_clock::duration m_Ping;

	/** ID of the last ping request sent to the client. */
	/**上次发送到客户端的ping请求的ID*/
	UInt32 m_PingID;

	// Values required for block dig animation
	//块挖掘动画所需的值
	int m_BlockDigAnimStage;  // Current stage of the animation; -1 if not digging // //动画的现阶段-1如果不挖掘
	int m_BlockDigAnimSpeed;  // Current speed of the animation (units ???) // //动画的当前速度（单位？？）
	int m_BlockDigAnimX;		//区块挖掘动画X；
	int m_BlockDigAnimY;
	int m_BlockDigAnimZ;

	// To avoid dig / aim bug in the client, store the last position given in a DIG_START packet and compare to that when processing the DIG_FINISH packet:
	//为避免客户端出现 dig/aim 错误，请将最后一个位置存储在dig_开始数据包中，并与处理dig_完成数据包时的位置进行比较：
	bool m_HasStartedDigging;
	int m_LastDigBlockX;
	int m_LastDigBlockY;
	int m_LastDigBlockZ;



	/** Number of explosions sent this tick */
	/**这次发射的爆炸次数*/
	int m_NumExplosionsThisTick;

	/** Number of place or break interactions this tick */
	/**此勾选的放置或打断交互的数量*/
	int m_NumBlockChangeInteractionsThisTick;

	/** ID used for identification during authenticating. Assigned sequentially for each new instance. */
	/**身份验证期间用于标识的ID。为每个新实例按顺序分配*/
	int m_UniqueID;

	/** Set to true when the chunk where the player is is sent to the client. Used for spawning the player */
	/**将玩家所在的区块发送到客户端时，设置为true。用于生成玩家*/
	bool m_HasSentPlayerChunk;

	/** Client Settings */
	/**客户端设置*/
	AString m_Locale;

	/** The positions from the last sign that the player placed. It's needed to verify the sign text change. */
	/**玩家放置的最后一个标志的位置。需要验证符号文本的更改*/
	Vector3i m_LastPlacedSign;

	/** The version of the protocol that the client is talking, or 0 if unknown. */
	/**客户端正在谈论的协议版本，如果未知，则为0*/
	UInt32 m_ProtocolVersion;

	static int s_ClientCount;


	/** Time of the last ping request sent to the client. */
	/**上次发送到客户端的ping请求的时间*/
	std::chrono::steady_clock::time_point m_PingStartTime;

	

	/** Called by the protocol recognizer when the protocol version is known. */
	/**当协议版本已知时，由协议识别器调用*/
	void SetProtocolVersion(UInt32 a_ProtocolVersion) { m_ProtocolVersion = a_ProtocolVersion; }



	/** Called when the network socket has been closed. */
	/**在关闭网络套接字时调用*/
	void SocketClosed(void);

	/* Mutex protecting m_State from concurrent writes. */
	/*互斥保护m_状态不受并发写入的影响*/
	cCriticalSection m_CSState;





	
	/** Called to update m_State.
	Only succeeds if a_NewState > m_State, otherwise returns false. */
	/**调用以更新m_状态。

	仅当a_NewState>m_State时成功，否则返回false*/
	bool SetState(eState a_NewState);


	/** The current (networking) state of the client.
	Protected from concurrent writes by m_CSState; but may be read by other threads concurrently.
	If a function depends on m_State or wants to change m_State, it needs to lock m_CSState.
	However, if it only uses m_State for a quick bail out, or it doesn't break if the client disconnects in the middle of it,
	it may just read m_State without locking m_CSState. */
	/**客户端的当前（网络）状态。

	受m_CSState的并发写入保护；但其他线程可以同时读取。

	如果函数依赖于m_状态或希望更改m_状态，则需要锁定m_状态。

	但是，如果它只使用MyStand用于快速跳出，或者如果客户端在其中间断开，则不会中断。

	它可能只读取m_状态而不锁定m_状态*/
	std::atomic<eState> m_State;



	/** Shared pointer to self, so that this instance can keep itself alive when needed. */
	/**共享指向self的指针，以便此实例在需要时保持自身活动*/
	cClientHandlePtr m_Self;

	AString m_Username; //m_用户名

	AString m_IPString;//IP

	void Destroy(void); //毁灭

	/** The link that is used for network communication.
	m_CSOutgoingData is used to synchronize access for sending data. */
	/**用于网络通信的链路。
	m_CSOutgoingData用于同步发送数据的访问*/
	cTCPLinkPtr m_Link;

	/** Protects m_IncomingData against multithreaded access. */
	/**保护m_输入数据不受多线程访问*/
	cCriticalSection m_CSIncomingData;

	/** Queue for the incoming data received on the link until it is processed in Tick().
	Protected by m_CSIncomingData. */
	/**排队等待在链接上接收到的传入数据，直到在Tick（）中对其进行处理。
	受m_CSIncomingData保护*/
	AString m_IncomingData;

	/** Protects m_OutgoingData against multithreaded access. */
	/**保护m_OutingData免受多线程访问*/
	cCriticalSection m_CSOutgoingData;

	/** Buffer for storing outgoing data from any thread; will get sent in Tick() (to prevent deadlocks).
	Protected by m_CSOutgoingData. */
	/**用于存储来自任何线程的传出数据的缓冲区；将在Tick（）中发送（以防止死锁）。

	受m_CSOutingData保护*/
	AString m_OutgoingData;

	// cTCPLink::cCallbacks overrides:
	//cTCPLink:：cCallbacks覆盖：
	virtual void OnLinkCreated(cTCPLinkPtr a_Link) override;
	virtual void OnReceivedData(const char * a_Data, size_t a_Length) override;
	virtual void OnRemoteClosed(void) override;
	virtual void OnError(int a_ErrorCode, const AString & a_ErrorMsg) override;
};