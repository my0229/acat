#include "Globals.h"
#include "Root.h"
#include "OSSupport/NetworkSingleton.h"

#include "Logger.h"	
#include "MemorySettingsRepository.h"

class a
{
};
class b :public a
{
};
int main()
{


	// Initialize logging subsystem: 
	cLogger::InitiateMultithreading();

	cNetworkSingleton::Get().Initialise();//初始化网络

	cRoot root;

	
	auto repo = cpp14::make_unique<cMemorySettingsRepository>();
	root.start(std::move(repo));
}

/** If set to true, the protocols will log each player's incoming (C->S) communication to a per-connection logfile */
/**如果设置为true，协议将把每个玩家的传入（C->s）通信记录到每个连接日志文件中*/
bool g_ShouldLogCommIn;