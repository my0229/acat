
// BiomeDef.h

// Defines relevant information and methods related to biomes
//定义与生物群落相关的信息和方法





#pragma once





// tolua_begin
/** Biome IDs
The first batch corresponds to the clientside biomes, used by MineCraft.
BiomeIDs over 255 are used by Cuberite internally and are translated to MC biomes before sending them to client
*/
/**生物群落
第一批对应于MineCraft使用的客户端生物群落。
Cuberite内部使用超过255种的BioMeid，并在发送给客户之前将其翻译成MC biomes
*/
enum EMCSBiome
{
	biInvalidBiome     = -1,

	biFirstBiome       = 0,
	biOcean            = 0,
	biPlains           = 1,
	biDesert           = 2,
	biExtremeHills     = 3,
	biForest           = 4,
	biTaiga            = 5,
	biSwampland        = 6,
	biRiver            = 7,
	biHell             = 8,  // same as Nether
	biNether           = 8,
	biSky              = 9,  // same as biEnd
	biEnd              = 9,
	biFrozenOcean      = 10,
	biFrozenRiver      = 11,
	biIcePlains        = 12,
	biTundra           = 12,  // same as Ice Plains
	biIceMountains     = 13,
	biMushroomIsland   = 14,
	biMushroomShore    = 15,
	biBeach            = 16,
	biDesertHills      = 17,
	biForestHills      = 18,
	biTaigaHills       = 19,
	biExtremeHillsEdge = 20,
	biJungle           = 21,
	biJungleHills      = 22,

	// Release 1.7 biomes:
	biJungleEdge       = 23,
	biDeepOcean        = 24,
	biStoneBeach       = 25,
	biColdBeach        = 26,
	biBirchForest      = 27,
	biBirchForestHills = 28,
	biRoofedForest     = 29,
	biColdTaiga        = 30,
	biColdTaigaHills   = 31,
	biMegaTaiga        = 32,
	biMegaTaigaHills   = 33,
	biExtremeHillsPlus = 34,
	biSavanna          = 35,
	biSavannaPlateau   = 36,
	biMesa             = 37,
	biMesaPlateauF     = 38,
	biMesaPlateau      = 39,

	// Automatically capture the maximum consecutive biome value into biMaxBiome:
	//自动将最大连续生物群落值捕获到biMaxBiome中：
	biNumBiomes,  // True number of biomes, since they are zero-based //生物群落的真实数量，因为它们是零基的
	biMaxBiome = biNumBiomes - 1,  // The maximum biome value ////最大生物价值


	// Add this number to the biomes to get the variant
	//将此数字添加到biomes以获得变体

	biVariant = 128,

	// Release 1.7 biome variants:
	//第1.7版生物群落变体：
	biFirstVariantBiome    = 129,
	biSunflowerPlains      = 129,
	biDesertM              = 130,
	biExtremeHillsM        = 131,
	biFlowerForest         = 132,
	biTaigaM               = 133,
	biSwamplandM           = 134,
	biIcePlainsSpikes      = 140,
	biJungleM              = 149,
	biJungleEdgeM          = 151,
	biBirchForestM         = 155,
	biBirchForestHillsM    = 156,
	biRoofedForestM        = 157,
	biColdTaigaM           = 158,
	biMegaSpruceTaiga      = 160,
	biMegaSpruceTaigaHills = 161,
	biExtremeHillsPlusM    = 162,
	biSavannaM             = 163,
	biSavannaPlateauM      = 164,
	biMesaBryce            = 165,
	biMesaPlateauFM        = 166,
	biMesaPlateauM         = 167,
	// Automatically capture the maximum consecutive biome value into biVarientMaxBiome:
	//自动将最大连续生物群落值捕获到biVarientMaxBiome中：
	biNumVariantBiomes,  // True number of biomes, since they are zero-based
						//生物群落的真实数量，因为它们是零基的
	biMaxVariantBiome = biNumVariantBiomes - 1,  // The maximum biome value

} ;

// tolua_end





/** Hash for EMCSBiome, so that it can be used in std::unordered_map etc. */
/**EMCSBiome的散列，以便可以在std:：无序_映射等中使用*/
struct BiomeHasher
{
public:
	std::size_t operator() (const EMCSBiome a_Biome) const
	{
		return static_cast<std::size_t>(a_Biome);
	}
};





// tolua_begin

/** Translates a biome string to biome enum. Takes either a number or a biome alias (built-in). Returns biInvalidBiome on failure. */
/** 将生物群落字符串转换为生物群落枚举。采用数字或生物群落别名（内置）。失败时返回biInvalidBiome */
extern EMCSBiome StringToBiome(const AString & a_BiomeString);

/** Translates biome enum into biome string. Returns empty string on failure (unknown biome). */
/**将生物群落枚举转换为生物群落字符串。失败时返回空字符串（未知）*/
extern AString BiomeToString(int a_Biome);

/** Returns true if the biome has no downfall - deserts and savannas */
/**如果生物群落没有崩溃-沙漠和大草原，则返回true*/
extern bool IsBiomeNoDownfall(EMCSBiome a_Biome);

/** Returns true if the biome is an ocean biome. */
/**如果生物群落是海洋生物群落，则返回true*/
inline bool IsBiomeOcean(int a_Biome)
{
	return ((a_Biome == biOcean) || (a_Biome == biDeepOcean));
}

/** Returns true if the biome is very cold
(has snow on ground everywhere, turns top water to ice, has snowfall instead of rain everywhere).
Doesn't report mildly cold biomes (where it snows above certain elevation), use IsBiomeCold() for those. */
/**如果生物群落非常寒冷，则返回true
（到处都是雪，把水面变成冰，到处都是雪而不是雨）。
不报告轻度寒冷的生物群落（在一定海拔高度以上下雪），请使用IsBiomeCold（）进行报告*/
extern bool IsBiomeVeryCold(EMCSBiome a_Biome);

/** Returns true if the biome is cold
(has snow and snowfall at higher elevations but not at regular heights).
Doesn't report Very Cold biomes, use IsBiomeVeryCold() for those. */
/**如果生物群落寒冷，则返回true
（在较高海拔处有雪和降雪，但在正常高度上没有）。
不报告非常寒冷的生物群落，请使用IsBiomeVeryCold（）进行报告*/
extern bool IsBiomeCold(EMCSBiome a_Biome);

/** Returns the height when a biome when a biome starts snowing. */
/**返回生物群落开始下雪时的高度*/
extern int GetSnowStartHeight(EMCSBiome a_Biome);

// tolua_end
