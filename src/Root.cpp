#include "Globals.h" 

#include "Root.h"


#include "Server.h"	
#include "LoggerListeners.h"
#include "IniFile.h"
#include "OverridesSettingsRepository.h"
#include "Logger.h"
#include "World.h"

cRoot * cRoot::s_Root = nullptr;

cRoot::cRoot()
{
	s_Root = this;
}

void cRoot::start(std::unique_ptr<cSettingsRepositoryInterface> a_OverridesRepo)
{
	auto consoleLogListener = MakeConsoleListener(m_RunAsService);
	auto consoleAttachment = cLogger::GetInstance().AttachListener(std::move(consoleLogListener));

	cLogger::cAttachment fileAttachment;
	if (!a_OverridesRepo->HasValue("Server","DisableLogFile"))
	{
		auto fileLogListenerRet = MakeFileListener();
		if (!fileLogListenerRet.first)
		{
			//m_TerminateEventRaised = true;
			LOGERROR("Failed to open log file, aborting");
			return;
		}
		fileAttachment = cLogger::GetInstance().AttachListener(std::move(fileLogListenerRet.second));
	}

	m_Server = new cServer();

	m_SettingsFilename = "settings.ini";
	if (a_OverridesRepo->HasValue("Server","ConfigFile"))
	{
		m_SettingsFilename = a_OverridesRepo->GetValue("Server","ConfigFile");
	}

	auto IniFile = cpp14::make_unique<cIniFile>();
	bool IsNewIniFile = !IniFile->ReadFile(m_SettingsFilename);

	if (IsNewIniFile)
	{
		LOGWARN("Regenerating settings.ini, all settings will be reset"); /** ????????????settings.ini???????????????? */
		IniFile->AddHeaderComment(" This is the main server configuration");/** ???????????????? */
		IniFile->AddHeaderComment(" Most of the settings here can be configured using the webadmin interface, if enabled in webadmin.ini");/** ?????webadmin.ini???????????????webadmin???????????????????? */
	}
	
	auto settingsRepo = cpp14::make_unique<cOverridesSettingsRepository>(std::move(IniFile), std::move(a_OverridesRepo));

	LOG("Starting server..."); 

	
	bool ShouldAuthenticate = settingsRepo->GetValueSetB("Authentication", "Authenticate", true);
	
	if (!m_Server->InitServer(*settingsRepo, ShouldAuthenticate))
	{
		settingsRepo->Flush();
		LOGERROR("Failure starting server, aborting...");//日志错误（“启动服务器失败，正在中止…”）；
		return;
	}

	m_Server->Start();
	cWorld *w = new cWorld();
	w->Start();
	while (1);
}

void cRoot::AuthenticateUser(int a_ClientID, const AString & a_Name, const cUUID & a_UUID, const Json::Value & a_Properties)
{
	m_Server->AuthenticateUser(a_ClientID, a_Name, a_UUID, a_Properties);
}

void cRoot::KickUser(int a_ClientID, const AString & a_Reason)
{
		m_Server->KickUser(a_ClientID, a_Reason);
}

cWorld * cRoot::GetWorld(const AString & a_WorldName)
{
	const auto FindResult = m_WorldsByName.find(a_WorldName);
	if (FindResult != m_WorldsByName.cend())
	{
		return &FindResult->second;
	}

	return nullptr;
}
