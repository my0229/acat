#pragma once
#include "OSSupport/IsThread.h"
#include "ChunkDataCallback.h"

class cWorld;
class cChunkSender:
	public cIsThread,public cChunkDataCopyCollector
{
	using Super = cIsThread;
public:

	enum eChunkPriority
	{
		E_CHUNK_PRIORITY_HIGH = 0,
		E_CHUNK_PRIORITY_MIDHIGH,
		E_CHUNK_PRIORITY_MEDIUM,
		E_CHUNK_PRIORITY_LOW,

	};

	
	cChunkSender(cWorld & a_World);
	cWorld & m_World;
	// cIsThread override:
	virtual void Execute(void) override;
	/** Sends the specified chunk to all the specified clients */
	/**将指定的区块发送到所有指定的客户端 */
	void SendChunk(int a_ChunkX, int a_ChunkZ, std::unordered_set<cClientHandle *> a_Clients);

	unsigned char m_BiomeMap[cChunkDef::Width * cChunkDef::Width];



	struct sChunkQueue
	{
		eChunkPriority m_Priority;
		cChunkCoords m_Chunk;

		bool operator <(const sChunkQueue & a_Other) const
		{
			/* The Standard Priority Queue sorts from biggest to smallest
			return true here means you are smaller than the other object, and you get pushed down.

			The priorities go from HIGH (0) to LOW (2), so a smaller priority should mean further up the list
			therefore, return true (affirm we're "smaller", and get pushed down) only if our priority is bigger than theirs (they're more urgent)
			*/
			/*标准优先级队列从最大到最小排序

			这里返回true意味着你比另一个对象小，你会被压下去。

			优先级从高（0）到低（2），因此较小的优先级意味着列表的更高

			因此，只有当我们的优先级高于他们的优先级（他们的优先级更高）时，才返回true（确认我们“更小”，并被压下）

			*/
			return this->m_Priority > a_Other.m_Priority;
		}
	};
	std::priority_queue<sChunkQueue> m_SendChunks;
	cCriticalSection  m_CS;

	/** Used for sending chunks to specific clients */
	/**用于向特定客户端发送块*/
	struct sSendChunk
	{
		cChunkCoords m_Chunk;
		std::unordered_set<cClientHandle *> m_Clients;
		eChunkPriority m_Priority;
		sSendChunk(cChunkCoords a_Chunk, eChunkPriority a_Priority) :
			m_Chunk(a_Chunk),
			m_Priority(a_Priority)
		{
		}
	};

	std::unordered_map<cChunkCoords, sSendChunk, cChunkCoordsHash> m_ChunkInfo;
};