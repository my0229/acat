#pragma once

#include "Protocol/Authenticator.h"		
#include "World.h"
class cServer;
class cSettingsRepositoryInterface;
class cRoot
{
public:
	cRoot();
	static cRoot * Get() { return s_Root; }
	cServer * GetServer(void) { return m_Server; }
	void start(std::unique_ptr<cSettingsRepositoryInterface> a_OverridesRepo);
	cAuthenticator &   GetAuthenticator  (void) { return m_Authenticator; }
	cAuthenticator     m_Authenticator;
	AString m_SettingsFilename;
	/** Called by cAuthenticator to auth the specified user */
	/***由cAuthenticator调用以对指定用户进行身份验证 */
	void AuthenticateUser(int a_ClientID, const AString & a_Name, const cUUID & a_UUID, const Json::Value & a_Properties);

	cServer * m_Server;
	/** Kicks the user, no matter in what world they are. Used from cAuthenticator */ 
	void KickUser(int a_ClientID, const AString & a_Reason);

	/* If set to true, binary will attempt to run as a service on Windows */
	/**如果设置为true，binary将尝试在Windows上作为服务运行*/
	bool cRoot::m_RunAsService = false;
	/** Returns a pointer to the world specified. If no world of that name exists, returns a nullptr. */

	cWorld * GetWorld(const AString & a_WorldName);
	static cRoot * s_Root;

	typedef std::map<AString, cWorld> WorldMap;

	cWorld * m_pDefaultWorld;
	WorldMap m_WorldsByName;
};

