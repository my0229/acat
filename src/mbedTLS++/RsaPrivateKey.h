
// RsaPrivateKey.h
//Rsa私钥
// Declares the cRsaPrivateKey class representing a private key for RSA operations.
//声明表示RSA操作私钥的cRsaPrivateKey类。

//创建服务时调用



#pragma once

#include "CtrDrbgContext.h"
#include "mbedtls/rsa.h"





/** Encapsulates an RSA private key used in PKI cryptography */
/**封装用于PKI加密的RSA私钥*/
class cRsaPrivateKey
{
	friend class cSslContext;

public:
	/** Creates a new empty object, the key is not assigned */
	/**创建新的空对象，但未指定关键点*/
	cRsaPrivateKey(void);

	/** Deep-copies the key from a_Other */
	cRsaPrivateKey(const cRsaPrivateKey & a_Other);

	~cRsaPrivateKey();

	/** Generates a new key within this object, with the specified size in bits.
	Returns true on success, false on failure. */
	/**在此对象内生成一个新密钥，其大小以位为单位。

	成功时返回true，失败时返回false*/
	bool Generate(unsigned a_KeySizeBits = 1024);

	/** Returns the public key part encoded in ASN1 DER encoding */
	/**返回按ASN1 DER编码的公钥部分*/
	AString GetPubKeyDER(void);

	/** Decrypts the data using RSAES-PKCS#1 algorithm.
	Both a_EncryptedData and a_DecryptedData must be at least <KeySizeBytes> bytes large.
	Returns the number of bytes decrypted, or negative number for error. */
	/**使用RSAES-PKCS#1算法解密数据。

	加密数据和解密数据必须至少大<KeySizeBytes>字节。

	返回解密的字节数，或返回错误的负数*/
	int Decrypt(const Byte * a_EncryptedData, size_t a_EncryptedLength, Byte * a_DecryptedData, size_t a_DecryptedMaxLength);

	/** Encrypts the data using RSAES-PKCS#1 algorithm.
	Both a_EncryptedData and a_DecryptedData must be at least <KeySizeBytes> bytes large.
	Returns the number of bytes decrypted, or negative number for error. */

	/**使用RSAES-PKCS#1算法加密数据。

	加密数据和解密数据必须至少大<KeySizeBytes>字节。

	返回解密的字节数，或返回错误的负数*/
	int Encrypt(const Byte * a_PlainData, size_t a_PlainLength, Byte * a_EncryptedData, size_t a_EncryptedMaxLength);

protected:
	/** The mbedTLS key context */
	/**mbedTLS关键上下文*/
	mbedtls_rsa_context m_Rsa;

	/** The random generator used for generating the key and encryption / decryption */
	/**用于生成密钥和加密/解密的随机生成器*/
	cCtrDrbgContext m_CtrDrbg;


	/** Returns the internal context ptr. Only use in mbedTLS API calls. */
	/**返回内部上下文ptr。仅在mbedTLS API调用中使用*/
	mbedtls_rsa_context * GetInternal(void) { return &m_Rsa; }
} ;

typedef std::shared_ptr<cRsaPrivateKey> cRsaPrivateKeyPtr;





