
// BoundingBox.h

// Declares the cBoundingBox class representing an axis-aligned bounding box with floatingpoint coords
//声明cBoundingBox类，该类表示具有浮点坐标的轴对齐边界框


#pragma once

#include "Defines.h"





// tolua_begin

/** Represents two sets of coords, minimum and maximum for each direction.
All the coords within those limits (inclusive the edges) are considered "inside" the box.
For intersection purposes, though, if the intersection is "sharp" in any coord (i. e. zero volume),
the boxes are considered non-intersecting. */
/**表示两组坐标，每个方向的最小坐标和最大坐标。
这些限制范围内的所有坐标（包括边缘）都被视为“在”框内。
但是，出于交叉口的目的，如果交叉口在任何坐标中都是“尖锐的”（即。E零体积），
这些框被认为是不相交的*/

class cBoundingBox
{
public:
	cBoundingBox(double a_MinX, double a_MaxX, double a_MinY, double a_MaxY, double a_MinZ, double a_MaxZ);
	cBoundingBox(Vector3d a_Min, Vector3d a_Max);
	cBoundingBox(Vector3d a_Pos, double a_Radius, double a_Height);
	/** Constructor that allows to define a bounding box given a center point (a_Pos), a horizontal radius (a_Radius),
	a height starting from given center point (a_Height) and a vertical offset (a_VerticalOffset) to adjust the vertical starting point.
	For example: cBoundingBox([0, 0, 0], 6, 6, -3) would create a bounding cube from (-3, -3, -3) to (3, 3, 3). */
	/**构造函数，允许定义给定中心点（a_位置）、水平半径（a_半径）的边界框，
	从给定中心点开始的高度（a_高度）和垂直偏移（a_垂直偏移），以调整垂直起点。
	例如：cBoundingBox（[0,0,0]，6,6，-3）将创建一个从（-3，-3，-3）到（3,3,3）的边界立方体*/
	cBoundingBox(Vector3d a_Pos, double a_Radius, double a_Height, double a_VerticalOffset);
	cBoundingBox(Vector3d a_Pos, double a_CubeLength);

	#ifdef TOLUA_EXPOSITION  // tolua isn't aware of implicitly generated copy constructors
		cBoundingBox(const cBoundingBox & a_Orig);
	#endif

	/** Moves the entire boundingbox by the specified offset */
	/**将整个边界框移动指定的偏移量*/
	void Move(double a_OffX, double a_OffY, double a_OffZ);

	/** Moves the entire boundingbox by the specified offset */
	/**将整个边界框移动指定的偏移量*/
	void Move(Vector3d a_Off);

	/** Expands the bounding box by the specified amount in each direction (so the box becomes larger by 2 * Expand in each direction) */
	/**在每个方向上按指定的数量展开边界框（使框变大2*在每个方向上展开）*/
	void Expand(double a_ExpandX, double a_ExpandY, double a_ExpandZ);

	/** Returns true if the two bounding boxes intersect */
	/**如果两个边界框相交，则返回true*/
	bool DoesIntersect(const cBoundingBox & a_Other);

	/** Returns the union of the two bounding boxes */
	/**返回两个边界框的并集*/
	cBoundingBox Union(const cBoundingBox & a_Other);

	/** Returns true if the point is inside the bounding box */
	/**如果点位于边界框内，则返回true*/
	bool IsInside(Vector3d a_Point);

	/** Returns true if the point is inside the bounding box */
	/**如果点位于边界框内，则返回true*/
	bool IsInside(double a_X, double a_Y, double a_Z);

	/** Returns true if a_Other is inside this bounding box */
	/**如果某个对象在此边界框内，则返回true*/
	bool IsInside(cBoundingBox & a_Other);

	/** Returns true if a boundingbox specified by a_Min and a_Max is inside this bounding box */
	/**如果由最小值和最大值指定的边界框在此边界框内，则返回true*/
	bool IsInside(Vector3d a_Min, Vector3d a_Max);

	/** Returns true if the specified point is inside the bounding box specified by its min / max corners */
	/**如果指定点位于由其最小/最大角点指定的边界框内，则返回true*/
	static bool IsInside(Vector3d a_Min, Vector3d a_Max, Vector3d a_Point);

	/** Returns true if the specified point is inside the bounding box specified by its min / max corners */
	/**如果指定点位于由其最小/最大角点指定的边界框内，则返回true*/
	static bool IsInside(Vector3d a_Min, Vector3d a_Max, double a_X, double a_Y, double a_Z);

	// tolua_end

	/** Returns true if this bounding box is intersected by the line specified by its two points
	Also calculates the distance along the line in which the intersection occurs, and the face hit (BLOCK_FACE_ constants)
	Only forward collisions (a_LineCoeff >= 0) are returned.
	Exported to Lua manually, because ToLua++ would generate needless input params (a_LineCoeff, a_Face). */

	bool CalcLineIntersection(Vector3d a_LinePoint1, Vector3d a_LinePoint2, double & a_LineCoeff, eBlockFace & a_Face) const;

	/** Returns true if the specified bounding box is intersected by the line specified by its two points
	Also calculates the distance along the line in which the intersection occurs, and the face hit (BLOCK_FACE_ constants)
	Only forward collisions (a_LineCoeff >= 0) are returned.
	Exported to Lua manually, because ToLua++ would generate needless input params (a_LineCoeff, a_Face). */
	static bool CalcLineIntersection(Vector3d a_Min, Vector3d a_Max, Vector3d a_LinePoint1, Vector3d a_LinePoint2, double & a_LineCoeff, eBlockFace & a_Face);

	/** Calculates the intersection of the two bounding boxes; returns true if nonempty.
	Exported manually, because ToLua++ would generate needless input params (a_Intersection). */
	bool Intersect(const cBoundingBox & a_Other, cBoundingBox & a_Intersection) const;

	// tolua_begin

	double GetMinX(void) const { return m_Min.x; }
	double GetMinY(void) const { return m_Min.y; }
	double GetMinZ(void) const { return m_Min.z; }

	double GetMaxX(void) const { return m_Max.x; }
	double GetMaxY(void) const { return m_Max.y; }
	double GetMaxZ(void) const { return m_Max.z; }

	Vector3d GetMin(void) const { return m_Min; }
	Vector3d GetMax(void) const { return m_Max; }

	// tolua_end

protected:
	Vector3d m_Min;
	Vector3d m_Max;

} ;  // tolua_export




